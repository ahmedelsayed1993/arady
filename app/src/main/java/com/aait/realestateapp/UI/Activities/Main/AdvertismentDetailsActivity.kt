package com.aait.realestateapp.UI.Activities.Main

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.GPS.GPSTracker
import com.aait.realestateapp.GPS.GpsTrakerListener
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.AddEstateResponse
import com.aait.realestateapp.Models.ConversationIdResponse
import com.aait.realestateapp.Models.EstateDetailsResponse
import com.aait.realestateapp.Models.EstatesModel
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Controllers.MyEstatesAdapter
import com.aait.realestateapp.UI.Controllers.SlidersAdapter
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.DialogUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderViewPager
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.Float
import java.util.*
import kotlin.collections.ArrayList

class AdvertismentDetailsActivity:ParentActivity(),OnItemClickListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_advertisement_details
    lateinit var estate_info:LinearLayout
    lateinit var info_image:ImageView
    lateinit var info_text:TextView
    lateinit var advertiser:LinearLayout
    lateinit var advertiser_image:ImageView
    lateinit var advertiser_text:TextView
    lateinit var similar:LinearLayout
    lateinit var similar_image:ImageView
    lateinit var similar_text:TextView
    lateinit var info_lay:LinearLayout
    lateinit var advert_lay:LinearLayout
    lateinit var similar_lay:LinearLayout
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var viewPager:CardSliderViewPager
    lateinit var category:TextView
    lateinit var price:TextView
    lateinit var address:TextView
    lateinit var views:TextView
    lateinit var fav:ImageView
    lateinit var share:ImageView
    lateinit var ads_num:TextView
    lateinit var details:TextView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var phone:TextView
    lateinit var chat:TextView
    lateinit var map:MapView
    lateinit var ads:RecyclerView
    lateinit var features:WebView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var myEstatesAdapter: MyEstatesAdapter
    var estates = ArrayList<EstatesModel>()
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var link = ""
    internal lateinit var markerOptions: MarkerOptions
    internal var hmap = HashMap<Marker, EstatesModel>()
    private var mAlertDialog: AlertDialog? = null
    lateinit var estatesModel:EstatesModel
    var id = 0
    var reciver = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        CommonUtil.setConfig(lang.appLanguage,mContext)
        val intent = Intent(this,AdvertismentDetails::class.java)
        intent.putExtra("id",id)
        startActivity(intent)
        finish()

        estate_info = findViewById(R.id.estate_info)
        info_image = findViewById(R.id.info_image)
        info_text = findViewById(R.id.info_text)
        advertiser = findViewById(R.id.advertiser)
        advertiser_image = findViewById(R.id.advertiser_image)
        advertiser_text = findViewById(R.id.advertiser_text)
        similar = findViewById(R.id.similar)
        similar_image = findViewById(R.id.similar_image)
        similar_text = findViewById(R.id.similar_text)
        info_lay = findViewById(R.id.info_lay)
        advert_lay = findViewById(R.id.advert_lay)
        similar_lay = findViewById(R.id.similar_lay)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        chat = findViewById(R.id.chat)
        map = findViewById(R.id.map)
        ads = findViewById(R.id.ads)
        back = findViewById(R.id.back)
        CommonUtil.setConfig(lang.appLanguage,mContext)
        features = findViewById(R.id.features)
        CommonUtil.setConfig(lang.appLanguage,mContext)
        viewPager = findViewById(R.id.viewPager)
        category = findViewById(R.id.category)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        views = findViewById(R.id.views)
        price = findViewById(R.id.price)
        fav = findViewById(R.id.fav)
        share = findViewById(R.id.share)
        ads_num = findViewById(R.id.ads_num)
        address = findViewById(R.id.address)
        details = findViewById(R.id.details)
        title = findViewById(R.id.title)
        CommonUtil.setConfig(lang.appLanguage,mContext)
        val webSettings: WebSettings = features.getSettings()
        webSettings.userAgentString = lang.appLanguage
        webSettings.javaScriptEnabled = true
        CommonUtil.setConfig(lang.appLanguage,mContext)
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)
        try {
            MapsInitializer.initialize(mContext!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        back.setOnClickListener { startActivity(Intent(this,SearchActivity::class.java))
        finish()}
        title.text = getString(R.string.property_details)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        myEstatesAdapter = MyEstatesAdapter(mContext,estates,R.layout.recycle_ads)
        myEstatesAdapter.setOnItemClickListener(this)
        ads.layoutManager = linearLayoutManager
        ads.adapter = myEstatesAdapter
        share.setOnClickListener { CommonUtil.ShareProductName(mContext,link,"") }
        estate_info.setOnClickListener {
            CommonUtil.setConfig(lang.appLanguage,mContext)
            estate_info.background = mContext.resources.getDrawable(R.drawable.blue_shape)
            info_image.setImageResource(R.mipmap.active_info)
            info_text.visibility = View.VISIBLE
            advertiser.background = mContext.resources.getDrawable(R.color.colorWhite)
            advertiser_image.setImageResource(R.mipmap.user_non)
            advertiser_text.visibility = View.GONE
            similar.background = mContext.resources.getDrawable(R.color.colorWhite)
            similar_image.setImageResource(R.mipmap.gray_ads)
            similar_text.visibility = View.GONE
            info_lay.visibility = View.VISIBLE
            advert_lay.visibility = View.GONE
            similar_lay.visibility = View.GONE
        }

        advertiser.setOnClickListener {
            CommonUtil.setConfig(lang.appLanguage,mContext)
            estate_info.background = mContext.resources.getDrawable(R.color.colorWhite)
            info_image.setImageResource(R.mipmap.gray_info_non)
            info_text.visibility = View.GONE
            advertiser.background = mContext.resources.getDrawable(R.drawable.blue_shape)
            advertiser_image.setImageResource(R.mipmap.active_user)
            advertiser_text.visibility = View.VISIBLE
            similar.background = mContext.resources.getDrawable(R.color.colorWhite)
            similar_image.setImageResource(R.mipmap.gray_ads)
            similar_text.visibility = View.GONE
            info_lay.visibility = View.GONE
            advert_lay.visibility = View.VISIBLE
            similar_lay.visibility = View.GONE
        }
        similar.setOnClickListener {
            CommonUtil.setConfig(lang.appLanguage,mContext)
            estate_info.background = mContext.resources.getDrawable(R.color.colorWhite)
            info_image.setImageResource(R.mipmap.gray_info_non)
            info_text.visibility = View.GONE
            advertiser.background = mContext.resources.getDrawable(R.color.colorWhite)
            advertiser_image.setImageResource(R.mipmap.user_non)
            advertiser_text.visibility = View.GONE
            similar.background = mContext.resources.getDrawable(R.drawable.blue_shape)
            similar_image.setImageResource(R.mipmap.blue_ads_acctive)
            similar_text.visibility = View.VISIBLE
            info_lay.visibility = View.GONE
            advert_lay.visibility = View.GONE
            similar_lay.visibility = View.VISIBLE
        }
        getLocationWithPermission(null,null)

        chat.setOnClickListener { if (user.loginStatus!!){
            getID()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }
        }

        fav.setOnClickListener {
            if (user.loginStatus!!){
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.AddFav("Bearer"+user.userData.token,lang.appLanguage,id)?.enqueue(object :Callback<AddEstateResponse>{
                    override fun onResponse(call: Call<AddEstateResponse>, response: Response<AddEstateResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                if (response.body()?.data==1){
                                    fav.setImageResource(R.mipmap.like_active)
                                }else{
                                    fav.setImageResource(R.mipmap.white_fav)
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<AddEstateResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()

                    }

                })
            }else{
                 CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,SearchActivity::class.java))
        finish()
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this, AdvertismentDetailsActivity::class.java)
        intent.putExtra("id",estates.get(position).id)
        startActivity(intent)
        finish()
    }

    override fun onTextChanged(view: View, position: Int) {

    }

    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Conversation("Bearer"+user.userData.token,lang.appLanguage,reciver,id)?.enqueue(object :
                Callback<ConversationIdResponse> {
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                    call: Call<ConversationIdResponse>,
                    response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@AdvertismentDetailsActivity,ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    override fun onMapReady(p0: GoogleMap) {
        this.googleMap = p0!!
        googleMap!!.setOnMarkerClickListener(this)
    }

    override fun onMarkerClick(p0: Marker): Boolean {
        if (p0.getTitle() != "موقعى") {
            if (hmap.containsKey(p0)) {
                estatesModel = hmap[p0]!!
                val intent = Intent(this, AdvertismentDetailsActivity::class.java)
                intent.putExtra("id",estatesModel.id)
                startActivity(intent)
                finish()
            }
        }
        return false
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission(type: String?, category: Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(type, category)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(type, category)
        }

    }

    internal fun getCurrentLocation(type: String?, category: Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                if (user.loginStatus!!){
                   // getData(gps.getLatitude().toString(), gps.getLongitude().toString(),"Bearer"+user.userData.token)
                }else {
                  //  getData(gps.getLatitude().toString(), gps.getLongitude().toString(), null)
                }
                //  putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address", result)
                    }
                } catch (e: IOException) {
                }
                // googleMap.clear()
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
            }
        }
    }

    fun getData(lat:String,lng:String,token:String?){
        showProgressDialog(getString(R.string.please_wait))
        CommonUtil.setConfig(lang.appLanguage,mContext)
        Client.getClient()?.create(Service::class.java)?.EstateDetails(token,lang.appLanguage,id,lat, lng)?.enqueue(object : Callback<EstateDetailsResponse> {
            override fun onResponse(call: Call<EstateDetailsResponse>, response: Response<EstateDetailsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        myEstatesAdapter.updateAll(response.body()?.data?.estates!!)
                        if (response.body()?.data?.estates!!.isEmpty()){
                            googleMap.clear()
                        }else{
                            googleMap!!.clear()

                            for (i in 0..response.body()?.data?.estates!!.size - 1) {
                                addShop(response.body()?.data?.estates?.get(i)!!)
                            }
                        }
                        initSliderAds(response.body()?.data?.images!!)
                        category.text = response.body()?.data?.category
                        views.text = response.body()?.data?.views
                        price.text = response.body()?.data?.price + getString(R.string.Rial)
                        CommonUtil.setConfig(lang.appLanguage,mContext)
                        features.loadUrl(response.body()?.data?.features_url!!)
                        CommonUtil.setConfig(lang.appLanguage,mContext)
                        address.text = response.body()?.data?.address
                        ads_num.text = response.body()?.data?.number.toString()
                        details.text = response.body()?.data?.details
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        phone.text = response.body()?.data?.phone
                        name.text = response.body()?.data?.username
                        ads_num.text = response.body()?.data?.number.toString()
                        reciver = response.body()?.data?.user!!
                        link = response.body()?.data?.share_link!!
                        if (response.body()?.data?.show_info==1){
                            image.visibility = View.VISIBLE
                            name.visibility = View.VISIBLE
                            phone.visibility = View.VISIBLE
                            chat.visibility = View.VISIBLE
                        }else{
                            image.visibility = View.GONE
                            name.visibility = View.GONE
                            phone.visibility = View.GONE
                            chat.visibility = View.GONE
                        }
                        if (response.body()?.data?.is_favorite==1){
                            fav.setImageResource(R.mipmap.like_active)
                        }else{
                            fav.setImageResource(R.mipmap.white_fav)
                        }

                    }
                }
            }

            override fun onFailure(call: Call<EstateDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })
    }

    fun initSliderAds(list: java.util.ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE

        }
        else{
            viewPager.visibility= View.VISIBLE

            viewPager.adapter= SlidersAdapter(mContext!!, list)

        }
    }

    internal fun addShop(shopModel: EstatesModel) {
        markerOptions = MarkerOptions().position(
            LatLng(Float.parseFloat(shopModel.lat!!).toDouble(), Float.parseFloat(shopModel.lng!!).toDouble()))
            .title("")
            .icon(BitmapDescriptorFactory.fromBitmap(createStoreMarker(shopModel.price + getString(R.string.Rial))))
        myMarker = googleMap!!.addMarker(markerOptions)

        googleMap!!.animateCamera(
                CameraUpdateFactory.newLatLngZoom(LatLng(java.lang.Double.parseDouble(shopModel.lat!!), java.lang.Double.parseDouble(shopModel.lng!!)), 10f)
        )
        hmap[myMarker] = shopModel

    }
    private fun createStoreMarker(text: String): Bitmap? {
        val markerLayout: View = layoutInflater.inflate(R.layout.custom_blue_marker, null)
        val markerImage = markerLayout.findViewById<View>(R.id.marker_image) as ImageView
        val markerRating = markerLayout.findViewById<View>(R.id.marker_text) as TextView
        markerImage.setImageResource(R.mipmap.pink_marker)
        markerRating.setText(text)
        markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        markerLayout.layout(0, 0, markerLayout.measuredWidth, markerLayout.measuredHeight)
        val bitmap: Bitmap = Bitmap.createBitmap(markerLayout.measuredWidth, markerLayout.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        markerLayout.draw(canvas)
        return bitmap
    }
}