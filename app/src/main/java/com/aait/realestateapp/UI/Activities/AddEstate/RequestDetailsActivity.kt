package com.aait.realestateapp.UI.Activities.AddEstate

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.ConversationIdResponse
import com.aait.realestateapp.Models.EstateDetailsResponse
import com.aait.realestateapp.Models.RequestDetailsResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.ChatActivity
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.aait.realestateapp.UI.Activities.Main.SearchActivity
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RequestDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_request_details
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var category:TextView
    lateinit var date:TextView
    lateinit var honest:TextView
    lateinit var features:WebView
    lateinit var details:TextView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var chat:TextView
    lateinit var call:TextView
    lateinit var whats:TextView
    lateinit var publish:Button
    lateinit var info:LinearLayout
    var id = 0
    var phone = ""
    var what = ""
    var use = 0
    var pupl = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        CommonUtil.setConfig(lang.appLanguage,mContext)
        val intent = Intent(this,RequestDetails::class.java)
        intent.putExtra("id",id)
        startActivity(intent)
        finish()
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        category = findViewById(R.id.category)
        date = findViewById(R.id.date)
        honest = findViewById(R.id.honest)
        features = findViewById(R.id.features)
        details = findViewById(R.id.details)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        chat = findViewById(R.id.chat)
        call = findViewById(R.id.call)
        whats = findViewById(R.id.whats)
        publish = findViewById(R.id.publish)
        info = findViewById(R.id.info)
        val webSettings: WebSettings = features.getSettings()
        webSettings.javaScriptEnabled = true
        webSettings.userAgentString = lang.appLanguage
        title.text = getString(R.string.details_page)
        back.setOnClickListener { startActivity(Intent(this,SearchActivity::class.java))
        finish()}
        if (user.loginStatus!!) {
           // getData("Bearer"+user.userData.token)
        }else{
           // getData(null)
        }
        chat.setOnClickListener { if (user.loginStatus!!){
            getID()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }
        }
        call.setOnClickListener { getLocationWithPermission(phone) }
        whats.setOnClickListener {  what.replaceFirst("0","+966",false)
            openWhatsAppConversationUsingUri(mContext!!,what,"")  }
        publish.setOnClickListener {
            if (pupl == 1) {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.RequestDetails("Bearer"+user.userData.token,lang.appLanguage,id,0)
                        ?.enqueue(object : Callback<RequestDetailsResponse> {
                            override fun onResponse(call: Call<RequestDetailsResponse>, response: Response<RequestDetailsResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        startActivity(Intent(this@RequestDetailsActivity, SearchActivity::class.java))
                                        finish()

                                    } else {
                                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<RequestDetailsResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext, t)
                                t.printStackTrace()
                                hideProgressDialog()
                            }

                        })
            }else{
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.RequestDetails("Bearer"+user.userData.token,lang.appLanguage,id,1)
                        ?.enqueue(object : Callback<RequestDetailsResponse> {
                            override fun onResponse(call: Call<RequestDetailsResponse>, response: Response<RequestDetailsResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        startActivity(Intent(this@RequestDetailsActivity, AddingDoneActivity::class.java))
                                        finish()

                                    } else {
                                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<RequestDetailsResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext, t)
                                t.printStackTrace()
                                hideProgressDialog()
                            }

                        })
            }

        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,SearchActivity::class.java))
        finish()
    }
    fun getData(token:String?){
       // showProgressDialog(getString(R.string.please_wait))
        CommonUtil.setConfig(lang.appLanguage,mContext)
        Client.getClient()?.create(Service::class.java)?.RequestDetails(token,lang.appLanguage,id,null)
                ?.enqueue(object : Callback<RequestDetailsResponse> {
                    override fun onResponse(call: Call<RequestDetailsResponse>, response: Response<RequestDetailsResponse>) {
                       // hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                category.text = response.body()?.data?.category
                                date.text = response.body()?.data?.created
                                if (response.body()?.data?.honest==0){
                                    honest.visibility = View.GONE
                                }else{
                                    honest.visibility = View.VISIBLE
                                }
                                CommonUtil.setConfig(lang.appLanguage,mContext)
                                features.loadUrl(response.body()?.data?.features_url!!)
                                name.text = response.body()?.data?.username
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                                details.text = response.body()?.data?.details
                                phone = response.body()?.data?.phone!!
                                what = response.body()?.data?.phone!!
                                use = response.body()?.data?.user!!
                                pupl = response.body()?.data?.published!!
                                if (response.body()?.data?.show_info==1){
                                    info.visibility = View.VISIBLE
                                }else{
                                    info.visibility = View.GONE
                                }
                                if (response.body()?.data?.published==1){
                                    publish.visibility = View.GONE
                                }else{
                                    publish.visibility = View.VISIBLE
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<RequestDetailsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        //hideProgressDialog()
                    }

                })
    }
    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Conversation("Bearer"+user.userData.token,lang.appLanguage,use,id)?.enqueue(object :
                Callback<ConversationIdResponse> {
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                    call: Call<ConversationIdResponse>,
                    response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@RequestDetailsActivity, ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                            mContext as Activity, PermissionUtils.CALL_PHONE,
                            300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }
    fun openWhatsAppConversationUsingUri(
            context: Context,
            numberWithCountryCode: String,
            message: String
    ) {

        val uri =
                Uri.parse("https://api.whatsapp.com/send?phone=$numberWithCountryCode&text=$message")

        val sendIntent = Intent(Intent.ACTION_VIEW, uri)

        context.startActivity(sendIntent)
    }

}