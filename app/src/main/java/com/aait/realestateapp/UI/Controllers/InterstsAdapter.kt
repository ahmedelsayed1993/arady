package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.InterstesModel
import com.aait.realestateapp.Models.OrdersModel
import com.aait.realestateapp.R

class InterstsAdapter  (context: Context, data: MutableList<InterstesModel>, layoutId: Int) :
        ParentRecyclerAdapter<InterstesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.city.text = mcontext.getString(R.string.city)+": "+listModel.city
        viewHolder.neighborhood.text = mcontext.getString(R.string.neighborhood)+": "+listModel.neighborhood
        viewHolder.category.text = mcontext.getString(R.string.category)+": "+listModel.category
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.delete.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var city=itemView.findViewById<TextView>(R.id.city)
        internal var category = itemView.findViewById<TextView>(R.id.category)
        internal var delete = itemView.findViewById<ImageView>(R.id.delete)
        internal var neighborhood = itemView.findViewById<TextView>(R.id.neighborhood)



    }

}