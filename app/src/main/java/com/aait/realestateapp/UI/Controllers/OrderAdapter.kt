package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.EstatesModel
import com.aait.realestateapp.Models.OrdersModel
import com.aait.realestateapp.R
import com.bumptech.glide.Glide

class OrderAdapter (context: Context, data: MutableList<OrdersModel>, layoutId: Int) :
        ParentRecyclerAdapter<OrdersModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.title!!.setText(listModel.category)
        viewHolder.price.text = listModel.price+mcontext.getString(R.string.Rial)
        viewHolder.city.text = listModel.city
        viewHolder.neighborhood.text = listModel.neighborhoods
        viewHolder.applicant.text = listModel.applicant
        if (listModel.honest==0){
            viewHolder.honest.visibility = View.GONE
        }else{
            viewHolder.honest.visibility = View.VISIBLE
        }

        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var title=itemView.findViewById<TextView>(R.id.title)
        internal var city = itemView.findViewById<TextView>(R.id.city)
        internal var price = itemView.findViewById<TextView>(R.id.price)
        internal var neighborhood = itemView.findViewById<TextView>(R.id.neighborhood)
        internal var applicant = itemView.findViewById<TextView>(R.id.applicant)
        internal var honest = itemView.findViewById<TextView>(R.id.honest)


    }

}