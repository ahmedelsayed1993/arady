package com.aait.realestateapp.UI.Activities.AddEstate


import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Controllers.ImagesAdapter
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.google.gson.Gson
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream


class AddImageActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_add_ads_photos
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var add:ImageView
    lateinit var images:RecyclerView
    lateinit var next:Button
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options :Options= Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(8)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
           // .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
        .setVideoDurationLimitinSeconds(30)
        .setMode(Options.Mode.All)
    internal var options1 :Options= Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(8)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        // .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
        .setVideoDurationLimitinSeconds(30)
        .setMode(Options.Mode.Picture)
    internal var options2 :Options= Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(8)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        // .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
        .setVideoDurationLimitinSeconds(30)
        .setMode(Options.Mode.Video)
    var ImageBasePaths = ArrayList<String>()
    lateinit var imagesAdapter:ImagesAdapter
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var listModel: ListModel
    var marketing = 0
    lateinit var upload_images:Button
    lateinit var upload_video:Button
    override fun initializeComponents() {
        marketing = intent.getIntExtra("marketing", 0)
        listModel = intent.getSerializableExtra("cat") as ListModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        add = findViewById(R.id.add)
        upload_images = findViewById(R.id.upload_images)
        upload_video = findViewById(R.id.upload_video)
        images = findViewById(R.id.images)
        next = findViewById(R.id.next)
        imagesAdapter = ImagesAdapter(mContext, ArrayList<String>(), R.layout.recycle_images)
        gridLayoutManager = GridLayoutManager(mContext, 2)
        images.layoutManager = gridLayoutManager
        images.adapter = imagesAdapter
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Upload_ad_pictures)
        add.setOnClickListener { upload_video.visibility = View.VISIBLE
        upload_images.visibility = View.VISIBLE}
        upload_video.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(
                        mContext,
                        android.Manifest.permission.CAMERA
                    )&& PermissionUtils.hasPermissions(
                        mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    )&& PermissionUtils.hasPermissions(
                        mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options2)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options2)
            }
        }
        upload_images.setOnClickListener {  val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_add)
            val camera = dialog?.findViewById<LinearLayout>(R.id.camera)
            val gallery = dialog?.findViewById<LinearLayout>(R.id.gallery)
            camera.setOnClickListener { dialog.dismiss()
                if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                    if (!(PermissionUtils.hasPermissions(
                            mContext,
                            android.Manifest.permission.CAMERA
                        )&& PermissionUtils.hasPermissions(
                            mContext,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE
                        )&& PermissionUtils.hasPermissions(
                            mContext,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                                )) {
                        CommonUtil.PrintLogE("Permission not granted")
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                            )
                        }
                    } else {
                        Pix.start(this, options1)
                        CommonUtil.PrintLogE("Permission is granted before")
                    }
                } else {
                    CommonUtil.PrintLogE("SDK minimum than 23")
                    Pix.start(this, options1)
                }
            }
            gallery.setOnClickListener { dialog.dismiss()
                val intent = Intent()
                intent.type = "image/*"
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                intent.action = Intent.ACTION_GET_CONTENT
                previewRequest.launch(intent)
//                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)
//                val intent= Intent(mContext, MiraFilePickerActivity::class.java)
//                intent.putExtra("multiple", true)
//                intent.putExtra("type", "image/*")
//                previewRequest.launch(intent)
            }
            dialog.show()
        }
        next.setOnClickListener {

                  val intent = Intent(this, AdsLocationActivity::class.java)
                  intent.putExtra("cat", listModel)
                  intent.putExtra("imgs", ImageBasePaths)
                  intent.putExtra("marketing", marketing)
                  startActivity(intent)


        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

            if (requestCode == 100) {
                if (resultCode == 0) {

                } else {
                    returnValue?.clear()
                    returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                    for (i in 0..returnValue!!.size-1){
                        ImageBasePaths.add(returnValue!![i])
                    }
                    if(ImageBasePaths.size!=0){

                        images.visibility = View.VISIBLE
                        imagesAdapter.updateAll(ImageBasePaths)
                    }
                    Log.e("pathsss", Gson().toJson(returnValue))

                }
            }
    }
    val previewRequest =  registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if ( it.resultCode == Activity.RESULT_OK && it.data != null){
            if (it.resultCode == Activity.RESULT_OK) {

                var files: File? = null
                if (it.data?.data != null) {


                    ImageBasePaths?.add(saveFileInStorage(mContext, it.data?.data!!))

                    if(ImageBasePaths.size!=0){

                        images.visibility = View.VISIBLE
                        imagesAdapter.updateAll(ImageBasePaths)
                    }
                }
                if (it.data?.clipData != null) {
                    for (i in 0 until it.data?.clipData?.itemCount!!) {
                        val uri: Uri = it.data?.clipData?.getItemAt(i)?.uri!!

                        ImageBasePaths?.add(saveFileInStorage(mContext,uri))
                        // Log.e("file",convertToBase64(file))
                    }

                    if(ImageBasePaths.size!=0){

                        images.visibility = View.VISIBLE
                        imagesAdapter.updateAll(ImageBasePaths)
                    }
                }
            }
        }
    }

    fun saveFileInStorage(mContext: Context, uri: Uri): String {
        var path = ""
//        Thread {
        var file: File? = null
        try {
            val mimeType: String? = mContext.contentResolver.getType(uri)
            if (mimeType != null) {
                val inputStream: InputStream? = mContext.contentResolver.openInputStream(uri)
                val fileName = getFileName(mContext,uri)
                if (fileName != "") {
                    file = File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)?.getAbsolutePath().toString() + "/" + fileName)
                    val output: OutputStream = FileOutputStream(file)
                    try {
                        val buffer =
                            ByteArray(inputStream?.available()!!) // or other buffer size
                        var read: Int = 0
                        while (inputStream.read(buffer).also({ read = it }) != -1) {
                            output.write(buffer, 0, read)
                        }
                        output.flush()
                        path = file.getAbsolutePath() //use this path
                    } finally {
                        output.close()
                    }
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
//        }.start()
        return path
    }
    @SuppressLint("Range")
    fun getFileName(mContext: Context, uri: Uri): String {
        // The query, since it only applies to a single document, will only return
        // one row. There's no need to filter, sort, or select fields, since we want
        // all fields for one document.
        var displayName = ""
        var cursor: Cursor? = null
        cursor = mContext.contentResolver.query(uri, null, null, null, null, null)
        try {
            // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
            // "if there's anything to look at, look at it" conditionals.
            if (cursor != null && cursor.moveToFirst()) {

                // Note it's called "Display Name".  This is
                // provider-specific, and might not necessarily be the file name.
                displayName = cursor.getString(
                    cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                )
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            cursor?.close()
        }
        return displayName
    }

}