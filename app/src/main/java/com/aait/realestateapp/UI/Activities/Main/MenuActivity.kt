package com.aait.realestateapp.UI.Activities.Main

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.TermsResponse
import com.aait.realestateapp.Models.WebResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.AddingTermsActivity
import com.aait.realestateapp.UI.Activities.AppInfo.*
import com.aait.realestateapp.UI.Activities.Auth.LoginActivity
import com.aait.realestateapp.UI.Activities.Auth.ProfileActivity
import com.aait.realestateapp.UI.Activities.SplashActivity
import com.aait.realestateapp.Utils.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MenuActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_menu
    lateinit var logout:TextView
    lateinit var services:LinearLayout
    lateinit var myAds:LinearLayout
    lateinit var about_app:LinearLayout
    lateinit var contact_us:LinearLayout
    lateinit var today_ads:LinearLayout
    lateinit var special:LinearLayout
    lateinit var construction:LinearLayout
    lateinit var exclusive:LinearLayout
    lateinit var fav:LinearLayout
    lateinit var marketing:LinearLayout
    lateinit var back: ImageView
    lateinit var ar:TextView
    lateinit var en:TextView
    lateinit var main:LinearLayout
    lateinit var profile:LinearLayout
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var finance:LinearLayout

    lateinit var one:TextView
    lateinit var two:TextView
    lateinit var three:TextView
    lateinit var four:TextView
    lateinit var five:TextView
    lateinit var six:TextView
    lateinit var seven:TextView
    lateinit var eight:TextView
    lateinit var nine:TextView
    lateinit var ten:TextView
    lateinit var eleven:TextView
    lateinit var twelve:TextView
    var link = ""
    override fun initializeComponents() {
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        five = findViewById(R.id.five)
        six = findViewById(R.id.six)
        seven = findViewById(R.id.seven)
        eight = findViewById(R.id.eight)
        nine = findViewById(R.id.nine)
        ten = findViewById(R.id.ten)
        eleven = findViewById(R.id.eleven)
        twelve = findViewById(R.id.twelve)
        back = findViewById(R.id.back)
        ar = findViewById(R.id.ar)
        en = findViewById(R.id.en)
        main = findViewById(R.id.main)
        profile = findViewById(R.id.profile)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        logout = findViewById(R.id.logout)
        services = findViewById(R.id.services)
        myAds = findViewById(R.id.myAds)
        about_app = findViewById(R.id.about_app)
        contact_us = findViewById(R.id.contact_us)
        today_ads = findViewById(R.id.today_ads)
        special = findViewById(R.id.special)
        construction = findViewById(R.id.construction)
        exclusive = findViewById(R.id.exclusive)
        fav = findViewById(R.id.fav)
        marketing = findViewById(R.id.marketing)
        finance = findViewById(R.id.finance)
        Client.getClient()?.create(Service::class.java)?.WebLinks(lang.appLanguage)?.enqueue(object : Callback<WebResponse> {
            override fun onResponse(call: Call<WebResponse>, response: Response<WebResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        link = response.body()?.data?.loan_calculator!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<WebResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })

        main.setOnClickListener { startActivity(Intent(this,SearchActivity::class.java)) }
        profile.setOnClickListener { startActivity(Intent(this,ProfileActivity::class.java)) }
        fav.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,FavouriteActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }
        }
        back.setOnClickListener { startActivity(Intent(this,SearchActivity::class.java))
        finish()}
        ar.setOnClickListener { lang.appLanguage = "ar"
        finishAffinity()
        startActivity(Intent(this,SplashActivity::class.java))}
        en.setOnClickListener { lang.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this,SplashActivity::class.java))}
        finance.setOnClickListener { val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(link)
            startActivity(i) }
        marketing.setOnClickListener { startActivity(Intent(this,MarketingAgrementActivity::class.java)) }
        if (user.loginStatus!!){

            logout.text = getString(R.string.logout)
            profile.visibility = View.VISIBLE
            name.text = user.userData.name
            Glide.with(mContext).load(user.userData.avatar).into(image)
        }else{
            logout.text = getString(R.string.sign_in)
            profile.visibility = View.GONE



        }
        logout.setOnClickListener {
            if (user.loginStatus!!){
              logout()
            }else{
                startActivity(Intent(this,LoginActivity::class.java))
                finish()
            }
        }
        exclusive.setOnClickListener { if (user.loginStatus!!) {
            val intent = Intent(this, AddingTermsActivity::class.java)
            intent.putExtra("type", "add")
            intent.putExtra("marketing", 1)
            startActivity(intent)
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }}
        construction.setOnClickListener { startActivity(Intent(this,ContrastingActivity::class.java)) }
        special.setOnClickListener { startActivity(Intent(this,SpecialEstatesActivity::class.java)) }
        today_ads.setOnClickListener { startActivity(Intent(this,TodayEstatesActivty::class.java)) }
        myAds.setOnClickListener { if (user.loginStatus!!){startActivity(Intent(this,MyAdsActivity::class.java))}
        else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }}
        services.setOnClickListener {
            startActivity(Intent(this,ServicesActivity::class.java))
        }
        about_app.setOnClickListener { startActivity(Intent(this,AboutAppActivity::class.java)) }
        contact_us.setOnClickListener { startActivity(Intent(this,ContactActivity::class.java)) }
    }

    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut("Bearer "+user.userData.token,user.userData.device_id!!,"android",user.userData.mac_address!!,lang.appLanguage)?.enqueue(object :
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus=false
                        user.Logout()
                        CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(this@MenuActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        user.loginStatus=false
                        user.Logout()
                        // CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(this@MenuActivity, SplashActivity::class.java))
                        finish()

                    }
                }
            }
        })
    }
}