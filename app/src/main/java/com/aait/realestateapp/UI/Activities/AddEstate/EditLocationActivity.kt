package com.aait.realestateapp.UI.Activities.AddEstate

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.EditEstateResponse
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.ListResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.LocationActivity
import com.aait.realestateapp.UI.Views.ListDialog
import com.aait.realestateapp.UI.Views.SearchDialog
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditLocationActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_ads_location
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var location: TextView
    lateinit var city: TextView
    lateinit var neighborhood: TextView
    lateinit var next: Button
    var mLang = ""
    var mLat = ""
    var result = ""
    var listModels = ArrayList<ListModel>()
    lateinit var listDialog: SearchDialog
    lateinit var listModel: ListModel
    lateinit var listModel1: ListModel
    var selected = 0
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        location = findViewById(R.id.location)
        city = findViewById(R.id.city)
        neighborhood = findViewById(R.id.neighborhood)
        next = findViewById(R.id.next)
        back.setOnClickListener {
            onBackPressed()
            finish()
        }
        title.text = getString(R.string.Determine_the_location_of_the_ad)
        location.setOnClickListener { startActivityForResult(
            Intent(this,
                LocationActivity::class.java),1) }
        city.setOnClickListener {
            selected = 0
            getCity()
        }
        neighborhood.setOnClickListener {
            if (CommonUtil.checkTextError(city,getString(R.string.city))){
                return@setOnClickListener
            }else{
                selected = 1
                getNeighborhood(listModel.id!!)
            }
        }
        getEstate()
        next.setOnClickListener {
            if (CommonUtil.checkTextError(location,getString(R.string.Determine_the_location_of_the_ad))||
                CommonUtil.checkTextError(city,getString(R.string.city))||
                CommonUtil.checkTextError(neighborhood,getString(R.string.neighborhood))){
                return@setOnClickListener
            }else {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.EditEstate(lang.appLanguage,"Bearer"+user.userData.token,id
                    ,location.text.toString(),mLat,mLang,listModel.id,listModel1.id,null,null,null)?.enqueue(object :Callback<EditEstateResponse>{
                    override fun onResponse(
                        call: Call<EditEstateResponse>,
                        response: Response<EditEstateResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,getString(R.string.data_updated))
                                onBackPressed()
                                finish()

                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<EditEstateResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                })
            }
        }
    }

    fun getCity(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCities(lang.appLanguage)
            ?.enqueue(object : Callback<ListResponse> {
                override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            listModels = response.body()?.data!!
                            listDialog = SearchDialog(mContext,this@EditLocationActivity,listModels,getString(R.string.city))
                            listDialog.show()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

            })
    }
    fun getNeighborhood(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getNeighbors(lang.appLanguage,id)
            ?.enqueue(object : Callback<ListResponse> {
                override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            listModels = response.body()?.data!!
                            listDialog = SearchDialog(mContext,this@EditLocationActivity,listModels,getString(R.string.neighborhood))
                            listDialog.show()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

            })
    }


    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            listDialog.dismiss()
            if (selected==0){
                listModel = listModels.get(position)
                city.text = listModel.name
            }else{
                listModel1 = listModels.get(position)
                neighborhood.text = listModel1.name
            }
        }

    }
    fun getEstate(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditEstate(lang.appLanguage,"Bearer"+user.userData.token,id
            ,null,null,null,null,null,null,null,null)?.enqueue(object :Callback<EditEstateResponse>{
            override fun onResponse(
                call: Call<EditEstateResponse>,
                response: Response<EditEstateResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listModel  = ListModel(response.body()?.data?.city_id,response.body()?.data?.city)
                        listModel1 = ListModel(response.body()?.data?.neighborhood_id,response.body()?.data?.neighborhood)
                        city.text = listModel.name
                        neighborhood.text = listModel1.name
                        location.text = response.body()?.data?.address
                        mLat = response.body()?.data?.lat!!
                        mLang = response.body()?.data?.lng!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<EditEstateResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

        })
    }

    override fun onTextChanged(view: View, position: Int) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result")!!
                mLat = data?.getStringExtra("lat")!!
                mLang = data?.getStringExtra("lng")!!
                location.text = result
            } else {
                result = result
                mLat = mLat
                mLang = mLang
                location.text = result
            }
        }
    }
}