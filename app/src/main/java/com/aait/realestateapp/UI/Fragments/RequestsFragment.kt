package com.aait.realestateapp.UI.Fragments

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.BaseFragment
import com.aait.realestateapp.GPS.GPSTracker
import com.aait.realestateapp.GPS.GpsTrakerListener
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.HomeResponse
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.OrdersModel
import com.aait.realestateapp.Models.OrdersResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.RequestDetailsActivity
import com.aait.realestateapp.UI.Controllers.CategoryAdapter
import com.aait.realestateapp.UI.Controllers.CatsAdapter
import com.aait.realestateapp.UI.Controllers.OrderAdapter
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.DialogUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class RequestsFragment:BaseFragment(),OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.fargment_home_orders
    companion object {
        fun newInstance(): RequestsFragment {
            val args = Bundle()
            val fragment = RequestsFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var offers:Button
    lateinit var cats:RecyclerView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var categoryAdapter: CatsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var categories = ArrayList<ListModel>()
    lateinit var listModel: ListModel
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var orderAdapter: OrderAdapter
    var orders = ArrayList<OrdersModel>()
    override fun initializeComponents(view: View) {
        lang.appLanguage = lang.appLanguage
         offers = view.findViewById(R.id.offers)
        offers.setOnClickListener {
            val nextFrag: SearchFragment = SearchFragment.newInstance()
            activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.home_fragment_container, nextFrag, "findThisFragment")
                    ?.addToBackStack(null)
                    ?.commit()
        }
        cats = view.findViewById(R.id.cats)
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext!!, LinearLayoutManager.HORIZONTAL, false)
        categoryAdapter = CatsAdapter(mContext!!, categories, R.layout.recycle_categories)
        categoryAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = categoryAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL, false)
        orderAdapter = OrderAdapter(mContext!!, orders, R.layout.recycler_orders)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager1
        rv_recycle.adapter = orderAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getLocationWithPermission("orders", null)

        }
        getLocationWithPermission("orders", null)
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.cat){
            categoryAdapter.selected = position
            listModel = categories.get(position)
            categories.get(position).selected = true
            categoryAdapter.notifyDataSetChanged()
            if (categories.get(position).id==0) {
                getLocationWithPermission("orders", null)
            }else{
                getLocationWithPermission("orders", categories.get(position).id)
            }
        }else{
            val intent = Intent(activity, RequestDetailsActivity::class.java)
            intent.putExtra("id",orders.get(position).id)
            startActivity(intent)
        }
    }

    override fun onTextChanged(view: View, position: Int) {

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getLocationWithPermission(type: String, category: Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(type, category)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(type, category)
        }

    }

    internal fun getCurrentLocation(type: String, category: Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                getData(gps.getLatitude().toString(), gps.getLongitude().toString(), type, category)
                //  putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address", result)
                    }
                } catch (e: IOException) {
                }
                // googleMap.clear()
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
            }
        }
    }

    fun getData(lat: String, lng: String, type: String, category: Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Orders(lang.appLanguage, type, lat, lng, category,null)?.enqueue(object : Callback<OrdersResponse> {
            override fun onResponse(call: Call<OrdersResponse>, response: Response<OrdersResponse>) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        categories.clear()
                        categories = response.body()?.categories!!
                        categories.add(0, ListModel(0, getString(R.string.all)))
                        categoryAdapter.updateAll(categories)
                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            Log.e("data",Gson().toJson(response.body()?.data))
                            layNoItem!!.visibility = View.GONE
                            layNoInternet!!.visibility = View.GONE
                            rv_recycle.visibility = View.VISIBLE
                            orderAdapter.updateAll(response.body()?.data!!)
                        }

                    } else {
                        CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!, t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

        })
    }

}