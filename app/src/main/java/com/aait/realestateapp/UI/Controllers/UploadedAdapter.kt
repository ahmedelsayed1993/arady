package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.ImagesModel
import com.aait.realestateapp.R
import com.bumptech.glide.Glide

class UploadedAdapter (context: Context, data: MutableList<String>, layoutId: Int) :
    ParentRecyclerAdapter<String>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        Glide.with(mcontext).asBitmap().load(questionModel).into(viewHolder.image)
        viewHolder.delete.visibility = View.GONE
    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {


        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var delete=itemView.findViewById<ImageView>(R.id.delete)
    }
}