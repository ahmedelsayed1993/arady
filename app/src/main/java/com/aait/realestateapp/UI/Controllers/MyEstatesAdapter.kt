package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.EstateFeatureModel
import com.aait.realestateapp.Models.EstatesModel
import com.aait.realestateapp.R
import com.bumptech.glide.Glide

class MyEstatesAdapter (context: Context, data: MutableList<EstatesModel>, layoutId: Int) :
        ParentRecyclerAdapter<EstatesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.features!!.setText(listModel.features)
        viewHolder.price.text = listModel.price+mcontext.getString(R.string.Rial)
        viewHolder.category.text = listModel.category
        viewHolder.address.text = listModel.address
        Glide.with(mcontext).asBitmap().load(listModel.image).into(viewHolder.image)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var features=itemView.findViewById<TextView>(R.id.feature)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var price = itemView.findViewById<TextView>(R.id.price)
        internal var category = itemView.findViewById<TextView>(R.id.categor)
        internal var address = itemView.findViewById<TextView>(R.id.address)


    }

}