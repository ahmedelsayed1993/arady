package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.EstateFeatureModel
import com.aait.realestateapp.Models.OptionsModel
import com.aait.realestateapp.R
import com.bumptech.glide.Glide

class EstateFeatureAdapter (context: Context, data: MutableList<EstateFeatureModel>, layoutId: Int) :
        ParentRecyclerAdapter<EstateFeatureModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.feature!!.setText(listModel.feature)
        viewHolder.value.text = listModel.value

        if (listModel.icon.equals("")){
            viewHolder.icon.visibility = View.GONE
        }else{
            viewHolder.icon.visibility = View.VISIBLE
            Glide.with(mcontext).asBitmap().load(listModel.icon).into(viewHolder.icon)
        }

    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var feature=itemView.findViewById<TextView>(R.id.feature)
        internal var icon = itemView.findViewById<ImageView>(R.id.icon)
        internal var value = itemView.findViewById<TextView>(R.id.value)


    }

}