package com.aait.realestateapp.UI.Activities.AddEstate

import android.app.ActionBar
import android.content.Intent
import android.os.Build
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.view.get
import androidx.core.view.marginTop
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.*
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.aait.realestateapp.UI.Controllers.FeatureAdapter
import com.aait.realestateapp.UI.Views.ListDialog
import com.aait.realestateapp.UI.Views.OptionDialog
import com.aait.realestateapp.Utils.CommonUtil
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class FeaturesActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_features
     lateinit var back:ImageView
     lateinit var title:TextView
     lateinit var lay:RecyclerView
     lateinit var listModel: ListModel
     var imgs = ArrayList<String>()
    var lat = ""
    var lng = ""
    var address = ""
    var city = 0
    var neigh = 0
    var marketing = 0
//     lateinit var layParams:LinearLayout.LayoutParams
//    lateinit var relParams:RelativeLayout.LayoutParams
//    lateinit var linParams:LinearLayout.LayoutParams
    var name = ""
    var comps = ArrayList<FeaturesModel>()
    lateinit var featureAdapter: FeatureAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var next:Button
    lateinit var listDialog: OptionDialog
    var pos = 0
     var optionModels = ArrayList<OptionsModel>()
    var models = ArrayList<Model>()
    lateinit var price:EditText
    lateinit var details:EditText
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initializeComponents() {
        marketing = intent.getIntExtra("marketing",0)
        listModel = intent.getSerializableExtra("cat") as ListModel
        imgs = intent.getStringArrayListExtra("imgs") as ArrayList<String>
        lat = intent.getStringExtra("lat")!!
        lng = intent.getStringExtra("lng")!!
        address = intent.getStringExtra("address")!!
        city = intent.getIntExtra("city",0)
        neigh = intent.getIntExtra("neigh",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        next = findViewById(R.id.next)
        lay  = findViewById(R.id.lay)
        price = findViewById(R.id.price)
        details = findViewById(R.id.details)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Enter_advertisement_details)
//        layParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
//        relParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT)
//        linParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT)
//        relParams.setMargins(0,10,0,5)
//        layParams.setMargins(0,25,0,0)
//        linParams.setMargins(7,0,7,0)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        featureAdapter = FeatureAdapter(mContext,comps,R.layout.recycle_feature)
        featureAdapter.setOnItemClickListener(this)
        lay.layoutManager = linearLayoutManager
        lay.adapter = featureAdapter
        getData()

        next.setOnClickListener {
           for (i in 0 until featureAdapter.data.size){
               if (featureAdapter.data.get(i).value_id.equals("")){

               }else {
                   if (featureAdapter.data.get(i).type.equals("total_price")||featureAdapter.data.get(i).type.equals("textarea")){

                   }else {
                       models.add(
                               Model(
                                       featureAdapter.data.get(i).feature_id.toString(),
                                       featureAdapter.data.get(i).value_id,
                                       featureAdapter.data.get(i).type
                               )
                       )
                   }
               }
           }
            Log.e("model",Gson().toJson(models))
            if (CommonUtil.checkEditError(price,getString(R.string.total_price))){
                return@setOnClickListener
            }else{
                if (models.isEmpty()){
                    CommonUtil.makeToast(mContext,getString(R.string.Enter_advertisement_details))
                }else{
                    if (imgs.isEmpty()){
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.AddProperty(lang.appLanguage, "Bearer" + user.userData.token, listModel.id!!, address, lat, lng, city, neigh, price.text.toString(), details.text.toString(), Gson().toJson(models),marketing)
                                ?.enqueue(object : Callback<AddEstateResponse> {
                                    override fun onResponse(
                                            call: Call<AddEstateResponse>,
                                            response: Response<AddEstateResponse>
                                    ) {
                                        hideProgressDialog()
                                        if (response.isSuccessful) {
                                            if (response.body()?.value.equals("1")) {
                                                val intent = Intent(this@FeaturesActivity, AdvertisementReviewActivity::class.java)
                                                intent.putExtra("id",response.body()?.data)
                                                startActivity(intent)
                                                finish()
                                            } else {
                                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<AddEstateResponse>, t: Throwable) {
                                        hideProgressDialog()
                                        CommonUtil.handleException(mContext, t)
                                        t.printStackTrace()
                                    }

                                })
                    }else {
                        var images = ArrayList<MultipartBody.Part>()
                        for (i in 0..imgs.size - 1) {
                            var filePart: MultipartBody.Part? = null
                            val ImageFile = File(imgs.get(i))
                            val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
                            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.name, fileBody)
                            images.add(filePart)
                        }
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.AddProperty(lang.appLanguage, "Bearer" + user.userData.token, listModel.id!!, address, lat, lng, city, neigh, price.text.toString(), details.text.toString(), Gson().toJson(models), images,marketing)
                                ?.enqueue(object : Callback<AddEstateResponse> {
                                    override fun onResponse(
                                            call: Call<AddEstateResponse>,
                                            response: Response<AddEstateResponse>
                                    ) {
                                        hideProgressDialog()
                                        if (response.isSuccessful) {
                                            if (response.body()?.value.equals("1")) {
                                                val intent = Intent(this@FeaturesActivity, AdvertisementReviewActivity::class.java)
                                                intent.putExtra("id",response.body()?.data)
                                                startActivity(intent)
                                            } else {
                                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<AddEstateResponse>, t: Throwable) {
                                        hideProgressDialog()
                                        CommonUtil.handleException(mContext, t)
                                        t.printStackTrace()
                                    }

                                })
                    }
                }
            }
        }

    }

//    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//    fun Draw(components:ArrayList<FeaturesModel>){
//        for (i in 0..components.size-1){
//            if (components.get(i).type.equals("number")){
//                val lay = RelativeLayout(mContext)
////                lay.background = mContext.resources.getDrawable(R.drawable.white_blue_shape)
//                lay.layoutParams = relParams
//                body.put(components.get(i).feature_id!!,EditText(mContext))
//                val edit = EditText(mContext)
//                edit.id = components.get(i).feature_id!!
//                edit.tag = 1
//                edit.background = mContext.resources.getDrawable(R.drawable.white_blue_shape)
//                edit.setTextColor(mContext.resources.getColor(R.color.colorPrimary))
//                edit.setPadding(10,15,10,5)
//                edit.inputType = InputType.TYPE_CLASS_NUMBER
//                edit.textSize = 13F
//                edit.layoutParams = layParams
//                lay.addView(edit)
//                relParams.addRule(RelativeLayout.ABOVE,edit.id)
//                val text = TextView(mContext)
//                text.text = components.get(i).name
//                text.setTextColor(mContext.resources.getColor(R.color.colorPrimaryDark))
//                text.background = mContext.resources.getDrawable(R.drawable.white_background)
//                text.setPadding(3,3,3,3)
//                text.layoutParams = linParams
//                lay.addView(text)
//                layout.addView(lay)
//
//
//            }else if (components.get(i).type.equals("switch")){
//                body.put(components.get(i).feature_id!!,Switch(mContext))
//                var lay = LinearLayout(mContext)
//                lay.layoutParams = layParams
//                lay.orientation = LinearLayout.HORIZONTAL
//                lay.background = mContext.resources.getDrawable(R.drawable.white_blue_shape)
//                lay.setPadding(10,10,10,10)
//                var text = TextView(mContext)
//                text.width = 300
//                text.text = components.get(i).name
//                text.setTextColor(mContext.resources.getColor(R.color.colorPrimaryDark))
//                text.background = mContext.resources.getDrawable(R.drawable.white_background)
//                text.layoutParams = linParams
//                lay.addView(text)
//                lateinit var sw:Switch
//                sw = Switch(mContext)
//                sw.id = components.get(i).feature_id!!
//                sw.tag = 2
//                lay.addView(sw)
//                layout.addView(lay)
//
//            }else if (components.get(i).type.equals("radio")){
//                val text = TextView(mContext)
//                text.text = components.get(i).name
//                text.setTextColor(mContext.resources.getColor(R.color.colorPrimaryDark))
//                text.background = mContext.resources.getDrawable(R.drawable.white_background)
//                text.setPadding(3,3,3,3)
//                text.layoutParams = linParams
//                layout.addView(text)
//                var lay = RadioGroup(mContext)
//                lay.layoutParams = layParams
//                lay.orientation = LinearLayout.HORIZONTAL
//                lay.background = mContext.resources.getDrawable(R.drawable.white_blue_shape)
//                lay.setPadding(10,10,10,10)
//
//                for (j in 0 until components[i].options!!.size) {
//                    body.put(components.get(i).feature_id!!,EditText(mContext))
//                    lateinit var radio: RadioButton
//                    radio = RadioButton(mContext)
//                    radio.id = components.get(i).feature_id!!
//
//                    radio.text = components[i]?.options?.get(j)?.name
//                    lay.addView(radio)
//                }
//                layout.addView(lay)
//            }
//        }
//
//    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Features(lang.appLanguage,"Bearer"+user.userData.token,listModel.id!!)
                ?.enqueue(object : Callback<FeatureResponse>{
                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun onResponse(call: Call<FeatureResponse>, response: Response<FeatureResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                              featureAdapter.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<FeatureResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
       if (view.id == R.id.name){
            listDialog.dismiss()
            featureAdapter.data.get(pos).value = optionModels.get(position).name
            featureAdapter.data.get(pos).value_id = optionModels.get(position).id.toString()
            featureAdapter.notifyItemChanged(pos)

        }else{

               if (comps.get(position).type.equals("select")) {
                   pos = position
                   optionModels = comps.get(position)?.options!!
                   listDialog = OptionDialog(mContext, this, optionModels, comps.get(position).name!!)
                   listDialog.show()
               }

       }
    }

    override fun onTextChanged(view: View, position: Int) {

    }
}