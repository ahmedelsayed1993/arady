package com.aait.realestateapp.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.BaseFragment
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ChatsModel
import com.aait.realestateapp.Models.ChatsResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.ChatActivity
import com.aait.realestateapp.UI.Controllers.ConversationsAdapter
import com.aait.realestateapp.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChatsFragment:BaseFragment(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.fragment_chat
    companion object {
        fun newInstance(): ChatsFragment {
            val args = Bundle()
            val fragment = ChatsFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
     lateinit var linearLayoutManager: LinearLayoutManager
     var chatModels = java.util.ArrayList<ChatsModel>()
     lateinit var conversationsAdapter: ConversationsAdapter

    override fun initializeComponents(view: View) {

       lang.appLanguage = lang.appLanguage
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        conversationsAdapter =  ConversationsAdapter(mContext!!,chatModels,R.layout.recycler_conversations)
        conversationsAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = conversationsAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        if (user.loginStatus!!) {
            swipeRefresh!!.setOnRefreshListener { getHome() }
            getHome()
        }else{
           CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }


    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Conversations(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object :
                Callback<ChatsResponse> {
            override fun onResponse(call: Call<ChatsResponse>, response: Response<ChatsResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText("لا يوجد محادثات")
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                conversationsAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<ChatsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }

    override fun onItemClick(view: View, position: Int) {

               val intent = Intent(activity, ChatActivity::class.java)
               intent.putExtra("id", chatModels.get(position).conversation_id)
               intent.putExtra("receiver", chatModels.get(position).user_id)
               intent.putExtra("lastpage", chatModels.get(position).lastPage)
               startActivity(intent)


    }

    override fun onTextChanged(view: View, position: Int) {

    }
}