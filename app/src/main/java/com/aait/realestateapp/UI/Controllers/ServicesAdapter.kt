package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.OptionsModel
import com.aait.realestateapp.Models.ServicesModel
import com.aait.realestateapp.R
import com.bumptech.glide.Glide

class ServicesAdapter (context: Context, data: MutableList<ServicesModel>, layoutId: Int) :
        ParentRecyclerAdapter<ServicesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.name!!.setText(listModel.name)
        Glide.with(mcontext).load(listModel.image).into(viewHolder.image)
        if (position%4==0){
            viewHolder.card.setCardBackgroundColor(mcontext.resources.getColor(R.color.color1))
            viewHolder.card_img.setCardBackgroundColor(mcontext.resources.getColor(R.color.color9))
            viewHolder.name.setTextColor(mcontext.resources.getColor(R.color.color5))
        }else if (position%4==1){
            viewHolder.card.setCardBackgroundColor(mcontext.resources.getColor(R.color.color2))
            viewHolder.card_img.setCardBackgroundColor(mcontext.resources.getColor(R.color.color10))
            viewHolder.name.setTextColor(mcontext.resources.getColor(R.color.color6))
        }
        else if (position%4==2){
            viewHolder.card.setCardBackgroundColor(mcontext.resources.getColor(R.color.color3))
            viewHolder.card_img.setCardBackgroundColor(mcontext.resources.getColor(R.color.color11))
            viewHolder.name.setTextColor(mcontext.resources.getColor(R.color.color7))
        }
        else if (position%4==3){
            viewHolder.card.setCardBackgroundColor(mcontext.resources.getColor(R.color.color4))
            viewHolder.card_img.setCardBackgroundColor(mcontext.resources.getColor(R.color.color12))
            viewHolder.name.setTextColor(mcontext.resources.getColor(R.color.color8))
        }
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var card = itemView.findViewById<CardView>(R.id.card)
        internal var card_img = itemView.findViewById<CardView>(R.id.car_img)
        internal var image = itemView.findViewById<ImageView>(R.id.image)


    }
}