package com.aait.realestateapp.UI.Activities.Auth

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.BaseResponse
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.AdsLocationActivity
import com.aait.realestateapp.UI.Controllers.ImagesAdapter
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class OfficeActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_add_ads_photos
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var add: ImageView
    lateinit var images: RecyclerView
    lateinit var next: Button
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(8)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
          //  .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
        .setMode(Options.Mode.Picture)
    var ImageBasePaths = ArrayList<String>()
    lateinit var imagesAdapter: ImagesAdapter
    lateinit var gridLayoutManager: GridLayoutManager

    override fun initializeComponents() {

        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        add = findViewById(R.id.add)
        images = findViewById(R.id.images)
        next = findViewById(R.id.next)
        imagesAdapter = ImagesAdapter(mContext,ArrayList<String>(), R.layout.recycle_images)
        gridLayoutManager = GridLayoutManager(mContext,2)
        images.layoutManager = gridLayoutManager
        images.adapter = imagesAdapter
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.office_documentation)
        next.text = getString(R.string.confirm)
        add.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        next.setOnClickListener {
            if (ImageBasePaths.isEmpty()){
                CommonUtil.makeToast(mContext,getString(R.string.Upload_ad_pictures))
            }else{
                var images = ArrayList<MultipartBody.Part>()
                for (i in 0..ImageBasePaths.size - 1) {
                    var filePart: MultipartBody.Part? = null
                    val ImageFile = File(ImageBasePaths.get(i))
                    val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
                    filePart = MultipartBody.Part.createFormData("images[]", ImageFile.name, fileBody)
                    images.add(filePart)
                }
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Office(lang.appLanguage,"Bearer"+user.userData.token,images)
                        ?.enqueue(object : Callback<BaseResponse>{
                            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        onBackPressed()
                                        finish()
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                                hideProgressDialog()
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                            }

                        })
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                for (i in 0..returnValue!!.size-1){
                    ImageBasePaths.add(returnValue!![i])
                }
                if(ImageBasePaths.size!=0){

                    images.visibility = View.VISIBLE
                    imagesAdapter.updateAll(ImageBasePaths)
                }
                Log.e("pathsss", Gson().toJson(returnValue))

            }
        }
    }
}