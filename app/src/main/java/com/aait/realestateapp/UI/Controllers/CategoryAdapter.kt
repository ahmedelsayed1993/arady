package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.R

class CategoryAdapter (context: Context, data: MutableList<ListModel>, layoutId: Int) :
        ParentRecyclerAdapter<ListModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    var selected :Int = 0

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.cat!!.setText(questionModel.name)
        questionModel.selected = (selected==position)
        if(questionModel.selected!!){
            viewHolder.cat.background = mcontext.getDrawable(R.drawable.green_shape)
            viewHolder.cat.textColor = mcontext.resources.getColor(R.color.colorWhite)
        }else{
            viewHolder.cat.background = mcontext.getDrawable(R.drawable.white_green_shape)
            viewHolder.cat.textColor = mcontext.resources.getColor(R.color.colorGreen)
        }

        viewHolder.cat.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var cat=itemView.findViewById<TextView>(R.id.cat)


    }
}