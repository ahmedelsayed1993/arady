package com.aait.realestateapp.UI.Activities.AddEstate

import android.content.Intent
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.EstateDetailsResponse
import com.aait.realestateapp.Models.EstateFeatureModel
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.aait.realestateapp.UI.Activities.Main.SearchActivity
import com.aait.realestateapp.UI.Controllers.EstateFeatureAdapter
import com.aait.realestateapp.UI.Controllers.SlidersAdapter
import com.aait.realestateapp.Utils.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class AdvertisementReviewActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_advertise_review
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var viewPager: CardSliderViewPager
    lateinit var category:TextView
    lateinit var price:TextView
    lateinit var address:TextView
    lateinit var features:WebView
    lateinit var ads_num:TextView
    lateinit var details:TextView
    lateinit var publish:Button
    lateinit var estateFeatureAdapter: EstateFeatureAdapter
    lateinit var gridLayoutManager: GridLayoutManager
    var estateFeatureModels = ArrayList<EstateFeatureModel>()
    var id = 0
    var puplishing = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id", 0)
        CommonUtil.setConfig(lang.appLanguage,mContext)
        val intent = Intent(this,AdvertisementReview::class.java)
        intent.putExtra("id",id)
        startActivity(intent)
        finish()
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        viewPager = findViewById(R.id.viewPager)
        category = findViewById(R.id.category)
        price = findViewById(R.id.price)
        address = findViewById(R.id.address)
        features = findViewById(R.id.features)
        address = findViewById(R.id.address)
        ads_num = findViewById(R.id.ads_num)
        details = findViewById(R.id.details)
        publish = findViewById(R.id.publish)
        val webSettings: WebSettings = features.getSettings()
        webSettings.javaScriptEnabled = true
        webSettings.userAgentString = lang.appLanguage
        //getData()
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Review_advertisement_details)
        publish.setOnClickListener {
            if (puplishing == 1) {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.EstateDetails("Bearer" + user.userData.token, lang.appLanguage, id, 0)
                        ?.enqueue(object : Callback<EstateDetailsResponse> {
                            override fun onResponse(call: Call<EstateDetailsResponse>, response: Response<EstateDetailsResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful) {
                                    if (response.body()?.value.equals("1")) {
                                        startActivity(Intent(this@AdvertisementReviewActivity, SearchActivity::class.java))
                                        finish()

                                    } else {
                                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<EstateDetailsResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext, t)
                                t.printStackTrace()
                                hideProgressDialog()
                            }

                        })
            }else{
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.EstateDetails("Bearer" + user.userData.token, lang.appLanguage, id, 1)
                        ?.enqueue(object : Callback<EstateDetailsResponse> {
                            override fun onResponse(call: Call<EstateDetailsResponse>, response: Response<EstateDetailsResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful) {
                                    if (response.body()?.value.equals("1")) {
                                        startActivity(Intent(this@AdvertisementReviewActivity, AddingDoneActivity::class.java))
                                        finish()

                                    } else {
                                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<EstateDetailsResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext, t)
                                t.printStackTrace()
                                hideProgressDialog()
                            }

                        })
            }

        }

    }


    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EstateDetails("Bearer"+user.userData.token,lang.appLanguage, id,null)
                ?.enqueue(object : Callback<EstateDetailsResponse> {
                    override fun onResponse(call: Call<EstateDetailsResponse>, response: Response<EstateDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful) {
                            if (response.body()?.value.equals("1")) {
                                initSliderAds(response.body()?.data?.images!!)
                                category.text = response.body()?.data?.category
                                price.text = response.body()?.data?.price + getString(R.string.Rial)
                                CommonUtil.setConfig(lang.appLanguage,mContext)
                                features.loadUrl(response.body()?.data?.features_url!!)
                                CommonUtil.setConfig(lang.appLanguage,mContext)
                                address.text = response.body()?.data?.address
                                ads_num.text = response.body()?.data?.number.toString()
                                details.text = response.body()?.data?.details
                                puplishing = response.body()?.data?.published!!
                                if (response.body()?.data?.published==1){
                                    publish.text = getString(R.string.unpublish)
                                }else{
                                    publish.text = getString(R.string.publish)
                                }

                            } else {
                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<EstateDetailsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                })
    }
    fun initSliderAds(list: ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE

        }
        else{
            viewPager.visibility= View.VISIBLE

            viewPager.adapter= SlidersAdapter(mContext!!, list)

        }
    }

}