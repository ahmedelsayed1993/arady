package com.aait.realestateapp.UI.Activities.Main

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.GPS.GPSTracker
import com.aait.realestateapp.GPS.GpsTrakerListener
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.*
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.AddingTermsActivity
import com.aait.realestateapp.UI.Activities.AppInfo.MyAdsActivity
import com.aait.realestateapp.UI.Activities.Auth.LoginActivity
import com.aait.realestateapp.UI.Controllers.CategoryAdapter
import com.aait.realestateapp.UI.Controllers.FeatureAdapter
import com.aait.realestateapp.UI.Fragments.ListFragment
import com.aait.realestateapp.UI.Fragments.RequestsFragment
import com.aait.realestateapp.UI.Fragments.SearchFragment
import com.aait.realestateapp.UI.Views.ListDialog
import com.aait.realestateapp.UI.Views.OptionDialog
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.DialogUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.infideap.drawerbehavior.AdvanceDrawerLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.Float
import java.util.*
import kotlin.collections.ArrayList

class SearchActivity:ParentActivity()  , OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    GpsTrakerListener, OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.search_fragment
    lateinit var order:LinearLayout
    lateinit var add:ImageView
    lateinit var chat:LinearLayout
    lateinit var menu:LinearLayout
    var models = ArrayList<Model>()
    lateinit var orders: Button
    lateinit var drawer_layout: AdvanceDrawerLayout
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    lateinit var map: MapView
    lateinit var list: CardView
    lateinit var mode: CardView
    lateinit var location:CardView
    lateinit var profile:LinearLayout
    var mLang = ""
    var mLat = ""
    var result = ""
    lateinit var cats: RecyclerView
    lateinit var categoryAdapter: CategoryAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var categories = ArrayList<ListModel>()
    lateinit var listModel: ListModel
    lateinit var result_lay: LinearLayout
    lateinit var _result: TextView
    var type = "offers"
    internal lateinit var markerOptions: MarkerOptions
    internal var hmap = HashMap<Marker, EstatesModel>()
    lateinit var search: ImageView
    lateinit var filter_lay: LinearLayout
    lateinit var features: RecyclerView
    lateinit var feLinearLayoutManager: LinearLayoutManager
    lateinit var featurAdapter: FeatureAdapter
    var featureModels = ArrayList<FeaturesModel>()
    lateinit var category: LinearLayout
    lateinit var estate_type: TextView
    lateinit var addr: EditText
    var listModels = ArrayList<ListModel>()
    lateinit var listDialog: ListDialog
    lateinit var categoryModel: ListModel
    var categoryModels = ArrayList<ListModel>()
    var pos = 0
    var optionModels = ArrayList<OptionsModel>()
    lateinit var listDialog1: OptionDialog
    var selected = 0
    lateinit var ad_lay: RelativeLayout
    lateinit var close: ImageView
    lateinit var image: ImageView
    lateinit var categor: TextView
    lateinit var price: TextView
    lateinit var feature: TextView
    lateinit var address: TextView
    lateinit var estate_lay: CardView
    lateinit var estatesModel: EstatesModel
    lateinit var lay1:CardView
    lateinit var request:LinearLayout
    lateinit var add_estate:LinearLayout
    var cat:Int?=null
    lateinit var _switch: Switch
    var sel = 0
    override fun initializeComponents() {
        Log.e("error","error")
        lang.appLanguage = lang.appLanguage
        map = findViewById(R.id.map)
        drawer_layout = findViewById(R.id.drawer_layout)
        order = findViewById(R.id.order)
        chat = findViewById(R.id.chat)
        add = findViewById(R.id.add)
        menu = findViewById(R.id.menu)
        profile = findViewById(R.id.profile)
        order.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,OrdersActivity::class.java))

        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
        }
        chat.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,ChatsActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
        }
        menu.setOnClickListener {if (user.loginStatus!!) {
            startActivity(Intent(this,MenueActivity::class.java))
        }else{
            startActivity(Intent(this,MenuActivity::class.java))
        } }
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)
        try {
            MapsInitializer.initialize(mContext!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        lay1 = findViewById(R.id.lay1)
        request = findViewById(R.id.request)
        add_estate = findViewById(R.id.add_estate)
        add.setOnClickListener {
            if (lay1.visibility == View.GONE) {
                lay1.visibility = View.VISIBLE
            }else{
                lay1.visibility = View.GONE
            }
        }
        request.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, AddingTermsActivity::class.java)
                intent.putExtra("type", "request")
                intent.putExtra("marketing", 0)
                startActivity(intent)
                lay1.visibility = View.GONE
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        add_estate.setOnClickListener { if (user.loginStatus!!){val intent = Intent(this,
            AddingTermsActivity::class.java)
            intent.putExtra("type","add")
            intent.putExtra("marketing",0)
            startActivity(intent)
            lay1.visibility = View.GONE}else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        profile.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this, MyAdsActivity::class.java))
            lay1.visibility = View.GONE
        }else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        cats = findViewById(R.id.cats)
        linearLayoutManager = LinearLayoutManager(mContext!!, LinearLayoutManager.HORIZONTAL, false)
        categoryAdapter = CategoryAdapter(mContext!!, categories, R.layout.recycle_categories)
        categoryAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = categoryAdapter
        getCategory()
        orders = findViewById(R.id.orders)
        list = findViewById(R.id.list)
        mode = findViewById(R.id.mode)
        location = findViewById(R.id.location)

        search = findViewById(R.id.search)
        filter_lay = findViewById(R.id.filter_lay)

        ad_lay = findViewById(R.id.ad_lay)
        estate_lay = findViewById(R.id.estate_lay)
        close = findViewById(R.id.close)
        image = findViewById(R.id.image)
        categor = findViewById(R.id.categor)
        price = findViewById(R.id.price)
        feature = findViewById(R.id.feature)
        address = findViewById(R.id.address)
        sideMenu()
        close.setOnClickListener { ad_lay.visibility = View.GONE }
        estate_lay.setOnClickListener { val intent = Intent(this,AdvertismentDetailsActivity::class.java)
            intent.putExtra("id",estatesModel.id)
            startActivity(intent)
        }
        list.setOnClickListener { val intent = Intent(this,ListActivity::class.java)
            intent.putExtra("lat",mLat)
            intent.putExtra("lng",mLang)
            startActivity(intent)
        }
        mode.setOnClickListener {
            if (googleMap!!.mapType== GoogleMap.MAP_TYPE_SATELLITE){
                googleMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
            }else{
                googleMap!!.mapType = GoogleMap.MAP_TYPE_SATELLITE
            }
        }
        location.setOnClickListener {
            putMapMarker(gps.latitude,gps.longitude)
        }
        orders.setOnClickListener {

            startActivity(Intent(this,RequestActivity::class.java))

        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }

    private var mAlertDialog: AlertDialog? = null
    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
        googleMap!!.setOnMarkerClickListener(this)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
//        googleMap!!.isMyLocationEnabled = true
//        googleMap!!.uiSettings.isMyLocationButtonEnabled = false
        getLocationWithPermission(type, cat)
        googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(gps.latitude, gps.longitude), 9f))
        putMapMarker(gps.latitude,gps.longitude)
        googleMap!!.setOnCameraIdleListener {
            Log.e("jjjj", "uuuuuuu")
            getData(googleMap!!.cameraPosition.target.latitude.toString(), googleMap!!.cameraPosition.target.longitude.toString(), type, cat)
        }

    }

    override fun onResume() {
        super.onResume()
        getLocationWithPermission(type, cat)
    }
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    //    fun putMapMarker(lat: Double?, log: Double?) {
//        val latLng = LatLng(lat!!, log!!)
//        myMarker = googleMap.addMarker(
//                MarkerOptions()
//                        .position(latLng)
//                        .title("موقعي")
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.pink_marker)))
//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
//    }
    fun getLocationWithPermission(type: String, category: Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(type, category)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(type, category)
        }

    }

    internal fun getCurrentLocation(type: String, category: Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                getData(gps.getLatitude().toString(), gps.getLongitude().toString(), type, category)
                //  putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address", result)
                    }
                } catch (e: IOException) {
                }
//                 googleMap.clear()
//                 putMapMarker(gps.getLatitude(), gps.getLongitude())
            }
        }
    }
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.cat){
            categoryAdapter.selected = position
            listModel = categoryModels.get(position)
            categoryModels.get(position).selected = true
            categoryAdapter.notifyDataSetChanged()
            if (categoryModels.get(position).id==0) {
                cat = null
                getLocationWithPermission(type, null)
            }else{
                cat = categoryModels.get(position).id
                getLocationWithPermission(type, categoryModels.get(position).id)
            }
        }else if (view.id == R.id.name){
            if (selected == 0) {
                listDialog.dismiss()
                categoryAdapter.selected = position
                listModel = categoryModels.get(position)
                categoryModels.get(position).selected = true
                categoryAdapter.notifyDataSetChanged()
                categoryModel = categoryModels.get(position)
                estate_type.text = categoryModel.name
                if (categoryModel.id == 0) {
                    cat = null
                    getData(cat, null)
                } else {
                    cat = categoryModels.get(position).id
                    getData(cat, null)
                }
            }else{
                listDialog1.dismiss()
                featurAdapter.data.get(pos).value = optionModels.get(position).name
                featurAdapter.data.get(pos).value_id = optionModels.get(position).id.toString()
                featurAdapter.notifyItemChanged(pos)
                getDatas(categoryModel.id, Gson().toJson(models),addr.text.toString(),sel)
            }
        }else  {
            if (featurAdapter.data.get(position).type.equals("select")) {
                pos = position
                selected = 1
                optionModels = featurAdapter.data.get(position)?.options!!
                listDialog1 = OptionDialog(mContext!!, this, optionModels, featurAdapter.data.get(position).name!!)
                listDialog1.show()
            }else if (featurAdapter.data.get(position).type.equals("text")){
                Handler().postDelayed({
                    // logo.startAnimation(logoAnimation2)
                    Handler().postDelayed({

                        if (!featurAdapter.data.get(position).value_id.toString().equals("")){
                            models.add(Model(featurAdapter.data.get(position).feature_id.toString(),featurAdapter.data.get(position).value_id,
                                featurAdapter.data.get(position).type))
                        }else{

                        }
                        getDatas(categoryModel.id, Gson().toJson(models),addr.text.toString(),sel)

                    }, 2100)
                }, 1800)
//                pos = position
//                featurAdapter.notifyItemChanged(pos)
////                for (i in 0..featurAdapter.data.size-1) {
//                    getDatas(categoryModel.id, featurAdapter.data.get(pos).feature_id, featurAdapter.data.get(pos).value_id)
////                }
            }else if (featurAdapter.data.get(position).type.equals("number")){
                Handler().postDelayed({
                    // logo.startAnimation(logoAnimation2)
                    Handler().postDelayed({

                                if (!featurAdapter.data.get(position).value_id.toString().equals("")){
                                    models.add(Model(featurAdapter.data.get(position).feature_id.toString(),featurAdapter.data.get(position).value_id,
                                        featurAdapter.data.get(position).type))
                                }else{

                                }
                                getDatas(categoryModel.id, Gson().toJson(models),addr.text.toString(),sel)

                    }, 2100)
                }, 1800)
            }else if (featurAdapter.data.get(position).type.equals("switch")){
                Log.e("tt","2")
                Handler().postDelayed({
                    // logo.startAnimation(logoAnimation2)
                    Handler().postDelayed({
                        if (!featurAdapter.data.get(position).value_id.toString().equals("")){
                            models.add(Model(featurAdapter.data.get(position).feature_id.toString(),featurAdapter.data.get(position).value_id,
                                featurAdapter.data.get(position).type))
                        }else{

                        }
                        getDatas(categoryModel.id, Gson().toJson(models),addr.text.toString(),sel)
                    }, 2100)
                }, 1800)
//                featurAdapter.switch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
//                    if (isChecked) {
//                        models.add(Model(featurAdapter.data.get(position).feature_id.toString(),featurAdapter.data.get(position).value_id,
//                            featurAdapter.data.get(position).type))
//                        getDatas(categoryModel.id, Gson().toJson(models),addr.text.toString(),sel)
//                    } else {
//
//                    }
//                })
            }else if (featurAdapter.data.get(position).type.equals("radio")){
                Log.e("tt","1")
                Handler().postDelayed({
                    // logo.startAnimation(logoAnimation2)
                    Handler().postDelayed({
                        if (!featurAdapter.data.get(position).value_id.toString().equals("")){
                            models.add(Model(featurAdapter.data.get(position).feature_id.toString(),featurAdapter.data.get(position).value_id,
                                featurAdapter.data.get(position).type))
                        }else{

                        }
                        getDatas(categoryModel.id, Gson().toJson(models),addr.text.toString(),sel)
                    }, 2100)
                }, 1800)
//                featurAdapter.radioButton.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener {  buttonView, isChecked ->
//                    if (isChecked) {
//                        models.add(Model(featurAdapter.data.get(position).feature_id.toString(),featurAdapter.data.get(position).value_id,
//                            featurAdapter.data.get(position).type))
//                        getDatas(categoryModel.id, Gson().toJson(models.distinctBy { it.feature_id }),addr.text.toString(),sel)
//                    } else {
//
//                    }
//                })
            }
        }

    }

    override fun onTextChanged(view: View, position: Int) {
        if (featurAdapter.data.get(position).type.equals("text")){

            getDatas(categoryModel.id, Gson().toJson(models),addr.text.toString(),sel)

        }
    }

    fun getData(lat: String, lng: String, type: String, category: Int?){
        Client.getClient()?.create(Service::class.java)?.Home(lang.appLanguage, type, lat, lng, category,null)?.enqueue(object :
            Callback<HomeResponse> {
            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {

                        if (response.body()?.data?.isEmpty()!!) {
                            //CommonUtil.makeToast(mContext!!,getString(R.string.sorry_no))
                            googleMap!!.clear()

                        } else {
                            googleMap!!.clear()

                            for (i in 0..response.body()?.data?.size!! - 1) {
                                addShop(response.body()?.data?.get(i)!!)
                            }

                        }

                    } else {
                        CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!, t)
                t.printStackTrace()
            }

        })
    }
    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.placeholder_full))
        )
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }
    internal fun addShop(shopModel: EstatesModel) {
        Log.e("estate",Gson().toJson(shopModel))
        markerOptions = MarkerOptions().position(
            LatLng(Float.parseFloat(shopModel.lat!!).toDouble(), Float.parseFloat(shopModel.lng!!).toDouble())
        )
            .title("")
            .icon(BitmapDescriptorFactory.fromBitmap(createStoreMarker(shopModel.price + getString(R.string.Rial))))
        myMarker = googleMap!!.addMarker(markerOptions)

//        googleMap!!.animateCamera(
//                CameraUpdateFactory.newLatLngZoom(LatLng(java.lang.Double.parseDouble(shopModel.lat!!), java.lang.Double.parseDouble(shopModel.lng!!)), 10f)
//        )
        hmap[myMarker] = shopModel

    }
    private fun createStoreMarker(text: String): Bitmap? {
        val markerLayout: View = layoutInflater.inflate(R.layout.custom_marker, null)
        val markerImage = markerLayout.findViewById<View>(R.id.marker_image) as ImageView
        val markerRating = markerLayout.findViewById<View>(R.id.marker_text) as TextView
        markerImage.setImageResource(R.mipmap.pink_marker)
        markerRating.setText(text)
        markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        markerLayout.layout(0, 0, markerLayout.measuredWidth, markerLayout.measuredHeight)
        val bitmap: Bitmap = Bitmap.createBitmap(markerLayout.measuredWidth, markerLayout.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        markerLayout.draw(canvas)
        return bitmap
    }

    override fun onMarkerClick(p0: Marker): Boolean {
        if (p0.getTitle() != "موقعى") {
            if (hmap.containsKey(p0)) {
                estatesModel = hmap[p0]!!
                ShowEstate(estatesModel!!)
            }
        }
        return false
    }
    fun ShowEstate(estatesModel: EstatesModel){
        ad_lay.visibility = View.VISIBLE
        Glide.with(mContext!!).load(estatesModel.image).into(image)
        categor.text = estatesModel.category
        price.text = estatesModel.price+getString(R.string.Rial)
        feature.text = estatesModel.features
        address.text = estatesModel.address

    }

    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 150f)
        drawer_layout.setRadius(Gravity.END, 150f)
        drawer_layout.setViewScale(Gravity.START, 1f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 1f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
//        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 10f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 10f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(2f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 0f)

        search.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }
        addr = drawer_layout.findViewById(R.id.addr)
        features = drawer_layout.findViewById(R.id.features)
        category = drawer_layout.findViewById(R.id.category)
        estate_type = drawer_layout.findViewById(R.id.estate_type)
        result_lay = drawer_layout.findViewById(R.id.result_lay)
        _result = drawer_layout.findViewById(R.id.result)
        _switch = drawer_layout.findViewById(R.id._switch)
        feLinearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        featurAdapter = FeatureAdapter(mContext!!, featureModels, R.layout.recycle_feature)
        featurAdapter.setOnItemClickListener(this)
        features.layoutManager = feLinearLayoutManager
        features.adapter = featurAdapter
        getData(cat, null)
        category.setOnClickListener {
            selected = 0
            getCat()
        }
        result_lay.setOnClickListener {
            models.clear()
            for (i in 0 until featurAdapter.data.size) {
                if (featurAdapter.data.get(i).value_id.equals("")) {

                } else {
                    if (featurAdapter.data.get(i).type.equals("total_price") || featurAdapter.data.get(i).type.equals("address_search") || featurAdapter.data.get(i).type.equals("textarea")) {

                    } else {
                        models.add(
                            Model(
                                featurAdapter.data.get(i).feature_id.toString(),
                                featurAdapter.data.get(i).value_id,
                                featurAdapter.data.get(i).type
                            )
                        )
                    }
                }
            }
            Log.e("model", Gson().toJson(models))
            if (models.isEmpty()) {
                getDatas(cat, null,addr.text.toString(),sel)
            }else{
                getDatas(cat, Gson().toJson(models),addr.text.toString(),sel)
            }
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        addr.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (models.isEmpty()) {
                    getDatas(cat, null,p0.toString(),sel)
                }else{
                    getDatas(cat, Gson().toJson(models),p0.toString(),sel)
                }
            }
        })
        _switch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                sel = 1
                getDatas(cat, null,addr.text.toString(),sel)
            } else {
                sel = 0
                getDatas(cat, null,addr.text.toString(),sel)
            }
        })


    }
    fun getData(category: Int?, feature: String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Filter(lang.appLanguage, category, feature,null,null)
            ?.enqueue(object : Callback<FilterResponse> {
                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun onResponse(call: Call<FilterResponse>, response: Response<FilterResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {

                            feLinearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
                            featurAdapter = FeatureAdapter(mContext!!, response.body()?.data!!, R.layout.recycle_feature)
                            featurAdapter.setOnItemClickListener(this@SearchActivity)
                            features.layoutManager = feLinearLayoutManager
                            features.adapter = featurAdapter
                            Log.e("feat", Gson().toJson(featurAdapter.data))
                            // featurAdapter.data.clear()
                            _result.text = response.body()?.search.toString() + getString(R.string.Advertisement)
                            if (response.body()?.estates?.isEmpty()!!) {
                                //CommonUtil.makeToast(mContext!!,getString(R.string.sorry_no))
                                googleMap!!.clear()

                            } else {
                                googleMap!!.clear()

                                for (i in 0..response.body()?.estates?.size!! - 1) {
                                    addShop(response.body()?.estates?.get(i)!!)
                                }

                            }
                        } else {
                            CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext!!, t)
                    t.printStackTrace()
                }

            })
    }

    fun getDatas(category: Int?, feature: String?,address:String?,date:Int?){
//        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Filter(lang.appLanguage, category, feature,address,date)
            ?.enqueue(object : Callback<FilterResponse> {
                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun onResponse(call: Call<FilterResponse>, response: Response<FilterResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {


                            // featurAdapter.data.clear()
                            _result.text = response.body()?.search.toString() + getString(R.string.Advertisement)
                            if (response.body()?.estates?.isEmpty()!!) {
                                //CommonUtil.makeToast(mContext!!,getString(R.string.sorry_no))
                                googleMap!!.clear()

                            } else {
                                googleMap!!.clear()

                                for (i in 0..response.body()?.estates?.size!! - 1) {
                                    addShop(response.body()?.estates?.get(i)!!)
                                }

                            }
                        } else {
                            CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext!!, t)
                    t.printStackTrace()
                }

            })
    }
    fun getCategory(){

        Client.getClient()?.create(Service::class.java)?.Category(lang.appLanguage)
            ?.enqueue(object : Callback<ListResponse> {
                override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {


                            categoryModels = response.body()?.data!!
                            categoryModels.add(0, ListModel(0, getString(R.string.all)))
                            categoryAdapter.updateAll(categoryModels)
                            listDialog = ListDialog(mContext!!, this@SearchActivity, categoryModels, getString(
                                R.string.estate_type))

                        } else {
                            CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext!!, t)
                    t.printStackTrace()
                }

            })
    }
    fun getCat(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Category(lang.appLanguage)
            ?.enqueue(object : Callback<ListResponse> {
                override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {


                            categoryModels = response.body()?.data!!
                            categoryModels.add(0, ListModel(0, getString(R.string.all)))
                            categoryAdapter.updateAll(categoryModels)
                            listDialog = ListDialog(mContext!!, this@SearchActivity, categoryModels, getString(
                                R.string.estate_type))
                            listDialog.show()
                        } else {
                            CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext!!, t)
                    t.printStackTrace()
                }

            })
    }


}