package com.aait.realestateapp.UI.Activities.Main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.EstatesModel
import com.aait.realestateapp.Models.HomeResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.AddingTermsActivity
import com.aait.realestateapp.UI.Activities.AppInfo.MyAdsActivity
import com.aait.realestateapp.UI.Activities.Auth.LoginActivity
import com.aait.realestateapp.UI.Controllers.MyEstatesAdapter
import com.aait.realestateapp.UI.Fragments.ListFragment
import com.aait.realestateapp.UI.Fragments.SearchFragment
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListActivity:ParentActivity() , OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.fragment_list
    lateinit var order: LinearLayout
    lateinit var add: ImageView
    lateinit var chat: LinearLayout
    lateinit var menu: LinearLayout
    lateinit var lay1:CardView
    lateinit var request:LinearLayout
    lateinit var add_estate:LinearLayout
    lateinit var latest: RadioButton
    lateinit var price: RadioButton
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var myEstatesAdapter: MyEstatesAdapter
    var estates = ArrayList<EstatesModel>()
    lateinit var map: CardView
    lateinit var profile:LinearLayout
    var lat = ""
    var lng = ""
    var type = "latest"
    override fun initializeComponents() {
        lang.appLanguage = lang.appLanguage

        lat = intent.getStringExtra("lat")!!
        lng = intent.getStringExtra("lng")!!
        latest = findViewById(R.id.latest)
        price = findViewById(R.id.price)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        map = findViewById(R.id.map)
         order = findViewById(R.id.order)
        chat = findViewById(R.id.chat)
        add = findViewById(R.id.add)
        menu = findViewById(R.id.menu)
        profile = findViewById(R.id.profile)
        order.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,OrdersActivity::class.java))

        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
        }
        chat.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,ChatsActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
        }
        menu.setOnClickListener {if (user.loginStatus!!) {
            startActivity(Intent(this,MenueActivity::class.java))
        }else{
            startActivity(Intent(this,MenuActivity::class.java))
        } }
        lay1 = findViewById(R.id.lay1)
        request = findViewById(R.id.request)
        add_estate = findViewById(R.id.add_estate)
        add.setOnClickListener {
            if (lay1.visibility == View.GONE) {
                lay1.visibility = View.VISIBLE
            }else{
                lay1.visibility = View.GONE
            }
        }
        profile.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this, MyAdsActivity::class.java))
            lay1.visibility = View.GONE
        }else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        request.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, AddingTermsActivity::class.java)
                intent.putExtra("type", "request")
                intent.putExtra("marketing", 0)
                startActivity(intent)
                lay1.visibility = View.GONE
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }

        add_estate.setOnClickListener { if (user.loginStatus!!){val intent = Intent(this,
            AddingTermsActivity::class.java)
            intent.putExtra("type","add")
            intent.putExtra("marketing",0)
            startActivity(intent)
            lay1.visibility = View.GONE}else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        linearLayoutManager = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL, false)
        myEstatesAdapter = MyEstatesAdapter(mContext!!, estates, R.layout.recycle_ads)
        myEstatesAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = myEstatesAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData(lat, lng, type)

        }
        getData(lat, lng, type)
        price.setOnClickListener {
            type = "price"
            getData(lat, lng, type)
        }
        latest.setOnClickListener {
            type = "latest"
            getData(lat, lng, type)
        }
        map.setOnClickListener {
            startActivity(Intent(this,SearchActivity::class.java))
            finish()
        }
    }
    fun getData(lat: String, lng: String, type: String){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Home(lang.appLanguage, "offers", lat, lng, null, type)?.enqueue(object :
            Callback<HomeResponse> {
            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            myEstatesAdapter.updateAll(response.body()?.data!!)
                        }
                    } else {
                        CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!, t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this, AdvertismentDetailsActivity::class.java)
        intent.putExtra("id",estates.get(position).id)
        startActivity(intent)
    }

    override fun onTextChanged(view: View, position: Int) {

    }
}