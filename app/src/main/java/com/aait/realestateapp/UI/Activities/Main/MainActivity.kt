package com.aait.realestateapp.UI.Activities.Main

import android.content.Intent
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.AddingTermsActivity
import com.aait.realestateapp.UI.Activities.Auth.LoginActivity
import com.aait.realestateapp.UI.Fragments.ChatsFragment
import com.aait.realestateapp.UI.Fragments.OrdersFragment
import com.aait.realestateapp.UI.Fragments.SearchFragment

class MainActivity :ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_main

    lateinit var search:LinearLayout
    lateinit var search_image:ImageView
    lateinit var search_text:TextView
    lateinit var orders:LinearLayout
    lateinit var order_image:ImageView
    lateinit var order_text:TextView
    lateinit var add:ImageView
    lateinit var chat:LinearLayout
    lateinit var chat_image:ImageView
    lateinit var chat_text:TextView
    lateinit var menu:LinearLayout
    lateinit var menu_image:ImageView
    lateinit var menu_text:TextView
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var searchFragment: SearchFragment
    internal lateinit var chatsFragment: ChatsFragment
    internal lateinit var ordersFragment: OrdersFragment
    lateinit var lay:CardView
    lateinit var request:LinearLayout
    lateinit var add_estate:LinearLayout
    override fun initializeComponents() {
        searchFragment = SearchFragment.newInstance()
        ordersFragment = OrdersFragment.newInstance()
        chatsFragment = ChatsFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, searchFragment)
        transaction!!.add(R.id.home_fragment_container,ordersFragment)
        transaction!!.add(R.id.home_fragment_container,chatsFragment)
        transaction!!.commit()
        search = findViewById(R.id.search)
        search_image = findViewById(R.id.search_image)
        search_text = findViewById(R.id.search_text)
        orders = findViewById(R.id.orders)
        order_text = findViewById(R.id.order_text)
        order_image = findViewById(R.id.order_image)
        add = findViewById(R.id.add)
        chat = findViewById(R.id.chat)
        chat_image = findViewById(R.id.chat_image)
        chat_text = findViewById(R.id.chat_text)
        menu = findViewById(R.id.menu)
        menu_image = findViewById(R.id.menu_image)
        menu_text = findViewById(R.id.menu_text)
        lay = findViewById(R.id.lay)
        request = findViewById(R.id.request)
        add_estate = findViewById(R.id.add_estate)
        showSearch()
        search.setOnClickListener {
            lay.visibility = View.GONE
            showSearch() }
        orders.setOnClickListener {
            lay.visibility = View.GONE
            if (user.loginStatus!!){showOrders()}else{

        }
        }
        chat.setOnClickListener {
            lay.visibility = View.GONE
            if (user.loginStatus!!){showChat()}else{

        }
        }
        menu.setOnClickListener {if (user.loginStatus!!) {
            startActivity(Intent(this,MenueActivity::class.java))
        }else{
            startActivity(Intent(this,MenuActivity::class.java))
        } }
        add.setOnClickListener {
            if (lay.visibility == View.GONE) {
                lay.visibility = View.VISIBLE
            }else{
                lay.visibility = View.GONE
            }
        }
        request.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, AddingTermsActivity::class.java)
                intent.putExtra("type", "request")
                intent.putExtra("marketing", 0)
                startActivity(intent)
                lay.visibility = View.GONE
            }else{
                startActivity(Intent(this,LoginActivity::class.java))
            }
        }
        add_estate.setOnClickListener { if (user.loginStatus!!){val intent = Intent(this,AddingTermsActivity::class.java)
            intent.putExtra("type","add")
            intent.putExtra("marketing",0)
            startActivity(intent)
        lay.visibility = View.GONE}else{
            startActivity(Intent(this,LoginActivity::class.java))
        }
        }
    }

    fun showSearch(){
        selected = 1
        search_image.setImageResource(R.mipmap.search_blue_active)
        search_text.textColor = Color.parseColor("#274293")
        order_text.setTextColor(mContext.resources.getColor(R.color.colorAccent))
        chat_text.setTextColor(mContext.resources.getColor(R.color.colorAccent))
        menu_text.setTextColor(mContext.resources.getColor(R.color.colorAccent))
        order_image.setImageResource(R.mipmap.file_non)
        chat_image.setImageResource(R.mipmap.gray_message_non)
        menu_image.setImageResource(R.mipmap.menu_non)
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.replace(R.id.home_fragment_container, searchFragment)
        transaction!!.commit()
    }
    fun showOrders(){
        if (user.loginStatus!!){
        selected = 1
        search_image.setImageResource(R.mipmap.search_non)
        order_text.textColor = Color.parseColor("#274293")
        search_text.setTextColor(mContext.resources.getColor(R.color.colorAccent))
        chat_text.setTextColor(mContext.resources.getColor(R.color.colorAccent))
        menu_text.setTextColor(mContext.resources.getColor(R.color.colorAccent))
        order_image.setImageResource(R.mipmap.file_blue_active)
        chat_image.setImageResource(R.mipmap.gray_message_non)
        menu_image.setImageResource(R.mipmap.menu_non)
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.replace(R.id.home_fragment_container, ordersFragment)
        transaction!!.commit()}
    }
    fun showChat(){
        if (user.loginStatus!!) {
            selected = 1
            search_image.setImageResource(R.mipmap.search_non)
            chat_text.textColor = Color.parseColor("#274293")
            order_text.setTextColor(mContext.resources.getColor(R.color.colorAccent))
            search_text.setTextColor(mContext.resources.getColor(R.color.colorAccent))
            menu_text.setTextColor(mContext.resources.getColor(R.color.colorAccent))
            order_image.setImageResource(R.mipmap.file_non)
            chat_image.setImageResource(R.mipmap.blue_message_active)
            menu_image.setImageResource(R.mipmap.menu_non)
            transaction = fragmentManager!!.beginTransaction()
            transaction!!.replace(R.id.home_fragment_container, chatsFragment)
            transaction!!.commit()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}