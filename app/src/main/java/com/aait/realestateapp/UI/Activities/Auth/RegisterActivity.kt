package com.aait.realestateapp.UI.Activities.Auth

import android.content.Intent
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.ListResponse
import com.aait.realestateapp.Models.UserResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AppInfo.UserGideActivity
import com.aait.realestateapp.UI.Views.ListDialog
import com.aait.realestateapp.Utils.CommonUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.sql.CommonDataSource
import kotlin.math.log

class RegisterActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var city:TextView
    lateinit var password:EditText
    lateinit var confirm_password:EditText
    lateinit var email:EditText
    lateinit var register:Button
    lateinit var login:TextView
    lateinit var check:LinearLayout
    lateinit var terms:TextView
    var listModels = ArrayList<ListModel>()
    lateinit var listDialog: ListDialog
    lateinit var listModel: ListModel
    var deviceID = ""
    var ID = ""
    override fun initializeComponents() {
        ID =  Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device",deviceID)
            // Log and toast

        })
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        city = findViewById(R.id.city)
        password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_pass)
        register = findViewById(R.id.register)
        login = findViewById(R.id.login)
        check = findViewById(R.id.check)
        terms = findViewById(R.id.terms)
        email = findViewById(R.id.email)
        terms.setOnClickListener { startActivity(Intent(this, UserGideActivity::class.java))  }
        register.setOnClickListener {

            if(CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(email,getString(R.string.email))||
                    ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkTextError(city,getString(R.string.Country))||
                    CommonUtil.checkEditError(password,getString(R.string.password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))){
                return@setOnClickListener
            } else{
                if (!password.text.toString().equals(confirm_password.text.toString())) {
                    CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
                } else {


                    Register()


                }
            }
        }
        city.setOnClickListener { getCountry() }
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java))
        finish()}

    }

    fun getCountry(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCountries(lang.appLanguage)
                ?.enqueue(object :Callback<ListResponse>{
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                listDialog = ListDialog(mContext,this@RegisterActivity,listModels,getString(R.string.Country))
                                listDialog.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                       hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(name.text.toString(),phone.text.toString(),email.text.toString(),listModel.id!!
                ,password.text.toString(),deviceID,"android",ID,lang.appLanguage)?.enqueue(object:
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActiviateCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            listDialog.dismiss()
            listModel = listModels.get(position)
            city.text = listModel.name
        }

    }

    override fun onTextChanged(view: View, position: Int) {

    }


}