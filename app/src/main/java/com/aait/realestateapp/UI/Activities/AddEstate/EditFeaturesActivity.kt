package com.aait.realestateapp.UI.Activities.AddEstate

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.*
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Controllers.EditFeaturesAdapter
import com.aait.realestateapp.UI.Controllers.FeatureAdapter
import com.aait.realestateapp.UI.Views.OptionDialog
import com.aait.realestateapp.Utils.CommonUtil
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class EditFeaturesActivity : ParentActivity(), OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_features
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var lay: RecyclerView
    lateinit var listModel: ListModel
    var imgs = ArrayList<String>()
    var lat = ""
    var lng = ""
    var address = ""
    var city = 0
    var neigh = 0
    var marketing = 0
    //     lateinit var layParams:LinearLayout.LayoutParams
//    lateinit var relParams:RelativeLayout.LayoutParams
//    lateinit var linParams:LinearLayout.LayoutParams
    var name = ""
    var comps = ArrayList<FeaturesModel>()
    lateinit var featureAdapter: EditFeaturesAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var next: Button
    lateinit var listDialog: OptionDialog
    var pos = 0
    var optionModels = ArrayList<OptionsModel>()
    var models = ArrayList<Model>()
    lateinit var price: EditText
    lateinit var details: EditText
    var id = 0
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initializeComponents() {
       id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        next = findViewById(R.id.next)
        lay  = findViewById(R.id.lay)
        price = findViewById(R.id.price)
        details = findViewById(R.id.details)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.Enter_advertisement_details)

        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        featureAdapter = EditFeaturesAdapter(mContext,comps, R.layout.recycle_feature)
        featureAdapter.setOnItemClickListener(this)
        lay.layoutManager = linearLayoutManager
        lay.adapter = featureAdapter
        getData()

        next.setOnClickListener {
            Log.e("features",Gson().toJson(featureAdapter.data))
            for (i in 0 until featureAdapter.data.size){
                if (featureAdapter.data.get(i).value.equals("")){

                }else {
                    if (featureAdapter.data.get(i).type.equals("total_price")||featureAdapter.data.get(i).type.equals("textarea")){

                    }else {
                        models.add(
                            Model(
                                featureAdapter.data.get(i).feature_id.toString(),
                                featureAdapter.data.get(i).value,
                                featureAdapter.data.get(i).type
                            )
                        )
                    }
                }
            }
            Log.e("model", Gson().toJson(models))
            if (CommonUtil.checkEditError(price,getString(R.string.total_price))){
                return@setOnClickListener
            }else{
                if (models.isEmpty()){
                    CommonUtil.makeToast(mContext,getString(R.string.Enter_advertisement_details))
                }else{
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.EditEstate(lang.appLanguage,"Bearer"+user.userData.token,id
                        ,null,null,null,null,null,price.text.toString(),details.text.toString(),Gson().toJson(models))?.enqueue(object :Callback<EditEstateResponse>{
                        override fun onResponse(
                            call: Call<EditEstateResponse>,
                            response: Response<EditEstateResponse>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,getString(R.string.data_updated))
                                    onBackPressed()
                                    finish()

                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                        override fun onFailure(call: Call<EditEstateResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                    })
                }
            }
        }

    }



    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditEstate(lang.appLanguage,"Bearer"+user.userData.token,id,null,null,null,null,null,null
        ,null,null)
            ?.enqueue(object : Callback<EditEstateResponse> {
                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun onResponse(call: Call<EditEstateResponse>, response: Response<EditEstateResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            featureAdapter.updateAll(response.body()?.data?.features!!)
                            price.setText(response.body()?.data?.price)
                            details.setText(response.body()?.data?.details)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<EditEstateResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

            })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.layout) {
            if (comps.get(position).type.equals("select")) {
                pos = position
                optionModels = comps.get(position)?.options!!
                listDialog = OptionDialog(mContext, this, optionModels, comps.get(position).name!!)
                listDialog.show()
            }
        }else if (view.id == R.id.name){
            listDialog.dismiss()
            featureAdapter.data.get(pos).value_id = optionModels.get(position).name
            featureAdapter.data.get(pos).value = optionModels.get(position).id.toString()
            featureAdapter.notifyItemChanged(pos)

        }
    }

    override fun onTextChanged(view: View, position: Int) {

    }
}