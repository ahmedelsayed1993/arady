package com.aait.realestateapp.UI.Activities.Main

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.InterstesModel
import com.aait.realestateapp.Models.InterstesResponse
import com.aait.realestateapp.Models.TermsResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Controllers.InterstsAdapter
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddTypeActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_type
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var types:RecyclerView
    lateinit var add:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var interstsAdapter: InterstsAdapter
    var intestes = ArrayList<InterstesModel>()
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        types = findViewById(R.id.types)
        add = findViewById(R.id.add)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.estate_type)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        interstsAdapter = InterstsAdapter(mContext,intestes,R.layout.recycle_intersted)
        interstsAdapter.setOnItemClickListener(this)
        types.layoutManager = linearLayoutManager
        types.adapter = interstsAdapter
        getData()
        add.setOnClickListener { startActivity(Intent(this,TypeActivity::class.java)) }


    }

    override fun onResume() {
        super.onResume()
        Client.getClient()?.create(Service::class.java)?.Interstes(lang.appLanguage,"Bearer"+user.userData.token)
                ?.enqueue(object :Callback<InterstesResponse>{
                    override fun onResponse(call: Call<InterstesResponse>, response: Response<InterstesResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                interstsAdapter.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<InterstesResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Interstes(lang.appLanguage,"Bearer"+user.userData.token)
                ?.enqueue(object :Callback<InterstesResponse>{
                    override fun onResponse(call: Call<InterstesResponse>, response: Response<InterstesResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                interstsAdapter.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<InterstesResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.delete){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DeleteInterst(lang.appLanguage,"Bearer"+user.userData.token
            ,intestes.get(position).id!!)?.enqueue(object :Callback<TermsResponse>{
                override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.data!!)
                            getData()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

            })
        }

    }

    override fun onTextChanged(view: View, position: Int) {

    }
}