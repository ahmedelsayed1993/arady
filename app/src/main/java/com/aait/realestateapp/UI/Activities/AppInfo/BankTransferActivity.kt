package com.aait.realestateapp.UI.Activities.AppInfo

import android.app.DatePickerDialog
import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.TermsResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.aait.realestateapp.UI.Activities.Main.SearchActivity
import com.aait.realestateapp.Utils.CommonUtil
import com.google.android.gms.common.api.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class BankTransferActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_bank_transfer
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var bank_name:EditText
    lateinit var phone:EditText
    lateinit var amount:EditText
    lateinit var process:EditText
    lateinit var date:TextView
    lateinit var notes:EditText
    lateinit var send:Button
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        bank_name = findViewById(R.id.bank_name)
        phone = findViewById(R.id.phone)
        amount = findViewById(R.id.amount)
        process = findViewById(R.id.process)
        date = findViewById(R.id.date)
        notes = findViewById(R.id.notes)
        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.bank_transfer)
        date.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var m = monthOfYear+1
                date.setText("" + dayOfMonth + "-" + m + "-" + year)
            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            dpd.show() }
        send.setOnClickListener {
            if (CommonUtil.checkEditError(bank_name,getString(R.string.bank_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(amount,getString(R.string.transfer_amount))||
                    CommonUtil.checkEditError(process,getString(R.string.process_type))||
                    CommonUtil.checkTextError(date,getString(R.string.payment_time))){
                return@setOnClickListener
            }else{
                sendData()

            }
        }

    }
    fun sendData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.BankTransfer(lang.appLanguage,"Bearer"+user.userData.token!!,phone.text.toString(),amount.text.toString()
        ,bank_name.text.toString(),process.text.toString(),date.text.toString(),notes.text.toString())?.enqueue(object : Callback<TermsResponse> {
            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@BankTransferActivity,SearchActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })
    }
}