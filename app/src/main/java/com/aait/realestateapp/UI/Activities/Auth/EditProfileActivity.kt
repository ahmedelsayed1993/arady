package com.aait.realestateapp.UI.Activities.Auth

import android.content.Intent
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.ListResponse
import com.aait.realestateapp.Models.UserResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Views.ListDialog
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class EditProfileActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_edit_profile
    lateinit var name: EditText
    lateinit var phone: EditText
    lateinit var city: TextView
    lateinit var image:ImageView
    lateinit var confirm:Button
    lateinit var back:ImageView
    lateinit var title:TextView
    var listModels = ArrayList<ListModel>()
    lateinit var listDialog: ListDialog
    lateinit var listModel: ListModel
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
           // .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    override fun initializeComponents() {
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        city = findViewById(R.id.city)
        image = findViewById(R.id.image)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        confirm = findViewById(R.id.confirm)
        city.setOnClickListener { getCountry() }
        title.text = getString(R.string.profile)
        back.setOnClickListener { onBackPressed()
        finish()}
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkTextError(city,getString(R.string.Country))){
                return@setOnClickListener
            }else{
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Profile("Bearer"+user.userData.token,lang.appLanguage,name.text.toString(),phone.text.toString(),listModel.id)
                        ?.enqueue(object : Callback<UserResponse> {
                            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                                        user.userData = response.body()?.data!!
                                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                                        name.setText( response.body()?.data?.name)
                                        // user_name.text = response.body()?.data?.name
                                        phone.setText(response.body()?.data?.phone)
                                        city.text = response.body()?.data?.country
                                        listModel = ListModel(response.body()?.data?.country_id,response.body()?.data?.country)
                                        onBackPressed()
                                        finish()
                                    }else if(response.body()?.value.equals("401")){
                                        user.loginStatus=false
                                        user.Logout()
                                        startActivity(Intent(this@EditProfileActivity, LoginActivity::class.java))
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                                hideProgressDialog()
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                            }

                        })
            }
        }
        getData()
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

    }
    fun getCountry(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCountries(lang.appLanguage)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                listDialog = ListDialog(mContext,this@EditProfileActivity,listModels,getString(R.string.Country))
                                listDialog.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Profile("Bearer"+user.userData.token,lang.appLanguage,null,null,null)
                ?.enqueue(object : Callback<UserResponse> {
                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                user.userData = response.body()?.data!!
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                                phone.setText(response.body()?.data?.phone)
                                city.text = response.body()?.data?.country
                                name.setText(response.body()?.data?.name)
                                listModel = ListModel(response.body()?.data?.country_id,response.body()?.data?.country)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            listDialog.dismiss()
            listModel = listModels.get(position)
            city.text = listModel.name
        }
    }

    fun upLoad(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.Profile("Bearer "+user.userData.token,lang.appLanguage,filePart)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                        user.userData = response.body()?.data!!
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        name.setText( response.body()?.data?.name)
                        // user_name.text = response.body()?.data?.name
                        phone.setText(response.body()?.data?.phone)
                        city.text = response.body()?.data?.country
                        listModel = ListModel(response.body()?.data?.country_id,response.body()?.data?.country)
                        onBackPressed()
                        finish()

                    }else if(response.body()?.value.equals("401")){
                        user.loginStatus=false
                        user.Logout()
                        startActivity(Intent(this@EditProfileActivity, LoginActivity::class.java))
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onTextChanged(view: View, position: Int) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }
}