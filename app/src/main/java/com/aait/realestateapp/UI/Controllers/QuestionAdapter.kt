package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.OptionsModel
import com.aait.realestateapp.Models.QuestionModel
import com.aait.realestateapp.R

class QuestionAdapter (context: Context, data: MutableList<QuestionModel>, layoutId: Int) :
        ParentRecyclerAdapter<QuestionModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.question!!.setText("--"+listModel.question)
        viewHolder.answer!!.setText(listModel.answer)


    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var question=itemView.findViewById<TextView>(R.id.question)
        internal var answer = itemView.findViewById<TextView>(R.id.answer)


    }
}