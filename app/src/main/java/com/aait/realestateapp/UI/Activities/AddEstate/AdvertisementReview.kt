package com.aait.realestateapp.UI.Activities.AddEstate

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.*
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.aait.realestateapp.UI.Activities.Main.SearchActivity
import com.aait.realestateapp.UI.Controllers.*
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.github.islamkhsh.CardSliderViewPager
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Header
import retrofit2.http.Query
import java.io.File
import java.util.ArrayList

class AdvertisementReview : ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_advertise_review
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var more:TextView
    lateinit var special:ImageView
    lateinit var viewPager: CardSliderViewPager
    lateinit var category: TextView
    lateinit var price: TextView
    lateinit var address: TextView
    lateinit var features: WebView
    lateinit var ads_num: TextView
    lateinit var details: TextView
    lateinit var publish: Button
    lateinit var lay:LinearLayout
    lateinit var edit_images:TextView
    lateinit var edit_location:TextView
    lateinit var edit_details:TextView
    lateinit var delete:TextView
    lateinit var cancel:Button
    lateinit var report:ImageView
    lateinit var report_lay:LinearLayout
    lateinit var reports:RecyclerView
    lateinit var reportsAdapter: CommentsAdapter
    lateinit var linearLayoutManager1: LinearLayoutManager
    var reportsModels=ArrayList<CommentsModel>()
    lateinit var comments:RecyclerView
    lateinit var commentsAdapter: CommentsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var commentsModels=ArrayList<CommentsModel>()
    lateinit var estateFeatureAdapter: EstateFeatureAdapter
    lateinit var gridLayoutManager: GridLayoutManager
    var estateFeatureModels = ArrayList<EstateFeatureModel>()
    var imageList = ArrayList<String>()
    var files = ArrayList<FileModel>()
    lateinit var uplolinearLayoutManager: LinearLayoutManager
    lateinit var imglinearLayoutManager: LinearLayoutManager
    lateinit var imagesAdapter:FilesAdapter
    lateinit var uploadedAdapter: UploadedAdapter
    var id = 0
    var puplishing = 0
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options : Options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(8)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        // .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
        .setVideoDurationLimitinSeconds(30)
        .setMode(Options.Mode.All)

    var ImageBasePaths = ArrayList<String>()
    var IDs = ArrayList<Int>()
    override fun initializeComponents() {
        id = intent.getIntExtra("id", 0)
        CommonUtil.setConfig(lang.appLanguage,mContext)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        more = findViewById(R.id.more)
        report = findViewById(R.id.report)
        report_lay = findViewById(R.id.report_lay)
        reports = findViewById(R.id.reports)
        viewPager = findViewById(R.id.viewPager)
        category = findViewById(R.id.category)
        price = findViewById(R.id.price)
        address = findViewById(R.id.address)
        features = findViewById(R.id.features)
        address = findViewById(R.id.address)
        ads_num = findViewById(R.id.ads_num)
        details = findViewById(R.id.details)
        publish = findViewById(R.id.publish)
        special = findViewById(R.id.special)
        lay = findViewById(R.id.lay)
        edit_images = findViewById(R.id.edit_images)
        edit_location = findViewById(R.id.edit_location)
        edit_details = findViewById(R.id.edit_details)
        delete = findViewById(R.id.delete)
        cancel = findViewById(R.id.cancel)
        comments = findViewById(R.id.comments)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        commentsAdapter = CommentsAdapter(mContext,commentsModels,R.layout.recycle_comment)
        comments.layoutManager = linearLayoutManager
        comments.adapter = commentsAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        reportsAdapter = CommentsAdapter(mContext,reportsModels,R.layout.recycle_comment)
        reports.layoutManager = linearLayoutManager1
        reports.adapter = reportsAdapter
        edit_location.setOnClickListener { val intent = Intent(this,EditLocationActivity::class.java)
        intent.putExtra("id",id)
        startActivity(intent)}
        edit_images.setOnClickListener { val intent = Intent(this,EditImagesActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)}
        edit_details.setOnClickListener { val intent = Intent(this,EditFeaturesActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)}
        more.setOnClickListener {
            if (lay.visibility==View.GONE) {
                lay.visibility = View.VISIBLE
            }else{
                lay.visibility = View.GONE
            }
        }
        special.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_archive)
            val archive = dialog?.findViewById<EditText>(R.id.archive)
            val image = dialog?.findViewById<ImageView>(R.id.image)
            val uploaded = dialog?.findViewById<RecyclerView>(R.id.uploded)
            val images = dialog?.findViewById<RecyclerView>(R.id.images)
            val save = dialog?.findViewById<Button>(R.id.save)
            image.setOnClickListener {
                if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                    if (!(PermissionUtils.hasPermissions(mContext,
                            android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                        CommonUtil.PrintLogE("Permission not granted")
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                            )
                        }
                    } else {
                        Pix.start(this, options)
                        CommonUtil.PrintLogE("Permission is granted before")
                    }
                } else {
                    CommonUtil.PrintLogE("SDK minimum than 23")
                    Pix.start(this, options)
                }
            }
            uplolinearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
            imglinearLayoutManager  = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
            uploadedAdapter = UploadedAdapter(mContext,ArrayList<String>(),R.layout.recycler_images)
            imagesAdapter = FilesAdapter(mContext,files,R.layout.recycler_images)
            imagesAdapter.setOnItemClickListener(this)
            images.layoutManager = uplolinearLayoutManager
            images.adapter = uploadedAdapter
            uploaded.layoutManager = imglinearLayoutManager
            uploaded.adapter = imagesAdapter
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.Archive(lang.appLanguage,"Bearer"+user.userData.token
            ,id,null,null)?.enqueue(object :Callback<ArchiveResponse>{
                override fun onResponse(
                    call: Call<ArchiveResponse>,
                    response: Response<ArchiveResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            archive.setText(response.body()?.data?.archive)
                            imagesAdapter.updateAll(response.body()?.data?.files!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<ArchiveResponse>, t: Throwable) {
                   CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

            })
            save.setOnClickListener {
                if (ImageBasePaths.isEmpty()){
                    if (IDs.isEmpty()){
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.Archive(lang.appLanguage,"Bearer"+user.userData.token
                            ,id,archive.text.toString(),null)?.enqueue(object :Callback<ArchiveResponse>{
                            override fun onResponse(
                                call: Call<ArchiveResponse>,
                                response: Response<ArchiveResponse>
                            ) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        dialog.dismiss()
                                        archive.setText(response.body()?.data?.archive)
                                        imagesAdapter.updateAll(response.body()?.data?.files!!)
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                        dialog.dismiss()
                                    }
                                }
                            }

                            override fun onFailure(call: Call<ArchiveResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                                hideProgressDialog()
                                dialog.dismiss()
                            }

                        })
                    }else{
                        Client.getClient()?.create(Service::class.java)?.Archive(lang.appLanguage,"Bearer"+user.userData.token
                            ,id,archive.text.toString(),IDs.joinToString(separator = ",",prefix = "",postfix = ""))?.enqueue(object :Callback<ArchiveResponse>{
                            override fun onResponse(
                                call: Call<ArchiveResponse>,
                                response: Response<ArchiveResponse>
                            ) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        archive.setText(response.body()?.data?.archive)
                                        imagesAdapter.updateAll(response.body()?.data?.files!!)
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<ArchiveResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                                hideProgressDialog()
                            }

                        })
                    }
                }else{
                    var images = ArrayList<MultipartBody.Part>()
                    for (i in 0..ImageBasePaths.size - 1) {
                        var filePart: MultipartBody.Part? = null
                        val ImageFile = File(ImageBasePaths.get(i))
                        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
                        filePart = MultipartBody.Part.createFormData("upload_files[]", ImageFile.name, fileBody)
                        images.add(filePart)
                    }
                    if (IDs.isEmpty()){
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.Archive(lang.appLanguage,"Bearer"+user.userData.token
                            ,id,archive.text.toString(),null,images)?.enqueue(object :Callback<ArchiveResponse>{
                            override fun onResponse(
                                call: Call<ArchiveResponse>,
                                response: Response<ArchiveResponse>
                            ) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        ImageBasePaths.clear()
                                        archive.setText(response.body()?.data?.archive)
                                        imagesAdapter.updateAll(response.body()?.data?.files!!)
                                        uploadedAdapter.updateAll(ImageBasePaths)
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<ArchiveResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                                hideProgressDialog()
                            }

                        })
                    }else{
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.Archive(lang.appLanguage,"Bearer"+user.userData.token
                            ,id,archive.text.toString(),IDs.joinToString(separator = ",",prefix = "",postfix = ""),images)?.enqueue(object :Callback<ArchiveResponse>{
                            override fun onResponse(
                                call: Call<ArchiveResponse>,
                                response: Response<ArchiveResponse>
                            ) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        ImageBasePaths.clear()
                                        archive.setText(response.body()?.data?.archive)
                                        imagesAdapter.updateAll(response.body()?.data?.files!!)
                                        uploadedAdapter.updateAll(ImageBasePaths)
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<ArchiveResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                                hideProgressDialog()
                            }

                        })
                    }
                }
            }
            dialog?.show()
        }
        delete.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DeleteEstate(lang.appLanguage,"Bearer"+user.userData.token
            ,id)?.enqueue(object :Callback<TermsResponse>{
                override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.data!!)
                            onBackPressed()
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

            })
        }
        cancel.setOnClickListener { lay.visibility = View.GONE}
        report.setOnClickListener {
            val intent = Intent(this, CommentsActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
        }
        val webSettings: WebSettings = features.getSettings()
        webSettings.javaScriptEnabled = true
        webSettings.userAgentString = lang.appLanguage
        getData()
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.Review_advertisement_details)
        publish.setOnClickListener {
            if (puplishing == 1) {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.EstateDetails("Bearer" + user.userData.token, lang.appLanguage, id, 0)
                    ?.enqueue(object : Callback<EstateDetailsResponse> {
                        override fun onResponse(call: Call<EstateDetailsResponse>, response: Response<EstateDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful) {
                                if (response.body()?.value.equals("1")) {
                                    startActivity(Intent(this@AdvertisementReview, SearchActivity::class.java))
                                    finish()

                                } else {
                                    CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                }
                            }
                        }

                        override fun onFailure(call: Call<EstateDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext, t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                    })
            }else{
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.EstateDetails("Bearer" + user.userData.token, lang.appLanguage, id, 1)
                    ?.enqueue(object : Callback<EstateDetailsResponse> {
                        override fun onResponse(call: Call<EstateDetailsResponse>, response: Response<EstateDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful) {
                                if (response.body()?.value.equals("1")) {
                                    startActivity(Intent(this@AdvertisementReview, AddingDoneActivity::class.java))
                                    finish()

                                } else {
                                    CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                }
                            }
                        }

                        override fun onFailure(call: Call<EstateDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext, t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                    })
            }

        }

    }

    override fun onResume() {
        super.onResume()
        Client.getClient()?.create(Service::class.java)?.EstateDetails("Bearer"+user.userData.token,lang.appLanguage, id,null)
            ?.enqueue(object : Callback<EstateDetailsResponse> {
                override fun onResponse(call: Call<EstateDetailsResponse>, response: Response<EstateDetailsResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            initSliderAds(response.body()?.data?.images!!)
                            category.text = response.body()?.data?.category
                            price.text = response.body()?.data?.price + getString(R.string.Rial)
                            CommonUtil.setConfig(lang.appLanguage,mContext)
                            features.loadUrl(response.body()?.data?.features_url!!)
                            CommonUtil.setConfig(lang.appLanguage,mContext)
                            address.text = response.body()?.data?.address
                            ads_num.text = response.body()?.data?.number.toString()
                            details.text = response.body()?.data?.details
                            puplishing = response.body()?.data?.published!!
                            if (response.body()?.data?.published==1){
                                publish.text = getString(R.string.unpublish)
                            }else{
                                publish.text = getString(R.string.publish)
                            }

                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<EstateDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

            })
        Client.getClient()?.create(Service::class.java)?.Comments(lang.appLanguage,"Bearer"+user.userData.token,id)
            ?.enqueue(object :Callback<CommentsResponse>{
                override fun onResponse(
                    call: Call<CommentsResponse>,
                    response: Response<CommentsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            commentsAdapter.updateAll(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<CommentsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

            })

    }


    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EstateDetails("Bearer"+user.userData.token,lang.appLanguage, id,null)
            ?.enqueue(object : Callback<EstateDetailsResponse> {
                override fun onResponse(call: Call<EstateDetailsResponse>, response: Response<EstateDetailsResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            initSliderAds(response.body()?.data?.images!!)
                            category.text = response.body()?.data?.category
                            price.text = response.body()?.data?.price + getString(R.string.Rial)
                            CommonUtil.setConfig(lang.appLanguage,mContext)
                            features.loadUrl(response.body()?.data?.features_url!!)
                            CommonUtil.setConfig(lang.appLanguage,mContext)
                            address.text = response.body()?.data?.address
                            ads_num.text = response.body()?.data?.number.toString()
                            details.text = response.body()?.data?.details
                            puplishing = response.body()?.data?.published!!
                            if (response.body()?.data?.published==1){
                                publish.text = getString(R.string.unpublish)
                            }else{
                                publish.text = getString(R.string.publish)
                            }

                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<EstateDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

            })
        Client.getClient()?.create(Service::class.java)?.Comments(lang.appLanguage,"Bearer"+user.userData.token,id)
            ?.enqueue(object :Callback<CommentsResponse>{
                override fun onResponse(
                    call: Call<CommentsResponse>,
                    response: Response<CommentsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            commentsAdapter.updateAll(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<CommentsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

            })

    }
    fun initSliderAds(list: ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE

        }
        else{
            viewPager.visibility= View.VISIBLE

            viewPager.adapter= SlidersAdapter(mContext!!, list)

        }
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id==R.id.delete){
            IDs.add(files.get(position).id!!)
            files.remove(files.get(position))
            imagesAdapter.notifyDataSetChanged()
            Log.e("ids",Gson().toJson(IDs))
        }
    }

    override fun onTextChanged(view: View, position: Int) {

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                for (i in 0..returnValue!!.size-1){
                    ImageBasePaths.add(returnValue!![i])
                }
                if(ImageBasePaths.size!=0){


                    uploadedAdapter.updateAll(ImageBasePaths)
                }
                Log.e("pathsss", Gson().toJson(returnValue))

            }
        }
    }

}