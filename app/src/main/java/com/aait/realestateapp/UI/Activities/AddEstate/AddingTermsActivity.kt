package com.aait.realestateapp.UI.Activities.AddEstate

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.TermsResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddingTermsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_terms
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var terms:TextView
    lateinit var accept:Button
    var type = ""
    var marketing = 0
    override fun initializeComponents() {
        type = intent.getStringExtra("type")!!
        marketing = intent.getIntExtra("marketing",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        terms = findViewById(R.id.terms)
        accept = findViewById(R.id.accept)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Advertising_Terms)
        getData()
        accept.setOnClickListener { val intent =  Intent(this,CategoryActivity::class.java)
        intent.putExtra("type",type)
            intent.putExtra("marketing",marketing)
        startActivity(intent)}
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.About(lang.appLanguage)?.enqueue(object : Callback<TermsResponse> {
            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        terms.text = response.body()?.data
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })
    }
}