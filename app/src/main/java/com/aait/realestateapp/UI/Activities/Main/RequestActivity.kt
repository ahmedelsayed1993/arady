package com.aait.realestateapp.UI.Activities.Main

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.GPS.GPSTracker
import com.aait.realestateapp.GPS.GpsTrakerListener
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.OrdersModel
import com.aait.realestateapp.Models.OrdersResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.AddingTermsActivity
import com.aait.realestateapp.UI.Activities.AddEstate.RequestDetailsActivity
import com.aait.realestateapp.UI.Activities.AppInfo.MyAdsActivity
import com.aait.realestateapp.UI.Activities.Auth.LoginActivity
import com.aait.realestateapp.UI.Controllers.CatsAdapter
import com.aait.realestateapp.UI.Controllers.OrderAdapter
import com.aait.realestateapp.UI.Fragments.RequestsFragment
import com.aait.realestateapp.UI.Fragments.SearchFragment
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.DialogUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class RequestActivity:ParentActivity(), OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.fargment_home_orders

    lateinit var offers: Button
    lateinit var cats: RecyclerView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var categoryAdapter: CatsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var categories = ArrayList<ListModel>()
    lateinit var listModel: ListModel
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    lateinit var profile:LinearLayout
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var orderAdapter: OrderAdapter
    var orders = ArrayList<OrdersModel>()
    lateinit var order: LinearLayout
    lateinit var add: ImageView
    lateinit var chat: LinearLayout
    lateinit var menu: LinearLayout
    lateinit var lay1: CardView
    lateinit var request: LinearLayout
    lateinit var add_estate: LinearLayout
    override fun initializeComponents() {
        lang.appLanguage = lang.appLanguage
        offers = findViewById(R.id.offers)
        offers.setOnClickListener {
            startActivity(Intent(this,SearchActivity::class.java))
        }
        cats = findViewById(R.id.cats)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        order = findViewById(R.id.order)
        chat = findViewById(R.id.chat)
        add = findViewById(R.id.add)
        menu = findViewById(R.id.menu)
        profile = findViewById(R.id.profile)
        order.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,OrdersActivity::class.java))

        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
        }
        chat.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,ChatsActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
        }
        menu.setOnClickListener {if (user.loginStatus!!) {
            startActivity(Intent(this,MenueActivity::class.java))
        }else{
            startActivity(Intent(this,MenuActivity::class.java))
        } }
        lay1 = findViewById(R.id.lay1)
        request = findViewById(R.id.request)
        add_estate = findViewById(R.id.add_estate)
        add.setOnClickListener {
            if (lay1.visibility == View.GONE) {
                lay1.visibility = View.VISIBLE
            }else{
                lay1.visibility = View.GONE
            }
        }
        request.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, AddingTermsActivity::class.java)
                intent.putExtra("type", "request")
                intent.putExtra("marketing", 0)
                startActivity(intent)
                lay1.visibility = View.GONE
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        profile.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this, MyAdsActivity::class.java))
            lay1.visibility = View.GONE
        }else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        add_estate.setOnClickListener { if (user.loginStatus!!){val intent = Intent(this,
            AddingTermsActivity::class.java)
            intent.putExtra("type","add")
            intent.putExtra("marketing",0)
            startActivity(intent)
            lay1.visibility = View.GONE}else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        linearLayoutManager = LinearLayoutManager(mContext!!, LinearLayoutManager.HORIZONTAL, false)
        categoryAdapter = CatsAdapter(mContext!!, categories, R.layout.recycle_categories)
        categoryAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = categoryAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL, false)
        orderAdapter = OrderAdapter(mContext!!, orders, R.layout.recycler_orders)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager1
        rv_recycle.adapter = orderAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getLocationWithPermission("orders", null)

        }
        getLocationWithPermission("orders", null)
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.cat){
            categoryAdapter.selected = position
            listModel = categories.get(position)
            categories.get(position).selected = true
            categoryAdapter.notifyDataSetChanged()
            if (categories.get(position).id==0) {
                getLocationWithPermission("orders", null)
            }else{
                getLocationWithPermission("orders", categories.get(position).id)
            }
        }else{
            val intent = Intent(this, RequestDetailsActivity::class.java)
            intent.putExtra("id",orders.get(position).id)
            startActivity(intent)
        }
    }

    override fun onTextChanged(view: View, position: Int) {

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getLocationWithPermission(type: String, category: Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(type, category)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(type, category)
        }

    }

    internal fun getCurrentLocation(type: String, category: Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                getData(gps.getLatitude().toString(), gps.getLongitude().toString(), type, category)
                //  putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address", result)
                    }
                } catch (e: IOException) {
                }
                // googleMap.clear()
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
            }
        }
    }

    fun getData(lat: String, lng: String, type: String, category: Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Orders(lang.appLanguage, type, lat, lng, category,null)?.enqueue(object :
            Callback<OrdersResponse> {
            override fun onResponse(call: Call<OrdersResponse>, response: Response<OrdersResponse>) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        categories.clear()
                        categories = response.body()?.categories!!
                        categories.add(0, ListModel(0, getString(R.string.all)))
                        categoryAdapter.updateAll(categories)
                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            Log.e("data", Gson().toJson(response.body()?.data))
                            layNoItem!!.visibility = View.GONE
                            layNoInternet!!.visibility = View.GONE
                            rv_recycle.visibility = View.VISIBLE
                            orderAdapter.updateAll(response.body()?.data!!)
                        }

                    } else {
                        CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!, t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

        })
    }

}