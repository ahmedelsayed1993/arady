package com.aait.realestateapp.UI.Activities.Main

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.ListResponse
import com.aait.realestateapp.Models.TermsResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Views.ListDialog
import com.aait.realestateapp.UI.Views.SearchDialog
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TypeActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_add_type
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var city:TextView
    lateinit var neighborhood:TextView
    lateinit var category:TextView
    lateinit var confirm:Button
    var listModels = ArrayList<ListModel>()
    lateinit var listDialog: SearchDialog
    lateinit var listDialog1: ListDialog
    lateinit var listModel: ListModel
    lateinit var listModel1: ListModel
    var selected = 0
    lateinit var cat : ListModel

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        city = findViewById(R.id.city)
        neighborhood = findViewById(R.id.neighborhood)
        category = findViewById(R.id.category)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.add_estate)
        city.setOnClickListener {
            selected = 0
            getCity()
        }
        neighborhood.setOnClickListener {
            if (CommonUtil.checkTextError(city,getString(R.string.city))){
                return@setOnClickListener
            }else{
                selected = 1
                getNeighborhood(listModel.id!!)
            }
        }
        category.setOnClickListener {
            selected = 2
            getCategory()
        }
        confirm.setOnClickListener {
            if (CommonUtil.checkTextError(city,getString(R.string.city))||
                    CommonUtil.checkTextError(neighborhood,getString(R.string.neighborhood))||
                    CommonUtil.checkTextError(category,getString(R.string.category))){
                return@setOnClickListener
            }else{
                Add()
            }
        }

    }

    fun getCity(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCities(lang.appLanguage)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                listDialog = SearchDialog(mContext,this@TypeActivity,listModels,getString(R.string.city))
                                listDialog.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
    fun getNeighborhood(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getNeighbors(lang.appLanguage,id)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                listDialog = SearchDialog(mContext,this@TypeActivity,listModels,getString(R.string.neighborhood))
                                listDialog.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
    fun getCategory(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Category(lang.appLanguage)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                listDialog1 = ListDialog(mContext,this@TypeActivity,listModels,getString(R.string.category))
                                listDialog1.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){

            if (selected==0){
                listDialog.dismiss()
                listModel = listModels.get(position)
                city.text = listModel.name
            }else if (selected==1){
                listDialog.dismiss()
                listModel1 = listModels.get(position)
                neighborhood.text = listModel1.name
            }else{
                listDialog1.dismiss()
                cat = listModels.get(position)
                category.text = cat.name
            }
        }
    }

    override fun onTextChanged(view: View, position: Int) {

    }

    fun Add(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddInterst(lang.appLanguage,"Bearer"+user.userData.token,listModel.id!!
        ,listModel1.id!!,cat.id!!)?.enqueue(object :Callback<TermsResponse>{
            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        onBackPressed()
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })
    }
}