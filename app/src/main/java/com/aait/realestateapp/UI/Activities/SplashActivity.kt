package com.aait.realestateapp.UI.Activities


import android.content.Intent
import android.os.Handler
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.aait.realestateapp.UI.Activities.Main.SearchActivity

class SplashActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash



    var isSplashFinishid = false
    override fun initializeComponents() {

        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (user.loginStatus==true){

                        var intent = Intent(this@SplashActivity, SearchActivity::class.java)
                        startActivity(intent)
                        finish()

                }else {
                    var intent = Intent(this@SplashActivity, ChooseLanguageActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }, 2100)
        }, 1800)
    }
}