package com.aait.realestateapp.UI.Activities.Main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ChatsModel
import com.aait.realestateapp.Models.ChatsResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.AddingTermsActivity
import com.aait.realestateapp.UI.Activities.AppInfo.MyAdsActivity
import com.aait.realestateapp.UI.Activities.Auth.LoginActivity
import com.aait.realestateapp.UI.Controllers.ConversationsAdapter
import com.aait.realestateapp.UI.Fragments.ChatsFragment
import com.aait.realestateapp.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChatsActivity:ParentActivity() , OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.fragment_chat

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    var chatModels = java.util.ArrayList<ChatsModel>()
    lateinit var conversationsAdapter: ConversationsAdapter
    lateinit var lay1: CardView
    lateinit var request: LinearLayout
    lateinit var add_estate: LinearLayout
    lateinit var search: LinearLayout
    lateinit var add: ImageView
    lateinit var order: LinearLayout
    lateinit var menu: LinearLayout
    lateinit var pofile:LinearLayout
    override fun initializeComponents() {

        lang.appLanguage = lang.appLanguage
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
         lay1 = findViewById(R.id.lay1)
        request = findViewById(R.id.request)
        add_estate = findViewById(R.id.add_estate)
        search = findViewById(R.id.search)
        order = findViewById(R.id.order)
        add = findViewById(R.id.add)
        pofile = findViewById(R.id.profile)
        menu = findViewById(R.id.menu)
        search.setOnClickListener {
            startActivity(Intent(this,SearchActivity::class.java))
        }
        order.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,OrdersActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
        }
        menu.setOnClickListener {if (user.loginStatus!!) {
            startActivity(Intent(this,MenueActivity::class.java))
        }else{
            startActivity(Intent(this,MenuActivity::class.java))
        } }
        add.setOnClickListener {
            if (lay1.visibility == View.GONE) {
                lay1.visibility = View.VISIBLE
            }else{
                lay1.visibility = View.GONE
            }
        }
        request.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, AddingTermsActivity::class.java)
                intent.putExtra("type", "request")
                intent.putExtra("marketing", 0)
                startActivity(intent)
                lay1.visibility = View.GONE
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        pofile.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,MyAdsActivity::class.java))
            lay1.visibility = View.GONE
        }else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        add_estate.setOnClickListener { if (user.loginStatus!!){val intent = Intent(this,
            AddingTermsActivity::class.java)
            intent.putExtra("type","add")
            intent.putExtra("marketing",0)
            startActivity(intent)
            lay1.visibility = View.GONE}else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        conversationsAdapter =  ConversationsAdapter(mContext!!,chatModels, R.layout.recycler_conversations)
        conversationsAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = conversationsAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        if (user.loginStatus!!) {
            swipeRefresh!!.setOnRefreshListener { getHome() }
            getHome()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }


    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Conversations(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object :
            Callback<ChatsResponse> {
            override fun onResponse(call: Call<ChatsResponse>, response: Response<ChatsResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText("لا يوجد محادثات")
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                conversationsAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<ChatsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }

    override fun onItemClick(view: View, position: Int) {

        val intent = Intent(this, ChattActivity::class.java)
        intent.putExtra("id", chatModels.get(position).conversation_id)
        intent.putExtra("receiver", chatModels.get(position).user_id)
        intent.putExtra("lastpage", chatModels.get(position).lastPage)
        startActivity(intent)


    }

    override fun onTextChanged(view: View, position: Int) {

    }
}