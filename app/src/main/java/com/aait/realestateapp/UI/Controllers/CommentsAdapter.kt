package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.CommentsModel
import com.aait.realestateapp.R

import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class CommentsAdapter (context: Context, data: MutableList<CommentsModel>, layoutId: Int) :
        ParentRecyclerAdapter<CommentsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.name!!.setText(listModel.name)
        viewHolder.created.text = listModel.created
        viewHolder.comment.text = listModel.comment




    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var created = itemView.findViewById<TextView>(R.id.created)
        internal var comment = itemView.findViewById<TextView>(R.id.comment)

    }
}