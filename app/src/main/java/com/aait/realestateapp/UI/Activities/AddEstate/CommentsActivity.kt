package com.aait.realestateapp.UI.Activities.AddEstate

import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.CommentsModel
import com.aait.realestateapp.Models.CommentsResponse
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.ListResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Controllers.CommentsAdapter
import com.aait.realestateapp.UI.Controllers.ListAdapter
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class CommentsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_reports
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    lateinit var back: ImageView
    internal var layNoItem: RelativeLayout? = null
    lateinit var title: TextView
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var reportsAdapter: CommentsAdapter
    lateinit var linearLayoutManager1: LinearLayoutManager
    var reportsModels= ArrayList<CommentsModel>()
    var id = 0
    override fun initializeComponents() {

        id = intent.getIntExtra("id",0)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        back = findViewById(R.id.back)
        layNoItem = findViewById(R.id.lay_no_item)
        title = findViewById(R.id.title)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        reportsAdapter = CommentsAdapter(mContext,reportsModels,R.layout.recycle_comment)
        rv_recycle.layoutManager = linearLayoutManager1
        rv_recycle.adapter = reportsAdapter
        title.text = getString(R.string.reports)
        back.setOnClickListener { onBackPressed()
            finish()}
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Reports(lang.appLanguage,"Bearer"+user.userData.token,id)?.enqueue(object :
            Callback<CommentsResponse> {
            override fun onResponse(call: Call<CommentsResponse>, response: Response<CommentsResponse>) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            reportsAdapter.updateAll(response.body()?.data!!)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<CommentsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }
        })

    }

}