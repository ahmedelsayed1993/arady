package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.ScaleGestureDetector
import android.view.View
import android.widget.MediaController
import android.widget.VideoView
import com.aait.realestateapp.R
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import com.github.islamkhsh.CardSliderAdapter



class SliderAdpaters  (context: Context, list : ArrayList<String>) : CardSliderAdapter<String>(list) {


    var list = list
    var context=context

    lateinit var image: PhotoView
    lateinit var video:VideoView
    var mediaControls: MediaController? = null
    override fun bindView(position: Int, itemContentView: View, item: String?) {
        image = itemContentView.findViewById(R.id.image)
        video = itemContentView.findViewById(R.id.video)
        if (mediaControls == null) {
            // creating an object of media controller class
            mediaControls = MediaController(context)

            // set the anchor view for the video view
            mediaControls!!.setAnchorView(this.video)
        }
        Log.e("image",item!!)
        if (item.toString().endsWith(".mp4")){
            image.visibility = View.GONE
            video.visibility = View.VISIBLE
            video!!.setMediaController(mediaControls)

            // set the absolute path of the video file which is going to be played
            video!!.setVideoURI(Uri.parse(item))

            video!!.requestFocus()

            // starting the video
            video!!.start()
        }else {
            video.visibility = View.GONE
            image.visibility = View.VISIBLE
            Glide.with(context).load(item).into(image)
        }
//        itemContentView.setOnClickListener {
//            val intent  = Intent(context, ImagesActivity::class.java)
//            intent.putExtra("link",list)
//            context.startActivity(intent)
//        }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.images }

}