package com.aait.realestateapp.UI.Activities.AppInfo

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.R

class ContactActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_contact_us

    lateinit var fees:CardView
    lateinit var questions:CardView
    lateinit var contacts:CardView
    lateinit var guide:CardView
    lateinit var back:ImageView
    lateinit var title:TextView
    override fun initializeComponents() {
        fees = findViewById(R.id.fees)
        questions = findViewById(R.id.questions)
        contacts = findViewById(R.id.contacts)
        guide = findViewById(R.id.guide)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        title.text = getString(R.string.contact_us)
        back.setOnClickListener { onBackPressed()
        finish()}

        fees.setOnClickListener { startActivity(Intent(this,PaymentActivity::class.java)) }
        questions.setOnClickListener { startActivity(Intent(this,QuestionsActivity::class.java)) }
        contacts.setOnClickListener { startActivity(Intent(this,SocialMediaActivity::class.java)) }

        guide.setOnClickListener { startActivity(Intent(this,UserGideActivity::class.java))  }

    }
}