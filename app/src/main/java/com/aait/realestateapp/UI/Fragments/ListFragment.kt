package com.aait.realestateapp.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.BaseFragment
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.EstatesModel
import com.aait.realestateapp.Models.HomeResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.AdvertismentDetailsActivity
import com.aait.realestateapp.UI.Controllers.MyEstatesAdapter
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListFragment:BaseFragment(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.fragment_list
    companion object {
        fun newInstance(lat: String, lng: String): ListFragment {
            val args = Bundle()
            val fragment = ListFragment()
            args.putString("lat", lat)
            args.putString("lng", lng)
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var latest:RadioButton
    lateinit var price:RadioButton
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var myEstatesAdapter: MyEstatesAdapter
    var estates = ArrayList<EstatesModel>()
    lateinit var map:CardView
    var lat = ""
    var lng = ""
    var type = "latest"
    override fun initializeComponents(view: View) {
        lang.appLanguage = lang.appLanguage
        val bundle = this.arguments
        lat = bundle?.getString("lat")!!
        lng = bundle?.getString("lng")!!
        latest = view.findViewById(R.id.latest)
        price = view.findViewById(R.id.price)
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        map = view.findViewById(R.id.map)
        linearLayoutManager = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL, false)
        myEstatesAdapter = MyEstatesAdapter(mContext!!, estates, R.layout.recycle_ads)
        myEstatesAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = myEstatesAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData(lat, lng, type)

        }
        getData(lat, lng, type)
        price.setOnClickListener {
            type = "price"
            getData(lat, lng, type)
        }
        latest.setOnClickListener {
            type = "latest"
            getData(lat, lng, type)
        }
        map.setOnClickListener {
            val nextFrag: SearchFragment = SearchFragment.newInstance()
            activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.home_fragment_container, nextFrag, "findThisFragment")
                    ?.addToBackStack(null)
                    ?.commit()
        }
    }
    fun getData(lat: String, lng: String, type: String){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Home(lang.appLanguage, "offers", lat, lng, null, type)?.enqueue(object : Callback<HomeResponse> {
            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            myEstatesAdapter.updateAll(response.body()?.data!!)
                        }
                    } else {
                        CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!, t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity, AdvertismentDetailsActivity::class.java)
        intent.putExtra("id",estates.get(position).id)
        startActivity(intent)
    }

    override fun onTextChanged(view: View, position: Int) {

    }
}