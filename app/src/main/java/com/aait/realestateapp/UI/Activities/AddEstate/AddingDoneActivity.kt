package com.aait.realestateapp.UI.Activities.AddEstate

import android.content.Intent
import android.os.Handler
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.ChooseLanguageActivity
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.aait.realestateapp.UI.Activities.Main.SearchActivity

class AddingDoneActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_adding_done

    lateinit var back:ImageView
    lateinit var title:TextView
    var isSplashFinishid = false
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        title.text = getString(R.string.Confirmation_of_posting_the_advertisement)
        back.setOnClickListener { startActivity(Intent(this,SearchActivity::class.java))
        finish()}

        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true


                    var intent = Intent(this, SearchActivity::class.java)
                    startActivity(intent)
                    finish()


            }, 2100)
        }, 1800)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,SearchActivity::class.java))
        finish()
    }
}