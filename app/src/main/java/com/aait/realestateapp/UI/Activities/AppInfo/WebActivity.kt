package com.aait.realestateapp.UI.Activities.AppInfo

import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.WebResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WebActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_web
    lateinit var web:WebView
    lateinit var back:ImageView
    lateinit var title:TextView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        web = findViewById(R.id.web)
        val webSettings: WebSettings = web.getSettings()
        webSettings.javaScriptEnabled = true
        title.text = getString(R.string.Finance_Calculator)
        back.setOnClickListener { onBackPressed()
        finish()}
        getData()

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.WebLinks(lang.appLanguage)?.enqueue(object : Callback<WebResponse> {
            override fun onResponse(call: Call<WebResponse>, response: Response<WebResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        web.loadUrl(response.body()?.data?.loan_calculator!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<WebResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })
    }
}