package com.aait.realestateapp.UI.Activities.AddEstate

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.GPS.GPSTracker
import com.aait.realestateapp.GPS.GpsTrakerListener
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.*
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.aait.realestateapp.UI.Controllers.FeatureAdapter
import com.aait.realestateapp.UI.Controllers.NeighborAdapter
import com.aait.realestateapp.UI.Views.ListDialog
import com.aait.realestateapp.UI.Views.OptionDialog
import com.aait.realestateapp.UI.Views.SearchDialog
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.DialogUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.ln

class RequestActivity:ParentActivity(),OnItemClickListener,GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_request
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var lay: RecyclerView
    lateinit var listModel: ListModel
    var lat = ""
    var lng = ""
    var address = ""

    var neigh = 0
    //     lateinit var layParams:LinearLayout.LayoutParams
//    lateinit var relParams:RelativeLayout.LayoutParams
//    lateinit var linParams:LinearLayout.LayoutParams
    var name = ""
    var comps = ArrayList<FeaturesModel>()
    lateinit var featureAdapter: FeatureAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var next: Button
    lateinit var listDialog: OptionDialog
    lateinit var dialog: SearchDialog
    lateinit var dialog1: ListDialog
    var pos = 0
    var optionModels = ArrayList<OptionsModel>()
    var models = ArrayList<Model>()
    lateinit var start_price: EditText
    lateinit var end_price:EditText
    lateinit var type:TextView
    lateinit var city:TextView
    lateinit var neighborhood:RecyclerView
    lateinit var details: EditText
    var listModels = ArrayList<ListModel>()
    lateinit var cityModel: ListModel
    lateinit var applicantModel:ListModel
    var selected = 0
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var neighborAdapter: NeighborAdapter
    var neighbors = ArrayList<ListModel>()
    var ids = ArrayList<Int>()
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    private var mAlertDialog: AlertDialog? = null
    override fun initializeComponents() {
        listModel = intent.getSerializableExtra("cat") as ListModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        next = findViewById(R.id.next)
        lay  = findViewById(R.id.lay)
        start_price = findViewById(R.id.start_price)
        end_price = findViewById(R.id.end_price)
        city = findViewById(R.id.city)
        type = findViewById(R.id.type)
        neighborhood = findViewById(R.id.neighborhood)
        details = findViewById(R.id.details)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.Enter_advertisement_details)
        gridLayoutManager = GridLayoutManager(mContext,5)
        neighborAdapter = NeighborAdapter(mContext,neighbors,R.layout.recycler_neighbor)
        neighborAdapter.setOnItemClickListener(this)
        neighborhood.layoutManager = gridLayoutManager
        neighborhood.adapter = neighborAdapter
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        featureAdapter = FeatureAdapter(mContext,comps,R.layout.recycle_feature)
        featureAdapter.setOnItemClickListener(this)
        lay.layoutManager = linearLayoutManager
        lay.adapter = featureAdapter
        getLocationWithPermission()
        getData()
        type.setOnClickListener { selected = 0
            getApplicant()
        }
        city.setOnClickListener { selected = 1
            getCity()
        }
        next.setOnClickListener {
            ids.clear()
            for (i in 0..neighborAdapter.data.size-1){
              if (neighborAdapter.data.get(i).selected!!){
                  ids.add(neighborAdapter.data.get(i).id!!)
              }else{

              }
           }
            Log.e("ids",ids.joinToString(separator = ",",prefix = "",postfix = ""))
            for (i in 0 until featureAdapter.data.size){
                if (featureAdapter.data.get(i).type.equals("applicant_title")||featureAdapter.data.get(i).type.equals("cities")||featureAdapter.data.get(i).type.equals("neighborhoods")||featureAdapter.data.get(i).type.equals("start_price")||featureAdapter.data.get(i).type.equals("end_price")||featureAdapter.data.get(i).type.equals("textarea")){

                }else {
                    if (featureAdapter.data.get(i).value_id.equals("")){

                    }else {
                        models.add(
                                Model(
                                        featureAdapter.data.get(i).feature_id.toString(),
                                        featureAdapter.data.get(i).value_id,
                                        featureAdapter.data.get(i).type
                                )
                        )
                    }
                }
            }
            Log.e("model", Gson().toJson(models))
            if (CommonUtil.checkTextError(type,getString(R.string.Applicant_character))||
                    CommonUtil.checkTextError(city,getString(R.string.city))){
                return@setOnClickListener
            }else{
                if (ids.isEmpty()){
                    CommonUtil.makeToast(mContext,getString(R.string.neighborhoods))
                }else {
                    if (models.isEmpty()){
                        CommonUtil.makeToast(mContext,getString(R.string.Enter_advertisement_details))
                    }else{
                        if (CommonUtil.checkEditError(start_price,getString(R.string.start_price))||
                                CommonUtil.checkEditError(end_price,getString(R.string.end_price))){
                            return@setOnClickListener
                        }else{
                            showProgressDialog(getString(R.string.please_wait))
                            Client.getClient()?.create(Service::class.java)?.RequestEstate("Bearer"+user.userData.token,
                            lang.appLanguage,listModel.id!!,cityModel.id!!,applicantModel.id!!,ids.joinToString(separator = ",",prefix = "",postfix = ""),address,lat,lng
                            ,start_price.text.toString(),end_price.text.toString(),Gson().toJson(models),details.text.toString())?.enqueue(object :Callback<AddEstateResponse>{
                                override fun onResponse(call: Call<AddEstateResponse>, response: Response<AddEstateResponse>) {
                                    hideProgressDialog()
                                    if (response.isSuccessful){
                                        if (response.body()?.value.equals("1")){
                                            val intent = Intent(this@RequestActivity,RequestDetailsActivity::class.java)
                                            intent.putExtra("id",response.body()?.data)
                                            startActivity(intent)
                                            finish()
                                        }else{
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<AddEstateResponse>, t: Throwable) {
                                    hideProgressDialog()
                                    CommonUtil.handleException(mContext,t)
                                    t.printStackTrace()
                                }

                            })

                        }
                    }
                }
            }
        }

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.RequestFeatures(lang.appLanguage,"Bearer"+user.userData.token,listModel.id!!)
                ?.enqueue(object : Callback<FeatureResponse> {
                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun onResponse(call: Call<FeatureResponse>, response: Response<FeatureResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                featureAdapter.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<FeatureResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
    fun getCity(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCities(lang.appLanguage)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                dialog = SearchDialog(mContext,this@RequestActivity,listModels,getString(R.string.city))
                                dialog.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
    fun getApplicant(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Applicants(lang.appLanguage)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                dialog1 = ListDialog(mContext,this@RequestActivity,listModels,getString(R.string.Applicant_character))
                                dialog1.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }

    fun getNeighborhood(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getNeighbors(lang.appLanguage,id)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                neighborAdapter.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){

            if (selected==0){
                dialog1.dismiss()
                applicantModel = listModels.get(position)
                type.text = applicantModel.name
            }else if (selected==1){
                dialog.dismiss()
                cityModel = listModels.get(position)
                city.text = cityModel.name
                getNeighborhood(cityModel.id!!)
            }
            else if (selected == 2){
                listDialog.dismiss()
                featureAdapter.data.get(pos).value = optionModels.get(position).name
                featureAdapter.data.get(pos).value_id = optionModels.get(position).id.toString()
                featureAdapter.notifyItemChanged(pos)
            }
        }else if (view.id == R.id.neighbor){
            if (neighbors.get(position).selected!!) {
                neighbors.get(position).selected = false
            }else{
                neighbors.get(position).selected = true
            }
            neighborAdapter.notifyItemChanged(position)

        }else  {
            if (comps.get(position).type.equals("select")) {
                pos = position
                selected = 2
                optionModels = comps.get(position)?.options!!
                listDialog = OptionDialog(mContext, this, optionModels, comps.get(position).name!!)
                listDialog.show()
            }
        }

    }

    override fun onTextChanged(view: View, position: Int) {

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                //putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true

    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {

                lat = gps.getLatitude().toString()
                lng = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(lat),
                            java.lang.Double.parseDouble(lng),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        address = addresses[0].getAddressLine(0)
                        Log.e("address",address)
                        //CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }




            }
        }
    }
}