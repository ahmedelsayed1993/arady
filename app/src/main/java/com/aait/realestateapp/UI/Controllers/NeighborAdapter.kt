package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.R

class NeighborAdapter (context: Context, data: MutableList<ListModel>, layoutId: Int) :
        ParentRecyclerAdapter<ListModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    var selected :Int = 0

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.neighbor!!.setText(questionModel.name)
        if(!questionModel.selected!!){
            viewHolder.neighbor.background = mcontext.getDrawable(R.drawable.gray_shape)
            viewHolder.neighbor.textColor = mcontext.resources.getColor(R.color.colorPrimary)
        }else{

            viewHolder.neighbor.background = mcontext.getDrawable(R.drawable.blue_shape)
            viewHolder.neighbor.textColor = mcontext.resources.getColor(R.color.colorWhite)
        }

        viewHolder.neighbor.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var neighbor=itemView.findViewById<TextView>(R.id.neighbor)


    }
}