package com.aait.realestateapp.UI.Activities.AppInfo

import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.ListResponse
import com.aait.realestateapp.Models.QuestionModel
import com.aait.realestateapp.Models.QuestionResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Controllers.ListAdapter
import com.aait.realestateapp.UI.Controllers.QuestionAdapter
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QuestionsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_category
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    lateinit var back: ImageView
    internal var layNoItem: RelativeLayout? = null
    lateinit var title: TextView
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var listAdapter:QuestionAdapter
    var listModels = ArrayList<QuestionModel>()
    override fun initializeComponents() {
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        back = findViewById(R.id.back)
        layNoItem = findViewById(R.id.lay_no_item)
        title = findViewById(R.id.title)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter = QuestionAdapter(mContext,listModels,R.layout.recycle_question)

        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = listAdapter
        title.text = getString(R.string.Frequently_Asked_Questions)
        back.setOnClickListener { onBackPressed()
            finish()}
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Questions(lang.appLanguage)?.enqueue(object : Callback<QuestionResponse> {
            override fun onResponse(call: Call<QuestionResponse>, response: Response<QuestionResponse>) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            listAdapter.updateAll(response.body()?.data!!)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<QuestionResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }
        })

    }
}