package com.aait.realestateapp.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.BaseFragment
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.OrdersModel
import com.aait.realestateapp.Models.OrdersResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.RequestDetailsActivity
import com.aait.realestateapp.UI.Activities.Main.AddTypeActivity
import com.aait.realestateapp.UI.Controllers.CatsAdapter
import com.aait.realestateapp.UI.Controllers.OrderAdapter
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrdersFragment:BaseFragment() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.fragment_orders
    companion object {
        fun newInstance(): OrdersFragment {
            val args = Bundle()
            val fragment = OrdersFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var add:ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var orderAdapter: OrderAdapter
    var orders = ArrayList<OrdersModel>()
    override fun initializeComponents(view: View) {
        lang.appLanguage = lang.appLanguage
        add = view.findViewById(R.id.add)
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager1 = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL, false)
        orderAdapter = OrderAdapter(mContext!!, orders, R.layout.recycler_orders)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager1
        rv_recycle.adapter = orderAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        if (user.loginStatus!!) {
            swipeRefresh!!.setOnRefreshListener {
                getData()

            }
            getData()
        }else{

        }
        add.setOnClickListener {
            startActivity(Intent(activity,AddTypeActivity::class.java))
        }

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.InterstedOrders(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object : Callback<OrdersResponse> {
            override fun onResponse(call: Call<OrdersResponse>, response: Response<OrdersResponse>) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {

                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            orderAdapter.updateAll(response.body()?.data!!)
                        }

                    } else {
                        CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!, t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity, RequestDetailsActivity::class.java)
        intent.putExtra("id",orders.get(position).estate_id)
        startActivity(intent)
    }

    override fun onTextChanged(view: View, position: Int) {

    }
}