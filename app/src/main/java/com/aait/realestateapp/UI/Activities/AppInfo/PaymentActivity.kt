package com.aait.realestateapp.UI.Activities.AppInfo

import android.content.Intent
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.FeesBankActivity
import com.aait.realestateapp.Utils.CommonUtil

class PaymentActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_payment
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var bank:LinearLayout


    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        bank = findViewById(R.id.bank)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.payment_method)
        bank.setOnClickListener { if (user.loginStatus!!){startActivity(Intent(this,FeesBankActivity::class.java))}else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }
        }
    }
}