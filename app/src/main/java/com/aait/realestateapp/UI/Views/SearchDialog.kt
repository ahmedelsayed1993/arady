package com.aait.realestateapp.UI.Views

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Controllers.ListAdapter

class SearchDialog (
    internal var mContext: Context,
    internal var onItemClickListener: OnItemClickListener,
    internal var mCarsList: ArrayList<ListModel>,
    internal var title: String
) : Dialog(mContext) {


    internal var rvRecycle: RecyclerView? = null
    internal  var iv_close: ImageView?=null
    internal var text:EditText?=null
    internal var search:ImageView?=null

    internal var lay_no_data: LinearLayout? = null


    internal var tv_title: TextView? = null

    internal lateinit var mLinearLayoutManager: LinearLayoutManager

    internal var mCatsList: ArrayList<ListModel>? = null
    var mCatsList1= ArrayList<ListModel>()
    internal lateinit var mListAdapter: ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.search_dialog)
        window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        window!!.setGravity(Gravity.CENTER)
        setCancelable(false)

        initializeComponents()
    }
//    override fun onCreate(savedInstanceState: Bundle) {
//        super.onCreate(savedInstanceState)
//
//
//        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        setContentView(R.layout.dailog_custom_layout)
//        window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//        window!!.setGravity(Gravity.CENTER)
//        setCancelable(false)
//
//        initializeComponents()
//    }

    private fun initializeComponents() {
        tv_title = findViewById<TextView>(R.id.title)
        rvRecycle = findViewById(R.id.rv_recycle)
        lay_no_data = findViewById(R.id.lay_no_data)
        iv_close = findViewById(R.id.close)
        text = findViewById(R.id.text)
        search = findViewById(R.id.search)
        tv_title!!.text = title
        mLinearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        rvRecycle!!.layoutManager = mLinearLayoutManager
        mListAdapter = ListAdapter(mContext, mCarsList, R.layout.recycle_list)
        mListAdapter.setOnItemClickListener(onItemClickListener)
        rvRecycle!!.adapter = mListAdapter
        search!!.setOnClickListener {
            if (text!!.text.toString().equals("")){

            }else{
               mCatsList1?.clear()
                for (i in 0..mCarsList.size-1){
                    if (mCarsList.get(i).name!!.contains(text!!.text.toString())){
                        mCatsList1!!.add(mCarsList.get(i))
                    }
                }
                if (mCatsList1?.size==0){
                    lay_no_data!!.visibility = View.VISIBLE
                    rvRecycle!!.visibility = View.GONE
                }else{
                    rvRecycle!!.visibility = View.VISIBLE
                    mListAdapter.updateAll(mCatsList1)
                }
            }
        }

        if (mCarsList.size == 0) {
            lay_no_data!!.visibility = View.VISIBLE
        }
        iv_close?.setOnClickListener { dismiss() }

    }


}

