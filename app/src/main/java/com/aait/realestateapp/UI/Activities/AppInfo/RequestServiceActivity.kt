package com.aait.realestateapp.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.ListResponse
import com.aait.realestateapp.Models.ServicesModel
import com.aait.realestateapp.Models.TermsResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.aait.realestateapp.UI.Activities.Main.SearchActivity
import com.aait.realestateapp.UI.Views.ListDialog
import com.aait.realestateapp.UI.Views.SearchDialog
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RequestServiceActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_request_service
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var name:EditText
    lateinit var city:TextView
    lateinit var neighborhood:TextView
    lateinit var phone:EditText
    lateinit var request:EditText
    lateinit var send:Button
    var listModels = ArrayList<ListModel>()
    lateinit var listDialog: SearchDialog
    lateinit var listModel: ListModel
    lateinit var listModel1: ListModel
    var selected = 0
    lateinit var servicesModel: ServicesModel
    override fun initializeComponents() {
        servicesModel = intent.getSerializableExtra("service") as ServicesModel
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        city = findViewById(R.id.city)
        neighborhood = findViewById(R.id.neighborhood)
        phone = findViewById(R.id.phone)
        request = findViewById(R.id.request)
        send = findViewById(R.id.send)
        city.setOnClickListener {
            selected = 0
            getCity()
        }
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = servicesModel.name
        neighborhood.setOnClickListener {
            if (CommonUtil.checkTextError(city,getString(R.string.city))){
                return@setOnClickListener
            }else{
                selected = 1
                getNeighborhood(listModel.id!!)
            }
        }
        if (user.loginStatus!!){
            phone.setText(user.userData?.phone)
        }else{
            phone.setText("")
        }
        send.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkTextError(city,getString(R.string.city))||
                    CommonUtil.checkTextError(neighborhood,getString(R.string.neighborhood))||
                    CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(request,getString(R.string.What_is_required))){
                return@setOnClickListener
            }else{
                   SendData()
            }
        }

    }

    fun SendData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.RequestService(lang.appLanguage,name.text.toString(),phone.text.toString(),servicesModel.id!!,listModel.id!!
        ,listModel1.id!!,request.text.toString())?.enqueue(object :Callback<TermsResponse>{
            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@RequestServiceActivity,SearchActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })
    }
    fun getCity(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCities(lang.appLanguage)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                listDialog = SearchDialog(mContext,this@RequestServiceActivity,listModels,getString(R.string.city))
                                listDialog.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
    fun getNeighborhood(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getNeighbors(lang.appLanguage,id)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                listDialog = SearchDialog(mContext,this@RequestServiceActivity,listModels,getString(R.string.neighborhood))
                                listDialog.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            listDialog.dismiss()
            if (selected==0){
                listModel = listModels.get(position)
                city.text = listModel.name
            }else{
                listModel1 = listModels.get(position)
                neighborhood.text = listModel1.name
            }
        }
    }

    override fun onTextChanged(view: View, position: Int) {

    }
}