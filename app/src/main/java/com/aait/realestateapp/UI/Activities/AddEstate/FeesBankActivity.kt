package com.aait.realestateapp.UI.Activities.AddEstate

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.FeesResponse
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AppInfo.BanksActivity
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FeesBankActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_ad_fee
    lateinit var rent: TextView
    lateinit var sale: TextView
    lateinit var annual: TextView
    lateinit var email: TextView
    lateinit var points: TextView
    lateinit var pay: Button
    lateinit var back: ImageView
    lateinit var title: TextView

    override fun initializeComponents() {

        rent = findViewById(R.id.rent)
        sale = findViewById(R.id.sale)
        annual = findViewById(R.id.annual)
        email = findViewById(R.id.email)
        points = findViewById(R.id.points)
        pay = findViewById(R.id.pay)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        title.text = getString(R.string.Advertising_fee)
        back.setOnClickListener { onBackPressed()
            finish()}
        pay.text = getString(R.string.payment_method)
        getData()
        pay.setOnClickListener {
            startActivity(Intent(this,BanksActivity::class.java))
        }



    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Fees(lang.appLanguage,"Bearer"+user.userData.token,null)
                ?.enqueue(object : Callback<FeesResponse> {
                    override fun onResponse(call: Call<FeesResponse>, response: Response<FeesResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                rent.text = getString(R.string.rental_fee)+" : "+response.body()?.data?.rental_fee+getString(R.string.Rial)
                                sale.text = getString(R.string.Selling_Fee)+" : "+response.body()?.data?.selling_fee+getString(R.string.Rial)
                                annual.text = getString(R.string.For_an_annual_subscription)+" : "+response.body()?.data?.annual_subscription+getString(R.string.Rial)
                                email.text = getString(R.string.Please_contact_via_email)+" : "+response.body()?.data?.email
                                points.text = response.body()?.data?.explanation_points
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<FeesResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
}