package com.aait.realestateapp.UI.Activities.AppInfo

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.BanksModel
import com.aait.realestateapp.Models.BanksResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Controllers.BanksAdapter
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BanksActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_banks
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var banks:RecyclerView
    lateinit var pay:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var banksAdapter: BanksAdapter
    var banksModel = ArrayList<BanksModel>()
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        banks = findViewById(R.id.banks)
        pay = findViewById(R.id.pay)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        banksAdapter = BanksAdapter(mContext,banksModel,R.layout.recycle_banks)
        banks.layoutManager = linearLayoutManager
        banks.adapter = banksAdapter
        getData()
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.payment_method)
        pay.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,BankTransferActivity::class.java))
        }else{

        }
        }

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Banks(lang.appLanguage)?.enqueue(object : Callback<BanksResponse> {
            override fun onResponse(call: Call<BanksResponse>, response: Response<BanksResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        banksAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<BanksResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

        })
    }
}