package com.aait.realestateapp.UI.Activities.AppInfo

import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.ContrastResponse
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Models.ListResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Views.ListDialog
import com.aait.realestateapp.UI.Views.SearchDialog
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContrastingActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_contrasting

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var service:TextView
    lateinit var phone:EditText
    lateinit var city:TextView
    lateinit var details:EditText
    lateinit var send:Button
    var listModels = ArrayList<ListModel>()
    lateinit var listDialog: SearchDialog
    lateinit var listDialog1: ListDialog
    lateinit var listModel: ListModel
    lateinit var listModel1: ListModel
    var selected = 0
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        service = findViewById(R.id.service)
        phone = findViewById(R.id.phone)
        city = findViewById(R.id.city)
        details = findViewById(R.id.details)
        send = findViewById(R.id.send)
        service.setOnClickListener {
            selected = 0
            getServices()
        }
        city.setOnClickListener {
            selected = 1
            getCity()
        }
        title.text = getString(R.string.Construction_and_contracting)
        back.setOnClickListener { onBackPressed()
        finish()}
        send.setOnClickListener {
            if (CommonUtil.checkTextError(service,getString(R.string.Choose_the_desired_service))||
                    CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkTextError(city,getString(R.string.city))||
                    CommonUtil.checkEditError(details,getString(R.string.service_details))){
                return@setOnClickListener
            }else{
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Construction(lang.appLanguage,phone.text.toString(),details.text.toString(),listModel.id,listModel1.id)
                        ?.enqueue(object : Callback<ContrastResponse> {
                            override fun onResponse(call: Call<ContrastResponse>, response: Response<ContrastResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                        onBackPressed()
                                        finish()
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<ContrastResponse>, t: Throwable) {
                                hideProgressDialog()
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                            }

                        })
            }
        }

    }
    fun getCity(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCities(lang.appLanguage)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.data!!
                                listDialog = SearchDialog(mContext,this@ContrastingActivity,listModels,getString(R.string.city))
                                listDialog.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
    fun getServices(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Construction(lang.appLanguage,null,null,null,null)
                ?.enqueue(object : Callback<ContrastResponse> {
                    override fun onResponse(call: Call<ContrastResponse>, response: Response<ContrastResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listModels = response.body()?.services!!
                                listDialog1 = ListDialog(mContext,this@ContrastingActivity,listModels,getString(R.string.Choose_the_desired_service))
                                listDialog1.show()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ContrastResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {

        if (view.id == R.id.name){
           if (selected == 0){
               listDialog1.dismiss()
               listModel = listModels.get(position)
               service.text = listModel.name
           }else{
               listDialog.dismiss()
               listModel1 = listModels.get(position)
               city.text = listModel1.name
           }
        }

    }

    override fun onTextChanged(view: View, position: Int) {

    }
}