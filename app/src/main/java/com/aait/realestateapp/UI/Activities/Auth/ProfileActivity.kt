package com.aait.realestateapp.UI.Activities.Auth

import android.app.Dialog
import android.content.Intent
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.BaseResponse
import com.aait.realestateapp.Models.UserResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.Utils.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_profile
     lateinit var back:ImageView
     lateinit var title:TextView
     lateinit var image:ImageView
     lateinit var advertiser_num:TextView
     lateinit var ads_num:TextView
     lateinit var phone:TextView
     lateinit var country:TextView
     lateinit var change_pass:TextView
     lateinit var name:TextView
     lateinit var office:TextView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        advertiser_num = findViewById(R.id.advertiser_num)
        ads_num = findViewById(R.id.ads_num)
        phone = findViewById(R.id.phone)
        country = findViewById(R.id.country)
        office = findViewById(R.id.office)
        change_pass = findViewById(R.id.change_pass)
        office.setOnClickListener { startActivity(Intent(this,OfficeActivity::class.java)) }
        getData()
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.profile)
        image.setOnClickListener { startActivity(Intent(this,EditProfileActivity::class.java)) }
        change_pass.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_change_password)
            val old_pass = dialog?.findViewById<EditText>(R.id.old_pass)
            val new_pass = dialog?.findViewById<EditText>(R.id.new_pass)
            val confirm_pass = dialog?.findViewById<EditText>(R.id.confirm_pass)

            val save = dialog?.findViewById<Button>(R.id.confirm)


            save?.setOnClickListener {
                if (CommonUtil.checkEditError(old_pass,getString(R.string.old_password))||
                        CommonUtil.checkEditError(new_pass,getString(R.string.new_password))||
                        CommonUtil.checkLength(new_pass,getString(R.string.password_length),6)||
                        CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_password))){
                    return@setOnClickListener
                }else{
                    if (!new_pass.text.toString().equals(confirm_pass.text.toString())){
                        confirm_pass.error = getString(R.string.password_not_match)
                    }else{
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.resetPassword(lang.appLanguage,"Bearer "+user.userData.token,old_pass.text.toString(),new_pass.text.toString())?.enqueue(
                                object :Callback<BaseResponse>{
                                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                                        CommonUtil.handleException(mContext,t)
                                        t.printStackTrace()
                                        hideProgressDialog()
                                    }

                                    override fun onResponse(
                                            call: Call<BaseResponse>,
                                            response: Response<BaseResponse>
                                    ) {
                                        hideProgressDialog()
                                        if (response.isSuccessful){
                                            if (response.body()?.value.equals("1")){
                                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                                dialog?.dismiss()
                                            }else if(response.body()?.value.equals("401")){
                                                user.loginStatus=false
                                                user.Logout()
                                                startActivity(Intent(this@ProfileActivity, LoginActivity::class.java))
                                            }else{
                                                CommonUtil.makeToast(mContext,response.body()?.msg!!)

                                            }
                                        }
                                    }
                                }
                        )
                    }
                }

            }
            dialog?.show()
        }

    }

    override fun onResume() {
        super.onResume()
        Client.getClient()?.create(Service::class.java)?.Profile("Bearer"+user.userData.token,lang.appLanguage,null,null,null)
                ?.enqueue(object : Callback<UserResponse> {
                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                user.userData = response.body()?.data!!
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                                advertiser_num.text = response.body()?.data?.whatsapp
                                ads_num.text = response.body()?.data?.estates_count.toString()
                                phone.text = response.body()?.data?.phone
                                country.text = response.body()?.data?.country
                                name.text = response.body()?.data?.name
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Profile("Bearer"+user.userData.token,lang.appLanguage,null,null,null)
                ?.enqueue(object : Callback<UserResponse> {
                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                user.userData = response.body()?.data!!
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                                advertiser_num.text = response.body()?.data?.whatsapp
                                ads_num.text = response.body()?.data?.estates_count.toString()
                                phone.text = response.body()?.data?.phone
                                country.text = response.body()?.data?.country
                                name.text = response.body()?.data?.name
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
}