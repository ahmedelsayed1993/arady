package com.aait.realestateapp.UI.Activities.AddEstate

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.EditEstateResponse
import com.aait.realestateapp.Models.ImagesModel
import com.aait.realestateapp.Models.ListModel
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Controllers.ImagesAdapter
import com.aait.realestateapp.UI.Controllers.PhotosAdapter
import com.aait.realestateapp.Utils.CommonUtil
import com.aait.realestateapp.Utils.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.google.gson.Gson

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream

class EditImagesActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_edit_estate_images
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var add: ImageView
    lateinit var images: RecyclerView
    lateinit var photos: RecyclerView
    lateinit var next: Button
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options :Options= Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(8)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        // .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
        .setVideoDurationLimitinSeconds(30)
        .setMode(Options.Mode.All)
    internal var options1 :Options= Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(8)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        // .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
        .setVideoDurationLimitinSeconds(30)
        .setMode(Options.Mode.Picture)
    internal var options2 :Options= Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(8)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        // .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
        .setVideoDurationLimitinSeconds(30)
        .setMode(Options.Mode.Video)

    var ImageBasePaths = ArrayList<String>()
    lateinit var imagesAdapter: ImagesAdapter
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var photosAdapter: PhotosAdapter
    lateinit var gridLayoutManager1: GridLayoutManager
    var imagesModels = ArrayList<ImagesModel>()
    var id = 0
    var IDs = ArrayList<Int>()
    lateinit var upload_images:Button
    lateinit var upload_video:Button
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        add = findViewById(R.id.add)
        images = findViewById(R.id.photos)
        photos = findViewById(R.id.images)
        upload_images = findViewById(R.id.upload_images)
        upload_video = findViewById(R.id.upload_video)
        next = findViewById(R.id.next)
        imagesAdapter = ImagesAdapter(mContext,ArrayList<String>(),R.layout.recycle_images)
        gridLayoutManager = GridLayoutManager(mContext,2)
        images.layoutManager = gridLayoutManager
        images.adapter = imagesAdapter
        photosAdapter = PhotosAdapter(mContext,imagesModels,R.layout.recycler_photos)
        gridLayoutManager1 = GridLayoutManager(mContext,2)
        photosAdapter.setOnItemClickListener(this)
        photos.layoutManager = gridLayoutManager1
        photos.adapter = photosAdapter
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.Upload_ad_pictures)
        getEstate()
        add.setOnClickListener { upload_video.visibility = View.VISIBLE
            upload_images.visibility = View.VISIBLE}
        upload_video.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options2)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options2)
            }
        }
        upload_images.setOnClickListener {  val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_add)
            val camera = dialog?.findViewById<LinearLayout>(R.id.camera)
            val gallery = dialog?.findViewById<LinearLayout>(R.id.gallery)
            camera.setOnClickListener { dialog.dismiss()
                if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                    if (!(PermissionUtils.hasPermissions(mContext,
                            android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                        CommonUtil.PrintLogE("Permission not granted")
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                            )
                        }
                    } else {
                        Pix.start(this, options1)
                        CommonUtil.PrintLogE("Permission is granted before")
                    }
                } else {
                    CommonUtil.PrintLogE("SDK minimum than 23")
                    Pix.start(this, options1)
                }
            }
            gallery.setOnClickListener { dialog.dismiss()
                val intent = Intent()
                intent.type = "image/*"
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                intent.action = Intent.ACTION_GET_CONTENT
                previewRequest.launch(intent)
//                val intent= Intent(mContext, MiraFilePickerActivity::class.java)
//                intent.putExtra("multiple", true)
//                intent.putExtra("type", "image/*")
//                previewRequest.launch(intent)
            }
            dialog.show()
        }

        next.setOnClickListener {
            if (ImageBasePaths.isEmpty()){
                if (IDs.isEmpty()){

                }else{
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.EditImage(lang.appLanguage,"Bearer"+user.userData.token
                    ,id,IDs.joinToString(separator = ",",postfix = "",prefix = ""))?.enqueue(object :Callback<EditEstateResponse>{
                        override fun onResponse(
                            call: Call<EditEstateResponse>,
                            response: Response<EditEstateResponse>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,getString(R.string.data_updated))
                                    onBackPressed()
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                        override fun onFailure(call: Call<EditEstateResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                    })
                }
            }else{
                if (IDs.isEmpty()){
                    var images = ArrayList<MultipartBody.Part>()
                    for (i in 0..ImageBasePaths.size - 1) {
                        var filePart: MultipartBody.Part? = null
                        val ImageFile = File(ImageBasePaths.get(i))
                        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
                        filePart = MultipartBody.Part.createFormData("images[]", ImageFile.name, fileBody)
                        images.add(filePart)
                    }
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.EditImages(lang.appLanguage,"Bearer"+user.userData.token
                    ,id,images,null)?.enqueue(object :Callback<EditEstateResponse>{
                        override fun onResponse(
                            call: Call<EditEstateResponse>,
                            response: Response<EditEstateResponse>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,getString(R.string.data_updated))
                                    onBackPressed()
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                        override fun onFailure(call: Call<EditEstateResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                    })
                }else{
                    var images = ArrayList<MultipartBody.Part>()
                    for (i in 0..ImageBasePaths.size - 1) {
                        var filePart: MultipartBody.Part? = null
                        val ImageFile = File(ImageBasePaths.get(i))
                        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
                        filePart = MultipartBody.Part.createFormData("images[]", ImageFile.name, fileBody)
                        images.add(filePart)
                    }
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.EditImages(lang.appLanguage,"Bearer"+user.userData.token
                        ,id,images,IDs.joinToString(separator = ",",postfix = "",prefix = ""))?.enqueue(object :Callback<EditEstateResponse>{
                        override fun onResponse(
                            call: Call<EditEstateResponse>,
                            response: Response<EditEstateResponse>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,getString(R.string.data_updated))
                                    onBackPressed()
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                        override fun onFailure(call: Call<EditEstateResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                    })
                }
            }
        }
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id==R.id.delete){
            IDs.add(imagesModels.get(position).id!!)
            imagesModels.remove(imagesModels.get(position))
            photosAdapter.notifyDataSetChanged()
            Log.e("ids",Gson().toJson(IDs))
        }

    }

    override fun onTextChanged(view: View, position: Int) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue?.clear()
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                for (i in 0..returnValue!!.size-1){
                    ImageBasePaths.add(returnValue!![i])
                }
                if(ImageBasePaths.size!=0){

                    images.visibility = View.VISIBLE
                    imagesAdapter.updateAll(ImageBasePaths)
                }
                Log.e("pathsss", Gson().toJson(returnValue))

            }
        }
    }
    val previewRequest =  registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if ( it.resultCode == Activity.RESULT_OK && it.data != null){
            if (it.resultCode == Activity.RESULT_OK) {

                var files: File? = null
                if (it.data?.data != null) {


                    ImageBasePaths?.add(saveFileInStorage(mContext, it.data?.data!!))

                    if(ImageBasePaths.size!=0){

                        images.visibility = View.VISIBLE
                        imagesAdapter.updateAll(ImageBasePaths)
                    }
                }
                if (it.data?.clipData != null) {
                    for (i in 0 until it.data?.clipData?.itemCount!!) {
                        val uri: Uri = it.data?.clipData?.getItemAt(i)?.uri!!

                        ImageBasePaths?.add(saveFileInStorage(mContext,uri))
                        // Log.e("file",convertToBase64(file))
                    }

                    if(ImageBasePaths.size!=0){

                        images.visibility = View.VISIBLE
                        imagesAdapter.updateAll(ImageBasePaths)
                    }
                }
            }
        }
    }

    fun saveFileInStorage(mContext: Context, uri: Uri): String {
        var path = ""
//        Thread {
        var file: File? = null
        try {
            val mimeType: String? = mContext.contentResolver.getType(uri)
            if (mimeType != null) {
                val inputStream: InputStream? = mContext.contentResolver.openInputStream(uri)
                val fileName = getFileName(mContext,uri)
                if (fileName != "") {
                    file = File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)?.getAbsolutePath().toString() + "/" + fileName)
                    val output: OutputStream = FileOutputStream(file)
                    try {
                        val buffer =
                            ByteArray(inputStream?.available()!!) // or other buffer size
                        var read: Int = 0
                        while (inputStream.read(buffer).also({ read = it }) != -1) {
                            output.write(buffer, 0, read)
                        }
                        output.flush()
                        path = file.getAbsolutePath() //use this path
                    } finally {
                        output.close()
                    }
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
//        }.start()
        return path
    }
    @SuppressLint("Range")
    fun getFileName(mContext: Context, uri: Uri): String {
        // The query, since it only applies to a single document, will only return
        // one row. There's no need to filter, sort, or select fields, since we want
        // all fields for one document.
        var displayName = ""
        var cursor: Cursor? = null
        cursor = mContext.contentResolver.query(uri, null, null, null, null, null)
        try {
            // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
            // "if there's anything to look at, look at it" conditionals.
            if (cursor != null && cursor.moveToFirst()) {

                // Note it's called "Display Name".  This is
                // provider-specific, and might not necessarily be the file name.
                displayName = cursor.getString(
                    cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                )
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            cursor?.close()
        }
        return displayName
    }


    fun getEstate(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditEstate(lang.appLanguage,"Bearer"+user.userData.token,id
            ,null,null,null,null,null,null,null,null)?.enqueue(object :
            Callback<EditEstateResponse> {
            override fun onResponse(
                call: Call<EditEstateResponse>,
                response: Response<EditEstateResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                       photosAdapter.updateAll(response.body()?.data?.images!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<EditEstateResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

        })
    }

}