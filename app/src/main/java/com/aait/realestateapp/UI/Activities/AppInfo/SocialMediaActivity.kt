package com.aait.realestateapp.UI.Activities.AppInfo

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Models.ContactResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.Utils.CommonUtil
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SocialMediaActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_contact_media
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var phone:TextView
    lateinit var email:TextView
    lateinit var address:TextView
    lateinit var name:EditText
    lateinit var email_add:EditText
    lateinit var complaint:EditText
    lateinit var send:Button
    lateinit var twitter:ImageView
    lateinit var instgram:ImageView
    lateinit var snapchat:ImageView
    var twitt = ""
    var insta = ""
    var snap = ""
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        address = findViewById(R.id.address)
        name = findViewById(R.id.name)
        email_add = findViewById(R.id.email_add)
        complaint = findViewById(R.id.complaint)
        send = findViewById(R.id.send)
        twitter = findViewById(R.id.twitter)
        instgram = findViewById(R.id.instgram)
        snapchat = findViewById(R.id.snapchat)
        getData()
        twitter.setOnClickListener { if (!twitt.equals(""))
        {
            if (twitt.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(twitt)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$twitt"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        instgram.setOnClickListener { if (!insta.equals(""))
        {
            if (insta.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(insta)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$insta"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }  }
        snapchat.setOnClickListener { if (!snap.equals(""))
        {
            if (snap.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(snap)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$snap"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }
        }
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Means_of_communication)

        send.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.full_name))||
                    CommonUtil.checkEditError(email_add,getString(R.string.phone_number))||
                    CommonUtil.checkLength(email_add,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(complaint,getString(R.string.complaint))){
                return@setOnClickListener
            }else{
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Contact(lang.appLanguage,name.text.toString(),email_add.text.toString(),complaint.text.toString())?.enqueue(
                        object: Callback<ContactResponse> {
                            override fun onResponse(call: Call<ContactResponse>, response: Response<ContactResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                       CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                        onBackPressed()
                                        finish()
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                            override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                                hideProgressDialog()
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                            }

                        })
            }
        }


    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Contact(lang.appLanguage,null,null,null)?.enqueue(
                object: Callback<ContactResponse> {
                    override fun onResponse(call: Call<ContactResponse>, response: Response<ContactResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                phone.text = response.body()?.data?.phone
                                email.text = response.body()?.data?.email
                                address.text = response.body()?.data?.address
                                for (i in 0..response.body()?.socials?.size!!-1){
                                    if (response.body()?.socials?.get(i)?.name.equals("twitter")){
                                        twitt = response.body()?.socials?.get(i)?.link!!
                                    }else if (response.body()?.socials?.get(i)?.name.equals("instagram")){
                                        insta = response.body()?.socials?.get(i)?.link!!
                                    }else if (response.body()?.socials?.get(i)?.name.equals("snapchat")){
                                        snap = response.body()?.socials?.get(i)?.link!!
                                    }
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
}