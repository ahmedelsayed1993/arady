package com.aait.realestateapp.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.Auth.LoginActivity
import com.aait.realestateapp.UI.Activities.Main.MainActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class ChooseLanguageActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_choose_lang
    lateinit var ar:LinearLayout
    lateinit var en:LinearLayout
    lateinit var ar_sel:ImageView
    lateinit var en_sel:ImageView
    lateinit var choose:Button
    var language = "ar"

    override fun initializeComponents() {
        ar = findViewById(R.id.ar)
        ar_sel = findViewById(R.id.ar_sel)
        en = findViewById(R.id.en)
        en_sel = findViewById(R.id.en_sel)
        choose = findViewById(R.id.choose)

        ar.setOnClickListener {
            ar_sel.visibility = View.VISIBLE
            en_sel.visibility = View.GONE
            language = "ar"
        }
        en.setOnClickListener {
            en_sel.visibility = View.VISIBLE
            ar_sel.visibility = View.GONE
            language = "en"
        }
        choose.setOnClickListener {
            lang.appLanguage = language
            user.loginStatus = false
            startActivity(Intent(this,LoginActivity::class.java))
            finish()
        }

    }
}