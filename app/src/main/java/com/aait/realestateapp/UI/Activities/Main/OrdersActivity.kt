package com.aait.realestateapp.UI.Activities.Main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.realestateapp.Base.ParentActivity
import com.aait.realestateapp.Listeners.OnItemClickListener
import com.aait.realestateapp.Models.OrdersModel
import com.aait.realestateapp.Models.OrdersResponse
import com.aait.realestateapp.Network.Client
import com.aait.realestateapp.Network.Service
import com.aait.realestateapp.R
import com.aait.realestateapp.UI.Activities.AddEstate.AddingTermsActivity
import com.aait.realestateapp.UI.Activities.AddEstate.RequestDetailsActivity
import com.aait.realestateapp.UI.Activities.AppInfo.MyAdsActivity
import com.aait.realestateapp.UI.Activities.Auth.LoginActivity
import com.aait.realestateapp.UI.Controllers.OrderAdapter
import com.aait.realestateapp.UI.Fragments.OrdersFragment
import com.aait.realestateapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrdersActivity:ParentActivity() , OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.fragment_orders

    lateinit var add_order: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var orderAdapter: OrderAdapter
    var orders = ArrayList<OrdersModel>()
    lateinit var lay1: CardView
    lateinit var request: LinearLayout
    lateinit var add_estate: LinearLayout
    lateinit var search:LinearLayout
    lateinit var add:ImageView
    lateinit var chat:LinearLayout
    lateinit var menu:LinearLayout
    lateinit var profile:LinearLayout
    override fun initializeComponents() {
        lang.appLanguage = lang.appLanguage
        add_order = findViewById(R.id.add_order)
        rv_recycle = findViewById(R.id.rv_recycle)
        profile = findViewById(R.id.profile)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        lay1 = findViewById(R.id.lay1)
        request = findViewById(R.id.request)
        add_estate = findViewById(R.id.add_estate)
        search = findViewById(R.id.search)
        chat = findViewById(R.id.chat)
        add = findViewById(R.id.add)
        menu = findViewById(R.id.menu)
        search.setOnClickListener {
            startActivity(Intent(this,SearchActivity::class.java))
        }
        chat.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,ChatsActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
        }
        menu.setOnClickListener {if (user.loginStatus!!) {
            startActivity(Intent(this,MenueActivity::class.java))
        }else{
            startActivity(Intent(this,MenuActivity::class.java))
        } }
        add.setOnClickListener {
            if (lay1.visibility == View.GONE) {
                lay1.visibility = View.VISIBLE
            }else{
                lay1.visibility = View.GONE
            }
        }
        request.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, AddingTermsActivity::class.java)
                intent.putExtra("type", "request")
                intent.putExtra("marketing", 0)
                startActivity(intent)
                lay1.visibility = View.GONE
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        add_estate.setOnClickListener { if (user.loginStatus!!){val intent = Intent(this,
            AddingTermsActivity::class.java)
            intent.putExtra("type","add")
            intent.putExtra("marketing",0)
            startActivity(intent)
            lay1.visibility = View.GONE}else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        profile.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this, MyAdsActivity::class.java))
            lay1.visibility = View.GONE
        }else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        }
        linearLayoutManager1 = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL, false)
        orderAdapter = OrderAdapter(mContext!!, orders, R.layout.recycler_orders)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager1
        rv_recycle.adapter = orderAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        if (user.loginStatus!!) {
            swipeRefresh!!.setOnRefreshListener {
                getData()

            }
            getData()
        }else{

        }
        add_order.setOnClickListener {
            startActivity(Intent(this,AddTypeActivity::class.java))
        }

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.InterstedOrders(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object :
            Callback<OrdersResponse> {
            override fun onResponse(call: Call<OrdersResponse>, response: Response<OrdersResponse>) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {

                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            orderAdapter.updateAll(response.body()?.data!!)
                        }

                    } else {
                        CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!, t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this, RequestDetailsActivity::class.java)
        intent.putExtra("id",orders.get(position).estate_id)
        startActivity(intent)
    }

    override fun onTextChanged(view: View, position: Int) {

    }
}