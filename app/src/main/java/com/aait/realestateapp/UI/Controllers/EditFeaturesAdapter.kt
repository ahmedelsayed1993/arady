package com.aait.realestateapp.UI.Controllers

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Typeface
import android.os.Build
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.res.ResourcesCompat
import com.aait.realestateapp.Base.ParentRecyclerAdapter
import com.aait.realestateapp.Base.ParentRecyclerViewHolder
import com.aait.realestateapp.Models.FeaturesModel
import com.aait.realestateapp.R

class EditFeaturesAdapter(context: Context, data: MutableList<FeaturesModel>, layoutId: Int) :
    ParentRecyclerAdapter<FeaturesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    lateinit var layParams: LinearLayout.LayoutParams
    lateinit var relParams: RelativeLayout.LayoutParams
    lateinit var linParams: LinearLayout.LayoutParams
    lateinit var text: TextView
    lateinit var edit: EditText
    lateinit var switch: Switch
    lateinit var radioButton: RadioButton
    lateinit var radioGroup: RadioGroup
    lateinit var typeface: Typeface
    var myColorStateList = ColorStateList(
        arrayOf(intArrayOf(mcontext.resources.getColor(R.color.colorPrimaryDark))),
        intArrayOf(mcontext.resources.getColor(R.color.colorPrimaryDark))
    )

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        layParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        relParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        linParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        relParams.setMargins(0, 5, 0, 5)
        layParams.setMargins(0, 50, 0, 0)
        linParams.setMargins(7, 0, 7, 0)
        typeface = ResourcesCompat.getFont(mcontext, R.font.cairo_regular)!!

        if (listModel.type.equals("number")){
            val lay = RelativeLayout(mcontext)
            lay.layoutParams = relParams
            edit = EditText(mcontext)
            edit.id = listModel.feature_id!!
            edit.background = mcontext.resources.getDrawable(R.drawable.white_blue_shape)
            edit.setTextColor(mcontext.resources.getColor(R.color.colorPrimary))
            edit.setPadding(10, 15, 10, 10)
            edit.inputType = InputType.TYPE_CLASS_NUMBER
            edit.textSize = 12F
            edit.typeface = typeface
            edit.layoutParams = layParams
            edit.setText(listModel.value)
            lay.addView(edit)
            relParams.addRule(RelativeLayout.ABOVE, edit.id)
            text = TextView(mcontext)
            text.text = listModel.name
            text.setTextColor(mcontext.resources.getColor(R.color.colorPrimaryDark))
            text.background = mcontext.resources.getDrawable(R.drawable.white_background)
            text.setPadding(3, 3, 3, 3)
            text.typeface = typeface
            text.textSize = 12f
            text.layoutParams = linParams
            lay.addView(text)
            viewHolder.layout.addView(lay)
            edit.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                }
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    listModel?.value = p0.toString()
                }
            })
        }else if (listModel.type.equals("text")){
            val lay = RelativeLayout(mcontext)
            lay.layoutParams = relParams
            edit = EditText(mcontext)
            edit.id = listModel.feature_id!!
            edit.background = mcontext.resources.getDrawable(R.drawable.white_blue_shape)
            edit.setTextColor(mcontext.resources.getColor(R.color.colorPrimary))
            edit.setPadding(10, 15, 10, 10)
            edit.inputType = InputType.TYPE_CLASS_TEXT
            edit.textSize = 12F
            edit.typeface = typeface
            edit.layoutParams = layParams
            edit.setText(listModel.value)
            lay.addView(edit)
            relParams.addRule(RelativeLayout.ABOVE, edit.id)
            text = TextView(mcontext)
            text.text = listModel.name
            text.setTextColor(mcontext.resources.getColor(R.color.colorPrimaryDark))
            text.background = mcontext.resources.getDrawable(R.drawable.white_background)
            text.setPadding(3, 3, 3, 3)
            text.typeface = typeface
            text.textSize = 12F
            text.layoutParams = linParams
            lay.addView(text)
            viewHolder.layout.addView(lay)
            edit.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                }
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    listModel?.value = p0.toString()
                }
            })
        }
        else if (listModel.type.equals("switch")){
            var lay = LinearLayout(mcontext)
            lay.layoutParams = layParams
            lay.orientation = LinearLayout.HORIZONTAL
            lay.background = mcontext.resources.getDrawable(R.drawable.white_blue_shape)
            lay.setPadding(10, 10, 10, 10)
            switch = Switch(mcontext)
            switch.id = listModel.feature_id!!
            lay.addView(switch)
            viewHolder.layout.addView(lay)
            var text = TextView(mcontext)
            text.width = 300
            text.text = listModel.name
            text.typeface = typeface
            text.setTextColor(mcontext.resources.getColor(R.color.colorPrimaryDark))
            text.background = mcontext.resources.getDrawable(R.drawable.white_background)
            text.layoutParams = linParams
            lay.addView(text)
            if (listModel.value.equals("1")){
                switch.isChecked = true
            }else{
                switch.isChecked = false
            }
            switch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    listModel.value = "1"
                } else {
                    listModel.value = ""
                }
            })
        }else if (listModel.type.equals("radio")){
            val text = TextView(mcontext)
            text.text = listModel.name
            text.setTextColor(mcontext.resources.getColor(R.color.colorPrimaryDark))
            text.background = mcontext.resources.getDrawable(R.drawable.white_background)
            text.setPadding(3, 3, 3, 3)
            text.layoutParams = linParams
            text.typeface = typeface
            viewHolder.layout.addView(text)
            radioGroup = RadioGroup(mcontext)
            radioGroup.layoutParams = layParams
            radioGroup.orientation = LinearLayout.VERTICAL
            radioGroup.weightSum = 2F

            radioGroup.background = mcontext.resources.getDrawable(R.drawable.white_blue_shape)
            radioGroup.setPadding(10, 15, 10, 10)
            val gridLayout = GridLayout(mcontext)
            gridLayout.columnCount = 2
            radioGroup.addView(gridLayout)
            for (j in 0 until listModel.options!!.size) {
                radioButton = RadioButton(mcontext)
                radioButton.id = listModel!!.options?.get(j)?.id!!
                radioButton.typeface = typeface
                radioButton.buttonTintList = myColorStateList
                radioButton.text = listModel?.options?.get(j)?.name
                radioButton.textSize = 12f

                radioGroup.addView(radioButton)
                radioButton.setOnClickListener {  listModel.value = listModel.options?.get(j)?.id!!.toString()
                    listModel.value_id = listModel.options?.get(j)?.name
                    Log.e("val",listModel.value.toString()) }
//                    if (isChecked) {
//                        listModel.value = listModel.options?.get(j)?.id!!.toString()
//                        listModel.value_id = listModel.options?.get(j)?.name
//                        Log.e("val",listModel.value.toString())
//
//                    } else {
//                        listModel.value = ""
//                        listModel.value_id = ""
//                    }
//                })

                if (listModel.value!!.equals(listModel!!.options?.get(j)?.id!!.toString())){
                    radioButton.isChecked = true
                }else{
                    radioButton.isChecked = false
                }

            }
            viewHolder.layout.addView(radioGroup)
        }else if (listModel.type.equals("select")){
            val lay = RelativeLayout(mcontext)
            lay.layoutParams = relParams
            text = TextView(mcontext)
            text.id = listModel.feature_id!!
            text.background = mcontext.resources.getDrawable(R.drawable.white_blue_shape)
            text.setTextColor(mcontext.resources.getColor(R.color.colorPrimary))
            text.setPadding(10, 15, 10, 10)
            text.typeface = typeface
            text.textSize = 13F
            text.text = listModel.value_id
            text.layoutParams = layParams
            lay.addView(text)
            relParams.addRule(RelativeLayout.ABOVE, text.id)
            text = TextView(mcontext)
            text.text = listModel.name
            text.typeface = typeface
            text.setTextColor(mcontext.resources.getColor(R.color.colorPrimaryDark))
            text.background = mcontext.resources.getDrawable(R.drawable.white_background)
            text.setPadding(3, 3, 3, 3)
            text.layoutParams = linParams
            lay.addView(text)
            viewHolder.layout.addView(lay)
        }


        viewHolder.layout.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(view, position)
            onItemClickListener.onTextChanged(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {
        internal var layout=itemView.findViewById<LinearLayout>(R.id.layout)
    }
}