package com.aait.realestateapp.Models

import java.io.Serializable

class EditEstateResponse:BaseResponse(),Serializable {
    var data:EditEstateModel?=null
}