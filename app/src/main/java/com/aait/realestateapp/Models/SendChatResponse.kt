package com.aait.realestateapp.Models

import java.io.Serializable

class SendChatResponse:BaseResponse(),Serializable {
    var data:ChatModel?= null
}