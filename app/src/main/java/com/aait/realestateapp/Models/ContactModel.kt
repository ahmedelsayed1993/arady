package com.aait.realestateapp.Models

import java.io.Serializable

class ContactModel:Serializable {
    var phone:String?=null
    var email:String?=null
    var address:String?=null
}