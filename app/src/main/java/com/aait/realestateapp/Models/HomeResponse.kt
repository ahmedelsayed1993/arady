package com.aait.realestateapp.Models

import java.io.Serializable

class HomeResponse:BaseResponse(),Serializable {
    var categories:ArrayList<ListModel>?=null
    var data:ArrayList<EstatesModel>?=null
}