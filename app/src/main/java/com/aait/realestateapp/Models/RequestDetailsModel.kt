package com.aait.realestateapp.Models

import java.io.Serializable

class RequestDetailsModel:Serializable {
    var category:String?=null
    var honest:Int?=null
    var created:String?=null
    var applicant:String?=null
    var city:String?=null
    var neighborhoods:String?=null
    var price:String?=null
    var published:Int?=null
    var number:Int?=null
    var views:Int?=null
    var features_url:String?=null
    var details:String?=null
    var show_info:Int?=null
    var avatar:String?=null
    var username:String?=null
    var user:Int?=null
    var phone:String?=null
    var whatsapp:String?=null
}