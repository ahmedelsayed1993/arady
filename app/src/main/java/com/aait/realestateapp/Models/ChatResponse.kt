package com.aait.realestateapp.Models

import java.io.Serializable

class ChatResponse:BaseResponse(),Serializable {
    var paginate:PaginationModel?=null
    var data:ArrayList<ChatModel>?=null
    var estate:EstateModel?=null
   // var user_data:user?=null
}