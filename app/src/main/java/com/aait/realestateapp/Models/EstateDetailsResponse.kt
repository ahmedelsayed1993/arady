package com.aait.realestateapp.Models

import java.io.Serializable

class EstateDetailsResponse:Serializable,BaseResponse() {
    var data:EstateDetailsModel?=null
}