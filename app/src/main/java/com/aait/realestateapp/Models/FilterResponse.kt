package com.aait.realestateapp.Models

import java.io.Serializable

class FilterResponse:BaseResponse(),Serializable {
    var data:ArrayList<FeaturesModel>?=null
    var estates:ArrayList<EstatesModel>?=null
    var search:Int?=null
}