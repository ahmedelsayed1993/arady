package com.aait.realestateapp.Models

import java.io.Serializable

class EstateModel:Serializable {
    var id:Int?=null
    var category:String?=null
    var price:String?=null
    var address:String?=null
    var features:String?=null
    var type:String?=null
}