package com.aait.realestateapp.Models

import java.io.Serializable

class CommentsModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var comment:String?=null
    var created:String?=null
}