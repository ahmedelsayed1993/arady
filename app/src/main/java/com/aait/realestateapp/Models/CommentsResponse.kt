package com.aait.realestateapp.Models

import java.io.Serializable

class CommentsResponse:BaseResponse(),Serializable {
    var data:ArrayList<CommentsModel>?=null
}