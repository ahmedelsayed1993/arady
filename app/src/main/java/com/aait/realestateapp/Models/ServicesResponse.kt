package com.aait.realestateapp.Models

import java.io.Serializable

class ServicesResponse:BaseResponse(),Serializable {
    var data:ArrayList<ServicesModel>?=null
}