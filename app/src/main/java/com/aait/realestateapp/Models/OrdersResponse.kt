package com.aait.realestateapp.Models

import java.io.Serializable

class OrdersResponse:BaseResponse(),Serializable {
    var categories:ArrayList<ListModel>?=null
    var data:ArrayList<OrdersModel>?=null
}