package com.aait.realestateapp.Models

import java.io.Serializable

class QuestionModel:Serializable {
    var question:String?=null
    var answer:String?=null
}