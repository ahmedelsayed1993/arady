package com.aait.realestateapp.Models

import java.io.Serializable

class ConversationIdResponse:BaseResponse(),Serializable {
    var data:ConversationIdModel?=null
}