package com.aait.realestateapp.Models

import java.io.Serializable

class ListModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var selected:Boolean?=null

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }

    constructor(id: Int?, name: String?, selected: Boolean?) {
        this.id = id
        this.name = name
        this.selected = selected
    }

}