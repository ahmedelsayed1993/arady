package com.aait.realestateapp.Models

import java.io.Serializable

class EstateDetailsModel:Serializable {
    var images:ArrayList<String>?=null
    var estate_id:Int?=null
    var category:String?=null
    var price:String?=null
    var address:String?=null
    var published:Int?=null
    var details:String?=null
    var features_url:String?=null
    var number:Int?=null
    var lat:String?=null
    var lng:String?=null
    var features:ArrayList<EstateFeatureModel>?=null
    var views:String?=null
    var share_link:String?=null
    var estates:ArrayList<EstatesModel>?=null
    var avatar:String?=null
    var username:String?=null
    var user:Int?=null
    var phone:String?=null
    var is_favorite:Int?=null
    var show_info:Int?=null
}