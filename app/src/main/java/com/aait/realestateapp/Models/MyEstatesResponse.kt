package com.aait.realestateapp.Models

import java.io.Serializable

class MyEstatesResponse:BaseResponse(),Serializable {
    var data:ArrayList<EstatesModel>?=null
}