package com.aait.realestateapp.Models

import java.io.Serializable

class EstateFeatureModel:Serializable {
    var id:Int?=null
    var type:String?=null
    var feature:String?=null
    var value:String?=null
    var icon:String?=null
}