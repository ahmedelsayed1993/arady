package com.aait.realestateapp.Models

import java.io.Serializable

class OrdersModel:Serializable {
    var id:Int?=null
    var estate_id:Int?=null
    var category:String?=null
    var honest:Int?=null
    var applicant:String?=null
    var city:String?=null
    var neighborhoods:String?=null
    var price:String?=null
    var type:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var distance:Int?=null
    var features:String?=null
    var image:String?=null
}