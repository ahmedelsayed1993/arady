package com.aait.realestateapp.Models

import java.io.Serializable

class EstatesModel:Serializable {
    var id:Int?=null
    var address:String?=null
    var price:String?=null
    var lat:String?=null
    var lng:String?=null
    var distance:Int?=null
    var features:String?=null
    var image:String?=null
    var category:String?=null
    var category_id:String?=null

}