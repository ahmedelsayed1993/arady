package com.aait.realestateapp.Models

import java.io.Serializable

class ContactResponse:BaseResponse(),Serializable {
    var data:ContactModel?=null
    var socials:ArrayList<SocialModel>?=null
}