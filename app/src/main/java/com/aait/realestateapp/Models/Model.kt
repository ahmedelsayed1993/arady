package com.aait.realestateapp.Models

import java.io.Serializable

class Model:Serializable {
    var feature_id:String?=null
    var value:String?=null
    var type:String?=null

    constructor(feature_id: String?, value: String?, type:String?) {
        this.feature_id = feature_id
        this.value = value
        this.type = type
    }
}