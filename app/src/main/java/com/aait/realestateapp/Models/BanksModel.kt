package com.aait.realestateapp.Models

import java.io.Serializable

class BanksModel:Serializable {
    var bank_name:String?=null
    var account_name:String?=null
    var account_number:String?=null
    var iban_number:String?=null
    var image:String?=null
}