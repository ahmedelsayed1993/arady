package com.aait.realestateapp.Models

import java.io.Serializable

class FeaturesModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var type:String?=null
    var feature_id:Int?=null
    var value:String?=null
    var value_id:String?=null
    var options:ArrayList<OptionsModel>?=null
}