package com.aait.realestateapp.Models

import java.io.Serializable

class ContrastResponse:BaseResponse(),Serializable {
    var services:ArrayList<ListModel>?=null
    var cities:ArrayList<ListModel>?=null
}