package com.aait.realestateapp.Models

import java.io.Serializable

class FeesModel:Serializable {
    var rental_fee:String?=null
    var email:String?=null
    var annual_subscription:String?=null
    var selling_fee:String?=null
    var explanation_points:String?=null
}