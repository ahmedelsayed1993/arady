package com.aait.realestateapp.Models

import java.io.Serializable

class ImagesModel:Serializable {
    var id:Int?=null
    var image:String?=null

    constructor(id: Int?, image: String?) {
        this.id = id
        this.image = image
    }
}