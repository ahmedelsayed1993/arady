package com.aait.realestateapp.Models

import java.io.Serializable

class ArchiveModel:Serializable {
    var archive:String?=null
    var files:ArrayList<FileModel>?=null
}