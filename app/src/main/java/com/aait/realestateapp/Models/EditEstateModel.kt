package com.aait.realestateapp.Models

import java.io.Serializable

class EditEstateModel:Serializable {
    var images:ArrayList<ImagesModel>?=null
    var features:ArrayList<FeaturesModel>?=null
    var lat:String?=null
    var lng:String?=null
    var price:String?=null
    var address:String?=null
    var category:String?=null
    var city:String?=null
    var city_id:Int?=null
    var category_id:Int?=null
    var end_price:String?=null
    var published:Int?=null
    var details:String?=null
    var features_url:String?=null
    var neighborhood:String?=null
    var neighborhood_id:Int?=null
}