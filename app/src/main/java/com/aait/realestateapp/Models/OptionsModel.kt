package com.aait.realestateapp.Models

import java.io.Serializable

class OptionsModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var value:String?=null
}