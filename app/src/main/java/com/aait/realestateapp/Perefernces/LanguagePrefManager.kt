package com.aait.realestateapp.Perefernces

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import java.util.*

class LanguagePrefManager(private val mContext: Context) {

    // return sharedPreferences.getString(App_LANGUAGE, Locale.getDefault().getLanguage());
    var appLanguage: String
        @SuppressLint("ObsoleteSdkInt")
        get() {
            val locale = Locale("ar")
            Locale.setDefault(locale)
            val resources: Resources = mContext.getResources()
            val config: Configuration = resources.getConfiguration()
           // config.setLocale(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
                config.setLocale(locale)
            } else{
                config.setLocale(locale)
            }
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N){
                mContext.createConfigurationContext(config)
            } else {
                resources.updateConfiguration(config,resources.displayMetrics)
            }
            val sharedPreferences = mContext.getSharedPreferences(
                    SHARED_PREF_NAME, 0
            )
            return sharedPreferences.getString(App_LANGUAGE, locale.language)!!
        }
        @SuppressLint("ObsoleteSdkInt")
        set(language) {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val resources: Resources = mContext.getResources()
            val config: Configuration = resources.getConfiguration()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
                config.setLocale(locale)
            } else{
                config.setLocale(locale)
            }
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N){
                mContext.createConfigurationContext(config)
            } else {
                resources.updateConfiguration(config,resources.displayMetrics)
            }
            val sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, 0)
            val editor = sharedPreferences.edit()
            editor.putString(App_LANGUAGE, locale.language)
            editor.apply()
        }

    companion object {
        private val SHARED_PREF_NAME = "Mazad_pref"
        private val App_LANGUAGE = "Mazad_language"
    }
}
