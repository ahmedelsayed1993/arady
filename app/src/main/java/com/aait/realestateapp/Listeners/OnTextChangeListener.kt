package com.aait.realestateapp.Listeners

interface OnTextChangeListener {
    fun onTextChanged(p0: CharSequence?, position:Int)
}