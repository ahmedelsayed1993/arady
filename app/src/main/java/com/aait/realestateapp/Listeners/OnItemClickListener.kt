package com.aait.realestateapp.Listeners

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
    fun onTextChanged(view:View, position:Int)
}
