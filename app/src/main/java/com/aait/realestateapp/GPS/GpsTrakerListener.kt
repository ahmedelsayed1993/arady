package com.aait.realestateapp.GPS

interface GpsTrakerListener {
    fun onTrackerSuccess(lat: Double?, log: Double?)

    fun onStartTracker()
}
