package com.aait.realestateapp.Network

import com.aait.realestateapp.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Service {

    @POST("countries")
    fun getCountries(@Header("lang") lang:String):Call<ListResponse>

    @POST("cities")
    fun getCities(@Header("lang") lang: String):Call<ListResponse>

    @POST("neighborhoods")
    fun getNeighbors(@Header("lang") lang: String,
                      @Query("city_id") city_id:Int):Call<ListResponse>

    @POST("reports")
    fun Reports(@Header("lang") lang: String,
                @Header("Authorization") Authorization:String):Call<ListResponse>

    @POST("add-report")
    fun AddReport(@Header("lang") lang: String,
                  @Header("Authorization") Authorization:String,
                  @Query("estate_id") estate_id:Int,
                  @Query("report_id") report_id:Int,
                  @Query("reason") reason:String):Call<TermsResponse>
    @FormUrlEncoded
    @POST("sign-up")
    fun SignUp(@Field("name") name:String,
               @Field("phone") phone:String,
               @Field("email") email:String,
               @Field("country_id") country_id:Int,
               @Field("password") password:String,
               @Field("device_id") device_id:String,
               @Field("device_type") device_type:String,
               @Field("mac_address") mac_address_id:String,
               @Header("lang") lang:String): Call<UserResponse>
    @POST("edit-profile")
    fun Profile(@Header("Authorization") Authorization:String,
                @Header("lang") lang:String,
                @Query("name") name: String?,
                @Query("phone") phone: String?,
               @Query("country_id") country_id:Int?):Call<UserResponse>
    @Multipart
    @POST("edit-profile")
    fun Profile(@Header("Authorization") Authorization:String,
                @Header("lang") lang:String,
                @Part avatar:MultipartBody.Part):Call<UserResponse>
    @FormUrlEncoded
    @POST("reset-password")
    fun resetPassword(@Header("lang") lang:String,
                      @Header("Authorization") Authorization:String,
                      @Field("current_password") current_password:String,
                      @Field("password") password:String):Call<BaseResponse>
    @POST("log-out")
    fun logOut(@Header("Authorization") Authorization:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("mac_address") mac_address:String,
               @Header("lang") lang:String):Call<TermsResponse>

    @FormUrlEncoded
    @POST("check-code")
    fun CheckCode(@Header("Authorization") Authorization:String,
                  @Field("code") code:String,
                  @Header("lang") lang: String):Call<UserResponse>

    @POST("resend-code")
    fun Resend(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("sign-in")
    fun Login(@Field("phone") phone:String,
              @Field("password") password:String,
              @Field("device_id") device_id:String,
              @Field("device_type") device_type:String,
              @Field("mac_address") mac_address_id:String,
              @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("forget-password")
    fun ForGot(@Field("phone") phone:String,
               @Header("lang") lang:String):Call<UserResponse>

    @FormUrlEncoded
    @POST("update-password")
    fun NewPass(@Header("Authorization") Authorization:String,
                @Field("password") password:String,
                @Field("code") code:String,
                @Header("lang") lang: String):Call<UserResponse>


    @POST("terms-addition-estate")
    fun About(@Header("lang") lang:String):Call<TermsResponse>

    @POST("categories")
    fun Category(@Header("lang") lang:String):Call<ListResponse>

    @POST("applicants")
    fun Applicants(@Header("lang") lang: String):Call<ListResponse>

    @POST("advertising-fee")
    fun Fees(@Header("lang") lang:String,
             @Header("Authorization") Authorization:String,
            @Query("category_id") category_id:Int?):Call<FeesResponse>

    @POST("category-features")
    fun Features(@Header("lang") lang:String,
                 @Header("Authorization") Authorization:String,
                 @Query("category_id") category_id:Int):Call<FeatureResponse>

    @POST("request-features")
    fun RequestFeatures(@Header("lang") lang:String,
                        @Header("Authorization") Authorization:String,
                        @Query("category_id") category_id:Int):Call<FeatureResponse>

    @POST("filter-features")
    fun Filter(@Header("lang") lang: String,@Query("category_id") category_id:Int?,
               @Query("features") features:String?,
               @Query("address_search") address_search:String?,
               @Query("search_date") search_date:Int?):Call<FilterResponse>
    @Multipart
    @POST("add-estate")
    fun AddProperty(@Header("lang") lang:String,
                    @Header("Authorization") Authorization:String,
                    @Query("category_id") category_id:Int,
                   @Query("address") address:String,
                   @Query("lat") lat:String,
                  @Query("lng") lng:String,
                  @Query("city_id") city_id: Int,
                  @Query("neighborhood_id") neighborhood_id:Int,
                 @Query("price") price:String,
                 @Query("details") details:String,
                 @Query("features") features:String,
                 @Part images:ArrayList<MultipartBody.Part>,
                 @Query("marketing") marketing:Int):Call<AddEstateResponse>
    @POST("add-estate")
    fun AddProperty(@Header("lang") lang:String,
                    @Header("Authorization") Authorization:String,
                    @Query("category_id") category_id:Int,
                    @Query("address") address:String,
                    @Query("lat") lat:String,
                    @Query("lng") lng:String,
                    @Query("city_id") city_id: Int,
                    @Query("neighborhood_id") neighborhood_id:Int,
                    @Query("price") price:String,
                    @Query("details") details:String,
                    @Query("features") features:String,
                    @Query("marketing") marketing:Int):Call<AddEstateResponse>
    @POST("edit-estate")
    fun EditEstate(@Header("lang") lang:String,
                   @Header("Authorization") Authorization:String,
                   @Query("estate_id") estate_id:Int,
                   @Query("address") address:String?,
                   @Query("lat") lat:String?,
                   @Query("lng") lng:String?,
                   @Query("city_id") city_id: Int?,
                   @Query("neighborhood_id") neighborhood_id:Int?,
                   @Query("price") price:String?,
                   @Query("details") details:String?,
                   @Query("features") features:String?):Call<EditEstateResponse>
    @Multipart
    @POST("edit-estate")
    fun EditImages(@Header("lang") lang:String,
                   @Header("Authorization") Authorization:String,
                   @Query("estate_id") estate_id:Int,
                   @Part images:ArrayList<MultipartBody.Part>,
                  @Query("delete_images") delete_images:String?):Call<EditEstateResponse>
    @POST("edit-estate")
    fun EditImage(@Header("lang") lang:String,
                   @Header("Authorization") Authorization:String,
                   @Query("estate_id") estate_id:Int,
                   @Query("delete_images") delete_images:String?):Call<EditEstateResponse>

    @POST("delete-estate")
    fun DeleteEstate(@Header("lang") lang:String,
                     @Header("Authorization") Authorization:String,
                     @Query("estate_id") estate_id:Int):Call<TermsResponse>

    @POST("add-comment")
    fun AddComment(@Header("lang") lang:String,
                   @Header("Authorization") Authorization:String,
                   @Query("estate_id") estate_id:Int,
                   @Query("comment") comment:String):Call<TermsResponse>
    @POST("estate-comments")
    fun Comments(@Header("lang") lang:String,
                 @Header("Authorization") Authorization:String,
                 @Query("estate_id") estate_id:Int):Call<CommentsResponse>
    @POST("estate-reports")
    fun Reports(@Header("lang") lang:String,
                 @Header("Authorization") Authorization:String,
                 @Query("estate_id") estate_id:Int):Call<CommentsResponse>
    @POST("services")
    fun Services(@Header("lang") lang:String):Call<ServicesResponse>

    @POST("request-service")
    fun RequestService(@Header("lang") lang:String,
                       @Query("name") name:String,
                       @Query("phone") phone:String,
                       @Query("service_id") service_id:Int,
                       @Query("city_id") city_id:Int,
                       @Query("neighborhood_id") neighborhood_id:Int,
                       @Query("request") request:String):Call<TermsResponse>
    @POST("about")
    fun AboutApp(@Header("lang") lang:String):Call<TermsResponse>

    @POST("add-favorite")
    fun AddFav(@Header("Authorization") Authorization:String,
               @Header("lang") lang:String,
               @Query("estate_id") estate_id:Int):Call<AddEstateResponse>

    @POST("user-guide")
    fun Gide(@Header("lang") lang:String):Call<TermsResponse>
    @POST("estate-details")
    fun EstateDetails(@Header("Authorization") Authorization:String,
                      @Header("lang") lang: String,
                      @Query("estate_id") estate_id:Int,
                      @Query("published") published:Int?):Call<EstateDetailsResponse>

    @POST("estate-request-details")
    fun RequestDetails(@Header("Authorization") Authorization:String?,
                      @Header("lang") lang: String,
                      @Query("estate_id") estate_id:Int,
                      @Query("published") published:Int?):Call<RequestDetailsResponse>

    @POST("home")
    fun Home(@Header("lang") lang:String,
             @Query("estate_type") estate_type:String,
             @Query("lat") lat:String,
             @Query("lng") lng:String,
             @Query("category_id") category_id:Int?,
             @Query("sort") sort:String?):Call<HomeResponse>
    @POST("home")
    fun Orders(@Header("lang") lang:String,
             @Query("estate_type") estate_type:String,
             @Query("lat") lat:String,
             @Query("lng") lng:String,
             @Query("category_id") category_id:Int?,
             @Query("sort") sort:String?):Call<OrdersResponse>

    @POST("interested-orders")
    fun InterstedOrders(@Header("lang") lang: String,
                        @Header("Authorization") Authorization:String):Call<OrdersResponse>
    @POST("my-estates")
    fun MyEstates(@Header("Authorization") Authorization:String,
                  @Header("lang") lang: String,
                  @Query("active") active:Int):Call<MyEstatesResponse>

    @POST("estates-today")
    fun EstatesToday(@Header("lang") lang: String,
    @Query("city_id") city_id: Int?):Call<MyEstatesResponse>

    @POST("special-estates")
    fun Special(@Header("lang") lang: String):Call<MyEstatesResponse>

    @POST("my-favorites")
    fun MyFavs(@Header("lang") lang: String,
               @Header("Authorization") Authorization:String):Call<MyEstatesResponse>

    @POST("add-request-estate")
    fun RequestEstate(@Header("Authorization") Authorization:String,
                      @Header("lang") lang: String,
                      @Query("category_id") category_id:Int,
                      @Query("city_id") city_id:Int,
                      @Query("applicant_id") applicant_id:Int,
                      @Query("neighborhoods") neighborhoods:String,
                      @Query("address") address:String,
                      @Query("lat") lat: String,
                      @Query("lng") lng: String,
                      @Query("start_price") start_price:String,
                      @Query("end_price") end_price:String,
                      @Query("features") features:String,
                      @Query("details") notes:String?):Call<AddEstateResponse>

    @POST("estate-user-details")
    fun EstateDetails(@Header("Authorization") Authorization:String?,
                      @Header("lang") lang: String,
                      @Query("estate_id") estate_id:Int,
                      @Query("lat") lat:String,
                      @Query("lng") lng: String):Call<EstateDetailsResponse>

    @FormUrlEncoded
    @POST("conversation-id")
    fun Conversation(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String,
                     @Field("receiver_id") receiver_id:Int,
                     @Field("estate_id") estate_id:Int):Call<ConversationIdResponse>
    @POST("send-message")
    fun Send(@Header("lang") lang: String,
             @Header("Authorization") Authorization:String,
             @Query("receiver_id") receiver_id:Int,
             @Query("conversation_id") conversation_id:Int,
             @Query("type") type:String,
             @Query("message") message:String):Call<SendChatResponse>

    @Multipart
    @POST("send-message")
    fun SendImage(@Header("lang") lang: String,
                  @Header("Authorization") Authorization:String,
                  @Query("receiver_id") receiver_id:Int,
                  @Query("conversation_id") conversation_id:Int,
                  @Query("type") type:String,
                  @Part message: MultipartBody.Part):Call<SendChatResponse>

    @POST("conversation")
    fun Conversation(@Query("lang") lang: String,
                     @Query("conversation_id") conversation_id:Int,
                     @Header("Authorization") Authorization:String,
                     @Query("page") page:Int,
                     @Query("app_type") app_type:String):Call<ChatResponse>

    @POST("my-conversations")
    fun Conversations(@Header("lang") lang: String,
                      @Header("Authorization") Authorization:String):Call<ChatsResponse>

    @POST("questions")
    fun Questions(@Header("lang") lang:String):Call<QuestionResponse>

    @POST("contact-us")
    fun Contact(@Header("lang") lang: String,
                @Query("name") name:String?,
                @Query("phone") phone:String?,
                @Query("message") message:String?):Call<ContactResponse>

    @POST("construction-contracting")
    fun Construction(@Header("lang") lang: String,
                     @Query("phone") phone:String?,
                     @Query("details") details:String?,
                    @Query("contract_id") contract_id:Int?,
                    @Query("city_id") city_id:Int?):Call<ContrastResponse>

    @POST("user-interests")
    fun Interstes(@Header("lang") lang:String,
                  @Header("Authorization") Authorization:String):Call<InterstesResponse>

    @POST("add-interested")
    fun AddInterst(@Header("lang") lang:String,
                   @Header("Authorization") Authorization:String,
                   @Query("city_id") city_id: Int,
                   @Query("neighborhood_id") neighborhood_id:Int,
                   @Query("category_id") category_id:Int):Call<TermsResponse>
    @POST("delete-interest")
    fun DeleteInterst(@Header("lang") lang:String,
                      @Header("Authorization") Authorization:String,
                      @Query("interest_id") interest_id:Int):Call<TermsResponse>


    @POST("marketing-agreement")
    fun Marketing(@Header("lang") lang:String):Call<TermsResponse>

    @POST("banks-accounts")
    fun Banks(@Header("lang") lang:String):Call<BanksResponse>

    @POST("bank-transfer")
    fun BankTransfer(@Header("lang") lang:String,
                     @Header("Authorization") Authorization:String,
                     @Query("phone") phone:String,
                     @Query("amount") amount:String,
                     @Query("bank_name") bank_name:String,
                     @Query("operation_type") operation_type:String,
                     @Query("payment_date") payment_date:String,
                    @Query("payment_details") payment_details:String):Call<TermsResponse>

    @POST("webview-links")
    fun WebLinks(@Header("lang") lang:String):Call<WebResponse>

    @Multipart
    @POST("office-documents")
    fun Office(@Header("lang") lang:String,
               @Header("Authorization") Authorization:String,
               @Part images:ArrayList<MultipartBody.Part>):Call<BaseResponse>

    @POST("estate-archive")
    fun Archive(@Header("lang") lang:String,
                @Header("Authorization") Authorization:String,
                @Query("estate_id") estate_id: Int,
               @Query("archive") archive:String?,
               @Query("delete_files") delete_files:String?):Call<ArchiveResponse>

    @Multipart
    @POST("estate-archive")
    fun Archive(@Header("lang") lang:String,
                @Header("Authorization") Authorization:String,
                @Query("estate_id") estate_id: Int,
                @Query("archive") archive:String,
                @Query("delete_files") delete_files:String?,
                @Part upload_files:ArrayList<MultipartBody.Part>):Call<ArchiveResponse>

}